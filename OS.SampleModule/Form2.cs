﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenShellClasses;

namespace OS.SampleModule
{
    public partial class Form2 : Form, IPageEvents
    {
        private IConnectionContext cc;
        private RepositoryStorage storage;

        public Form2()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            storage.WriteValue("TestString", textBox1.Text);
        }

        #region IPageEvents Members

        public bool CanEnterPage(IModuleContext context)
        {
            return true;
        }

        public bool CanLeavePage(IModuleContext context)
        {
            return true;
        }

        public void CreatePageControl(IModuleContext context)
        {
            cc = context.ConnectionContext;
            storage = cc.GetStorage(@"SampleModule01");
        }

        public void EnterPage(IModuleContext context)
        {
        }

        public void LeavePage(IModuleContext context)
        {
        }

        #endregion

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            textBox1.Text = Convert.ToString(storage.ReadValue("TestString"));
        }
    }
}
