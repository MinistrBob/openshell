﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using OpenShellClasses;

namespace OS.SampleModule
{
    public partial class ctlTestDataConnection : UserControl, OpenShellClasses.IPageEvents
    {
        public ctlTestDataConnection()
        {
            InitializeComponent();
        }


        #region IPageEvents Members

        public bool CanEnterPage(IModuleContext context)
        {
            if (MessageBox.Show("Войти?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                return true;
            else
                return false;
        }

        public bool CanLeavePage(IModuleContext context)
        {
            if (MessageBox.Show("Выйти?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                return true;
            else
                return false;
        }

        public void CreatePageControl(IModuleContext context)
        {
            oracleCommand1.Connection = (context.ConnectionContext.Connection as DbConnection).Conn;
        }

        public void EnterPage(IModuleContext context)
        {
        }

        public void LeavePage(IModuleContext context)
        {
        }

        #endregion

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            tableRP_EMPS.Clear();
            tableRP_EMPS.Columns.Clear();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.Columns.Clear();
            oracleCommand1.CommandText = textBox1.Text;
            oracleDataAdapter1.Fill(tableRP_EMPS);
            bindingSource1.ResetBindings(false);
            dataGridView1.AutoGenerateColumns = true;
        }
    }
}
