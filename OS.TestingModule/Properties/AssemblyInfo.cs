﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("OS.TestingModule")]
[assembly: AssemblyDescription("OS.TestingModule")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ООО Октопус")]
[assembly: AssemblyProduct("OS.TestingModule")]
[assembly: AssemblyCopyright("Copyright © ООО Октопус")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using '*'.
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: ComVisible(false)]


//NOTE: When updating the namespaces in the project please add new or update existing the XmlnsDefinitionAttribute
//You can add additional attributes in order to map any additional namespaces you have in the project
//[assembly: System.Workflow.ComponentModel.Serialization.XmlnsDefinition("http://schemas.com/OS.TestingModule", "OS.TestingModule")]
[assembly: GuidAttribute("B590F3E8-D88D-473D-9592-1DEECC91BA07")]
