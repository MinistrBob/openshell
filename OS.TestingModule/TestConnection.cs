﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenShellClasses;

namespace OS.TestingModule
{
	public class TestConnectionType : ConnectionType
	{
		private TestConnection_LoginControl loginControl = new TestConnection_LoginControl();

		public TestConnectionType() 
		{
			Name = "Тестовое соединение";
			Hint = "Проверка-проверка";
			Image = Resource1.Image2;
		}

		public override Connection CreateConnection(System.Windows.Forms.Control loginControl)
		{
			return new TestConnection(this);            
		}

		public override System.Windows.Forms.Control GetLoginControl()
		{
			return loginControl;
		}

		public override bool LoginControl_Validate(System.Windows.Forms.Control control)
		{
			return true;
		}

		public override string LoginControl_GetData(System.Windows.Forms.Control control)
		{
			return null;
		}

		public override void LoginControl_SetData(System.Windows.Forms.Control control, string data)
		{
		}
	}

	public class TestConnection : Connection
	{
		public TestConnection(TestConnectionType ct) : base(ct)
		{
		}

		public override void Open(IConnectionContext Context)
		{
			//throw new NotImplementedException();
		}

		public override void Close(IConnectionContext Context)
		{
			//throw new NotImplementedException();
		}

		protected override string GetName()
		{
			return "Hello!";
		}

		protected override string GetConnectionPath()
		{
			return "TEST";
		}
	}
}
