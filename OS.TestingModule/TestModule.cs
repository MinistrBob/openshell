﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenShellClasses;
using System.Windows.Forms;

namespace OS.TestingModule
{
	[ModuleConnectionBinding(typeof(DbConnectionType), true)]
	[ModuleConnectionBinding(typeof(TestConnectionType), false)]
	public class TestModule : ModuleBase
	{
		#region IModule Members


		public override void Load()
		{
			MessageBox.Show("Module load");
			ContainerAction a = new ContainerAction("Test:Container", "Проверка", Resources.Resource1.Control_Timer);
			a.Childs.Add(new Action("TEST1", "test 1")).Image = Resources.Resource1.keys;
			a.Childs.Add(new Action("TEST2", "test 2")).Image = Resources.Resource1.install;
			Context.AddAction(a);
			Context.AddAction(a);
		}

		public override void Unload()
		{
			MessageBox.Show("Module unload");
		}

		#endregion
	}
}
