﻿// =================================================================================================
//  App.cs
//  Основной класс работы приложения
// =================================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using log4net;
using OpenShellClasses;
using OpenShell.Resources;

namespace OpenShell
{

    // =================================================================================================

    #region App - главный класс приложения

    /// <summary>
    /// App - главный класс приложения
    /// </summary>
    public class App
    {
        // ------------------------------------------------------------------------------------------------------------------------

        #region закрытые поля

        private static ILog log;

        private static App _Current;
        internal MainForm _MainForm;
        private string[] _Args;
        internal Arguments _Arguments;
        internal AppState _State = AppState.Initializing;
        //private Forms.frmWaitDialog frmWaitDialog;
        private AppProperties _Properties;

        private AppContext _Context;
        private AppActions _Actions;

        #endregion

        // ------------------------------------------------------------------------------------------------------------------------

        #region конструктор

        /// <summary>
        /// конструктор экземплра приложения
        /// </summary>
        /// <param name="args"></param>
        public App(string[] args)
        {
            _Current = this;

            _Args = args;
            _Arguments = new Arguments(args);
            _Properties = new AppProperties(this);

            _Context = new AppContext(this);
            _Actions = new AppActions(this);
        }

        #endregion

        // ----------------------------------------

        #region открытые свойства

        /// <summary>
        /// активное приложение
        /// </summary>
        public static App Current
        {
            get { return _Current; }
        }

        // ----------------------------------------

        /// <summary>
        /// состояние приложения
        /// </summary>
        public AppState State
        {
            get { return _State; }
        }

        // ----------------------------------------

        /// <summary>
        /// главная форма приложения
        /// </summary>
        public static MainForm MainForm
        {
            get { return Current._MainForm; }
        }

        // ----------------------------------------

        /// <summary>
        /// свойства приложения
        /// </summary>
        public AppProperties Properties
        {
            get { return _Properties; }
        }

        // ----------------------------------------

        /// <summary>
        /// контекст приложения
        /// </summary>
        internal AppContext Context
        {
            get { return _Context; }
        }

        /// <summary>
        /// действия приложения
        /// </summary>
        internal AppActions Actions
        {
            get { return _Actions; }
        }

        #endregion

        // ------------------------------------------------------------------------------------------------------------------------

        #region METHODS

        #region Методы загрузки приложения

        /// <summary>
        /// Точка входа в программу. Проверка - есть ли запущеные копии программы.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            // Проверка есть ли запущеные копии программы, реализована с помощью мьютекса
            bool onlyInstance;
            // Создаем мьютекс
            using (Mutex mtx = new Mutex(true, "OpenShell.exe", out onlyInstance))
            {
                // Если он создается, значит ни одной копии еще не запущено - просто запускаем.
                if (onlyInstance)
                {
                    Main1(args);
                }
                // Если он НЕ создается, значит уже есть запущеные экземпляры - спрашиваем юзера что делать
                else
                {
                    if (DialogResult.Yes == MessageBox.Show(
                                           "Приложение \"Универсальная оболочка\" уже запущено, хотите запустить еще один экземпляр?",
                                           "Сообщение",
                                           MessageBoxButtons.YesNo, MessageBoxIcon.Stop))
                    {
                        Main1(args);
                    }
                   
                }
            }
        }

        private static void Main1(string[] args)
        {
            //AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

            Main2(args);
        }

        private static void Main2(string[] args)
        {
            //string[] oracle_names = OpenShellClasses.Util.GetOracleDbNames();

            log4net.Config.XmlConfigurator.Configure();
            log = LogManager.GetLogger(typeof(App));
            log.Debug("Запуск приложения");

            //AppDomain.CurrentDomain.AssemblyLoad += new AssemblyLoadEventHandler(CurrentDomain_AssemblyLoad);
            //AppDomain.CurrentDomain.TypeResolve += new ResolveEventHandler(CurrentDomain_TypeResolve);

            try
            {
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
                Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
                App app = new App(args);
                app.Run();
            }
            catch (Exception ex)
            {
                ShowException(ex);
            }
        }

        //static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        //{
        //    return LoadAssemblyFromPath(args.Name, OpenShell.Properties.Settings.Default.AssemblySearchPaths.Split(';', ','));
        //}

        //private static Assembly LoadAssemblyFromPath(string AssemblyName, params string[] Paths)
        //{
        //    AssemblyName an = new AssemblyName(AssemblyName);
        //    foreach (string Path in Paths)
        //    {
        //        string path = Path;
        //        if (!System.IO.Path.IsPathRooted(path))
        //            path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path);

        //        if (System.IO.Directory.Exists(path))
        //        {
        //            List<string> executableFiles = new List<string>();
        //            executableFiles.AddRange(Directory.GetFiles(path, "*.exe"));
        //            executableFiles.AddRange(Directory.GetFiles(path, "*.dll"));

        //            foreach (string FileName in executableFiles)
        //                if (System.IO.Path.GetFileName(FileName).ToLower().StartsWith(an.Name.ToLower()))
        //                {
        //                    try
        //                    {
        //                        Assembly asm = AppDomain.CurrentDomain.Load(File.ReadAllBytes(FileName));

        //                        AssemblyName lan = new AssemblyName(asm.FullName);
        //                        if (an.Name == lan.Name &&
        //                            an.Version == lan.Version)
        //                        {
        //                            return asm;
        //                        }
        //                    }
        //                    catch
        //                    {
        //                    }
        //                }
        //        }
        //    }
        //    return null;
        //}

        //static Assembly CurrentDomain_TypeResolve(object sender, ResolveEventArgs args)
        //{
        //    return null;
        //}

        //static void CurrentDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
        //{
        //}

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            ShowException(e.Exception);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ShowException(e.ExceptionObject as Exception);
        }

        // ------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// основной метод запуска приложения
        /// </summary>
        public virtual void Run()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //_Properties = new AppProperties(this);

            //frmWaitDialog = Forms.frmWaitDialog.Start(Strings.App_Status_Initializing);

            if (_MainForm == null)
                _MainForm = new MainForm(this);
            Initialize();
            try
            {
                //frmWaitDialog.Close();
                _State = AppState.Running;

                Application.Run(MainForm);
            }
            finally
            {
                Deinitialize();
            }
        }

        #endregion

        // ------------------------------------------------------------------------------------------------------------------------

        #region Методы инициализации/деинициализации приложения

        /// <summary>
        /// операции перед загрузкой формы
        /// </summary>
        private void Initialize()
        {
            Context.Initialize();
        }

        // ----------------------------------------

        /// <summary>
        /// операции, выполняемые перед выходом из приложения
        /// </summary>
        protected internal void Deinitialize()
        {
            Context.Deinitialize();
        }

        #endregion

        // ------------------------------------------------------------------------------------------------------------------------

        #region Служебные общие методы

        public static void ShowException(Exception exception)
        {
            string msg = exception.Message;
            if (exception.InnerException != null)
                msg += string.Format("\r\n[{0}]", exception.InnerException.Message);

#if DEBUG
            msg += exception.StackTrace;
#endif

            log.Error(msg, exception);
            MessageBox.Show(msg, Strings.MsgError, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        // ------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// выход из приложения без предупреждения
        /// </summary>
        internal void Quit()
        {
            _State = AppState.Closing;
            if (App.Current.Context.Connections.Connections.Count > 0)
                App.Current.Context.CloseConnection(App.Current.Context.Repository as Connection);
            _MainForm.Close();
            _MainForm.Dispose();
        }

        #endregion

        #endregion

        // ------------------------------------------------------------------------------------------------------------------------

    }

    #endregion

    // ==================================================================================================

}
