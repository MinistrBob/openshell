﻿// =================================================================================================
//  AppActions.cs
//  Действия приложения, вызываемые пользователем
// =================================================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using OpenShellClasses;
using OpenShell.Resources;
using OpenShell.RepositoryControlModule;

namespace OpenShell
{

    // =================================================================================================
    
    #region AppActions - действия приложения, вызываемые пользователем

    /// <summary>
    /// AppActions - действия приложения, вызываемые пользователем
    /// </summary>
    internal class AppActions
    {
        // -------------------------------------------------------------------------------------------

        #region закрытые поля

        private App app;

        #endregion

        // -------------------------------------------------------------------------------------------

        #region Конструктор

        public AppActions(App app)
        {
            this.app = app;
        }

        #endregion

        // -------------------------------------------------------------------------------------------

        #region Методы - действия

        /// <summary>
        /// выход из приложения с предупреждением
        /// </summary>
        internal bool DoAskQuit()
        {
            try
            {
                if (app.State == AppState.Running)
                    if (MessageBox.Show(Strings.App_AskQuit_Question, Strings.App_AskQuit_Caption,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        app.Quit();
                        return true;
                    }
            }
            catch (Exception ex)
            {
                App.ShowException(ex);
            }
            return false;
        }

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// просмотр диалогового окна с информацие о программе
        /// </summary>
        internal void DoShowAbout()
        {
            try
            {
                using (Forms.frmAbout f = new OpenShell.Forms.frmAbout())
                    f.ShowDialog();
            }
            catch (Exception ex)
            {
                App.ShowException(ex);
            }
        }

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// установка подключения к репозиторию
        /// </summary>
        internal void DoConnectionOpen()
        {
            try
            {
                //if (app.Context.RepositoryConnected)
                //    throw new ArgumentException(Strings.Msg_Error_Repository_Already_Connected);
                showNewConnectionForm();
            }
            catch (Exception ex)
            {
                App.ShowException(ex);
            }
        }

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// отключение от текущего репозитория
        /// </summary>
        internal void DoConnectionClose()
        {
            try
            {
                if (app.Context.RepositoryConnected)
                    if (MessageBox.Show(string.Format(Strings.Msg_App_AskCloseRepository, app.Context.ActiveConnection.Connection),
                        Strings.Msg_App_AskCloseRepository_Title,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        app.Context.CloseConnection(app.Context.ActiveConnection.Connection as Connection);
                    }
            }
            catch (Exception ex)
            {
                App.ShowException(ex);
            }
        }

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// установка нового подключения, после установки подключения к УО
        /// </summary>
        /*internal void DoNewConnection()
        {
            try
            {
                if (!app.Context.RepositoryConnected)
                    throw new ArgumentException(Strings.Msg_Error_Repository_NotConnected);

                showNewConnectionForm();
            }
            catch (Exception ex)
            {
                App.ShowException(ex);
            }
        }*/

        // -------------------------------------------------------------------------------------------

        private void showNewConnectionForm()
        {
            Forms.frmNewConnection frm = new OpenShell.Forms.frmNewConnection();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                app.Context.OpenConnection(frm.NewConnection);
            }
        }

        // -------------------------------------------------------------------------------------------

        #endregion

        internal void DoShellSettings()
        {
            Forms.frmShellSettings frm = new OpenShell.Forms.frmShellSettings();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                App.Current.Properties.List.SetValues(frm.propList);
                App.Current.Context.SaveShellProperties();
                App.Current.Properties.Apply();
            }
        }

        internal void DoRepositoryControl()
        {
            try
            {
                if (frmRepositoryControlModule.Form == null)
                    frmRepositoryControlModule.Form = new OpenShell.RepositoryControlModule.frmRepositoryControlModule();
                frmRepositoryControlModule.Form.Show();
                frmRepositoryControlModule.Form.Activate();
            }
            catch (Exception ex)
            {
                App.ShowException(ex);
            }
        }
    }

    #endregion

    // =================================================================================================

}
