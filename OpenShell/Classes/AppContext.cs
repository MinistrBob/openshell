﻿// =================================================================================================
//  AppContext.cs
//  Контекст выполнения приложения и объект, который доступен из подключаемых модулей
// =================================================================================================

// флаг, указывающий на то что используется локальный репозиторий
#define LOCAL_REPOSITORY

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using OpenShellClasses;
using OpenShell.Resources;

namespace OpenShell
{
    // =================================================================================================

    #region AppContext - контекст выполнения приложения

    /// <summary>
    /// AppContext - контекст выполнения приложения
    /// </summary>
    internal class AppContext : IAppContext
    {

        // ------------------------------------------------------------------------------------------------------------

        #region закрытые поля

        private App app;
        private IRepository _Repository;
        private ConnectionTypeList _ConnectionTypes;
        private ConnectionContextList _Connections;
        private ClassPathLoader _PathLoader;
        private ClassPathList _ModuleClasses;
        internal ModuleConnetionTypeBindingList _ModuleConnTypeBindings;
        private ModuleContextList _Modules;
        private ConnectionContext _ActiveConnection;
        private ConnectionContext _RepositoryConnectionContext;

        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region события

        //public event AppEvent AppEvent;

        public event ConnectionEventHandler ConnectionOpened;
        public event ConnectionEventHandler ConnectionClosed;

        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region конструктор

        public AppContext(App app)
        {
            this.app = app;
            _PathLoader = new ClassPathLoader(Properties.Settings.Default.AssemblySearchPaths);
            _Connections = new ConnectionContextList();
            _ConnectionTypes = new ConnectionTypeList();
            _ModuleClasses = new ClassPathList();
            _ModuleConnTypeBindings = new ModuleConnetionTypeBindingList();
            _Modules = new ModuleContextList();
        }

        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region свойства

        /// <summary>
        /// текущий контекст приложения
        /// </summary>
        internal static AppContext Current
        {
            get
            {
                return App.Current.Context;
            }
        }

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// текущее подключение к репозиторию
        /// </summary>
        public IRepository Repository
        {
            get
            {
                return _Repository;
            }
        }

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// установлено ли подключение к репозиторию
        /// </summary>
        public bool RepositoryConnected
        {
            get { return (Repository != null); }
        }

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// список соединений
        /// </summary>
        public ConnectionTypeList ConnectionsTypes
        {
            get { return _ConnectionTypes; }
        }

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// коллекция установленных соединений
        /// </summary>
        public ConnectionContextList Connections
        {
            get { return _Connections; }
        }


        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region методы

        #region методы работы с соединениями

        /// <summary>
        /// открытие нового соединения
        /// </summary>
        public void OpenConnection(Connection Connection)
        {
            try
            {
                if ((Connection is IRepository) && RepositoryConnected)
                    throw new ArgumentException(Strings.Msg_Error_Repository_Already_Connected);

                using (Forms.frmWaitDialog wd = Forms.frmWaitDialog.Start(
                    string.Format(Strings.App_Status_Connection_Open, Connection.Name)))
                {
                    // создаем объект - оболочку для соединения
                    ConnectionContext cc;

                    cc = new ConnectionContext(Connection, app.Context);

                    // открываем соединение, используя метод Open, реализуемый соединением
                    Connection.Open(cc);

                    // добавляем созданный контекст соединения в список соединений
                    Connections.Add(cc);

                    try
                    {
                        // если подключение является репозиторием, то подключаем его как репозиторий
                        if (Connection is IRepository)
                            OpenRepository(Connection as IRepository);

                        SaveConnection(cc);

                        if (Connection is IRepository)
                            OpenRepositoryAfterSaveConnection(Connection as IRepository);                        

                        wd.Hide();

                        // если соединение успешно открыто, то активизируем контекст соединения
                        cc.Initialize();

                        OnConnectionOpened(cc);//

                        App.MainForm.ApplyMainFormProperties();
                    }
                    catch 
                    {
                        Connection.Close(cc);
                        Connections.Remove(cc);
                        if (Connection is IRepository) 
                            _Repository = null;
                        throw;
                    }
                }
            }
            finally
            {
                Update();
            }
        }

        private void SaveConnection(ConnectionContext Context)
        {
            // если указан флаг = сохранять соединение и соединяемся с репозиторием
            if (Context.Connection.PersistInfo && Context.Connection is IRepository)
            {
                SaveLocalRepositoryConnection(Context);
            }

            if (Repository != null)
            {
                // всегда регистрируем соединение в репозитории, так как на него ссылаю
                Context.Connection.RepositoryUserConnection = Repository.SaveConnection(Context);
            }
        }

        private void SaveLocalRepositoryConnection(ConnectionContext context)
        {
            LocalSavedRepositoryConnectionList.AddToFile(
                App.Current.Properties.LocalRepositoryConnectionsFile,
                new LocalSavedRepositoryConnection(context.Connection));
        }

        private void OnConnectionOpened(IConnectionContext context)
        {
            if (ConnectionOpened != null)
                ConnectionOpened(this, new ConnectionEventArgs(context));
        }

        // ------------------------------------------------------------------------------------------------------------

        private void OnConnectionClosed(IConnectionContext context)
        {
            if (ConnectionClosed != null)
                ConnectionClosed(this, new ConnectionEventArgs(context));
        }

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// закрытие указанного соединения
        /// </summary>
        public void CloseConnection(IConnection Connection)
        {
            try
            {
                ConnectionContext cc = Connections.GetContext(Connection as Connection);
                if (cc != null)
                {
                    // если подключение является репозиторием, то отключаем его как репозиторий
                    if (Connection is IRepository)
                    {
                        if (!RepositoryConnected)
                            throw new ArgumentException(Strings.Msg_Error_Repository_Already_Connected);

                        // закрываем репозиторий
                        CloseRepository();
                    }

                    // закрываем само соединение
                    Connection.Close(cc);

                    // закрываем связанный с соединением контекст
                    cc.Deinitialize();

                    if (Connections.Connections.Contains(cc.Connection))
                        Connections.Remove(cc);

                    if (_ActiveConnection == cc)
                        _ActiveConnection = null;

                    OnConnectionClosed(cc);
                }
            }
            finally
            {
                Update();
            }
        }

        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region методы работы с репозиторием

        /// <summary>
        /// открытие указанного репозитория
        /// </summary>
        protected virtual void OpenRepository(IRepository Repository)
        {
            // проверка, не присоединияемся ли мы к репозиторию повторно
            if (RepositoryConnected)
                throw new ArgumentException(Strings.Msg_Error_Repository_Already_Connected);

            _Repository = Repository;

            if (_RepositoryConnectionContext == null)
                _RepositoryConnectionContext = Connections.GetContext(Repository as Connection);

            // закрываем репозиторий методом определенным в классе репозитория
            Repository.OpenRepository(_RepositoryConnectionContext);
        }

        // ---------------------------------------------------------------

        public virtual void OpenRepositoryAfterSaveConnection(IRepository Repository)
        {
            // читаем настройки из БД
            Repository.StorageShell.ReadPropertyValues("Properties", app.Properties.List);

            // сохраняем настройки в локальный файл
            app.Properties.SaveToLocalUserDirectory();

            // применить изменившиеся настройки к универсальной оболочке
            app.Properties.Apply();
        }

        /// <summary>
        /// закрытие текущего репозитория
        /// </summary>
        protected virtual void CloseRepository()
        {
            if (Repository == null)
                throw new ArgumentException(Strings.Msg_Error_Repository_NotConnected);

            SaveShellProperties();
            // закрываем все дополнительные соединения
            foreach (Connection cc in Connections.ConnectionsArray())
                if (cc != (Repository as Connection))
                    CloseConnection(cc);

            // закрываем репозиторий методом определенным в классе репозитория
            Repository.CloseRepository(Connections.GetContext(Repository as Connection));

            _RepositoryConnectionContext = null;
            _Repository = null;
        }

        public void SaveShellProperties()
        {
            // сохраняем настройки в локальный файл
            app.Properties.SaveToLocalUserDirectory();

            // записываем настройки в БД
            if (Repository != null)
                Repository.StorageShell.WritePropertyValues("Properties", app.Properties.List);

        }

        #endregion

        // ------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// всеобщее обновление
        /// </summary>
        public virtual void Update()
        {
            App.MainForm.UpdateUI();
        }

        // ---------------------------------------------------------------

        #endregion

        #region инициализация при загрузке программы

        internal void Initialize()
        {
            //ConnectionsTypes.AddRange(ConnectionTypeAutoRegisterAttribute.GetList());

#if LOCAL_REPOSITORY
            ConnectToLocalRepository();
#endif

        }

        private void ConnectToLocalRepository()
        {
            LocalRepositoryConnection localConnection = new LocalRepositoryConnection();
            _RepositoryConnectionContext = new RepositoryConnectionContext(localConnection, this);
            OpenRepository(localConnection);
        }

        // ---------------------------------------------------------------

        internal void Deinitialize()
        {
#if LOCAL_REPOSITORY
            CloseRepository();
#endif
        }

        public ConnectionContext ActiveConnection
        {
            get { return _ActiveConnection; }
            set { _ActiveConnection = value; }
        }

        #endregion

        // ---------------------------------------------------------------

        #region IServiceProvider Members

        /// <summary>
        /// получение сервиса определенного типа
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            if (serviceType == typeof(ClassPathLoader))
                return _PathLoader;
            else if (serviceType == typeof(IConnectionTypeList))
                return this;
            else
                throw new ArgumentException(string.Format(Strings.AppContext_GetService_UnknownType, serviceType.Name));
        }

        #endregion

        #region IConnectionTypeList Members

        public bool AddConnectionType(ConnectionType type)
        {
            return _ConnectionTypes.Add(type);
        }

        public void RemoveConnectionType(ConnectionType type)
        {
            _ConnectionTypes.Remove(type);
        }

        #endregion


        #region IAppContext Members


        public ClassPathList ModuleClasses
        {
            get { return _ModuleClasses; }
        }

        public void LoadModules()
        {
            _ModuleConnTypeBindings.Clear();
            foreach (ClassPath module_path in _ModuleClasses)
                LoadModule(module_path);

            foreach (ConnectionContext cc in _Connections)
                cc.LoadModulesForConnection();

            (_RepositoryConnectionContext as RepositoryConnectionContext).InitializeLoadModules();
        }

        public ClassPathLoader ClassPathLoader
        {
            get { return _PathLoader; }
        }

        private void LoadModule(ClassPath module_path)
        {
            Type module_type = _PathLoader.LoadType(module_path);
            if (module_type != null)
            {
                List<ModuleConnectionBindingAttribute> conn_type_attrs =
                    new List<ModuleConnectionBindingAttribute>((ModuleConnectionBindingAttribute[])module_type.
                    GetCustomAttributes(typeof(ModuleConnectionBindingAttribute), true));

                // получаем список типов соединений, к которым монтировать модули
                foreach (ModuleConnectionBindingAttribute attr in conn_type_attrs)
                {
                    ConnectionType ct = _ConnectionTypes.Find(attr.ConnectionType.FullName);

                    if (ct == null && (attr.ConnectionType.Name == "LocalRepositoryConnectionType"))
                        ct = (Repository as LocalRepositoryConnection).Type;


                    if (ct != null)
                    {
                        _ModuleConnTypeBindings.Add(
                            new ModuleConnetionTypeBinding(ct, module_type, attr.Inheritable,
                                module_path.ClassRef));
                    }
                }
            }
            else
                MessageBox.Show(string.Format(
                    "Ошибка загрузки модуля ({0}): невозможно загрузить класс модуля!", module_path),
                    "Ошибка загрузки модуля", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #endregion

        #region IAppContext Members


        public System.Windows.Forms.Form MainForm
        {
            get { return this.app._MainForm; }
        }

        #endregion

        #region IAppContext Members


        public string LocalUserDirectory
        {
            get { return app.Properties.LocalUserDirectory; }
        }

        #endregion
    }

    #endregion

    // =================================================================================================
}
