﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using OpenShellClasses;
using OpenShell.Resources;

namespace OpenShell
{
    #region Свойства приложения

    public class AppProperties
    {
        #region закрытые поля

        private App app;
        private PropertyList _PropList;
        private string _LocalUserDirectory;
        //private string _LocalUserDirFile;
        private string _LocalRepositoryConnectionsFile;
        private MainFormProperties _MainForm;

        private const string PN_Language = "Language";
        private const string PN_MainForm_WindowState = "MainForm_WindowState";

        #endregion

        public enum LanguageKind
        {
            Russian = 0,
            English = 1
        }

        public AppProperties(App app)
        {
            this.app = app;
            LoadAtStartup();
            //ChangeLanguage();
        }

        /// <summary>
        /// загрузка настроек при старте приложения
        /// </summary>
        public void LoadAtStartup()
        {
            // инициализация локального каталога в профиле пользователя
            InitializeLocalUserDirectory();

            // загрузка схемы настроек (с языком по умолчанию)
            _PropList = PropertyList.DeserializeSchema(Strings.AppProperties);

            // загрузка сохраненных настроек 
            //LoadFromLocalUserDirectory();

            //if (Language == LanguageKind.English)
            //{
            //    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            //    _PropList = PropertyList.DeserializeSchema(Strings.AppProperties);
            //    //LoadFromLocalUserDirectory();
            //}

            _MainForm = new MainFormProperties(this);
        }


        private void InitializeLocalUserDirectory()
        {
            _LocalUserDirectory =
                Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                /*((AssemblyCompanyAttribute[])
                Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), true))[0].Company)*/
                Properties.Settings.Default.LocalUserDirectory);

            if (!Directory.Exists(_LocalUserDirectory))
                Directory.CreateDirectory(_LocalUserDirectory);

            //_LocalUserDirFile = Path.Combine(LocalUserDirectory,
              //  Assembly.GetEntryAssembly().GetName().Name + ".AppProperties.xml");

            //_LocalRepositoryConnectionsFile = Path.Combine(LocalUserDirectory,
                //Assembly.GetEntryAssembly().GetName().Name + ".Repository.Connections.xml");
        }

        //public void LoadFromLocalUserDirectory()
        //{
        //    /*if (File.Exists(_LocalUserDirFile))
        //    {
        //        _PropList.Deserialize(_LocalUserDirFile);
        //    }*/
        //}

        public void SaveToLocalUserDirectory()
        {
            //_PropList.Serialize(_LocalUserDirFile, false);
        }

        #region Свойства приложения УО

        public string LocalUserDirectory
        {
            get { return _LocalUserDirectory; }
        }

        //public LanguageKind Language
        //{
        //    get { return (LanguageKind)Enum.Parse(typeof(LanguageKind), _PropList[PN_Language].AsString); }
        //    set { _PropList[PN_Language].AsString = value.ToString(); }
        //}

        public MainFormProperties MainForm
        {
            get { return _MainForm; }
        }

        #endregion

        public string LocalRepositoryConnectionsFile
        {
            get { return _LocalRepositoryConnectionsFile; }
        }

        public PropertyList List
        {
            get { return _PropList; }
        }

        // ---------------------------------------------------------------

        //public void ChangeLanguage()
        //{
        //    if (CurrentLanguage != Language)
        //    {
        //        CurrentLanguage = Language;
        //    }
        //}

        // ---------------------------------------------------------------

        public AppProperties.LanguageKind CurrentLanguage
        {
            get
            {
                if (Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName.ToLower() == "en")
                    return AppProperties.LanguageKind.English;
                else
                    return AppProperties.LanguageKind.Russian;
            }
            set
            {
                if (value == AppProperties.LanguageKind.English)
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
                else
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("ru");
            }
        }

        // ---------------------------------------------------------------

        internal void Apply()
        {
            //ChangeLanguage();
            App.MainForm.ApplyMainFormProperties();
        }

        // ---------------------------------------------------------------

        public virtual AppProperties Clone()
        {
            AppProperties props = new AppProperties(app);
            props.List.SetValues(this.List);
            return props;
        }

        public AppViewSkin ViewSkin
        {
            get
            {
                return (AppViewSkin)StrToEnum(List.Get("ViewSkin", AppViewSkin.Black), typeof(AppViewSkin));
            }
            set
            {
                List["ViewSkin"].Value = value;
            }
        }

        public bool ShowConnectFormAtStartup
        {
            get
            {
                return global::System.Convert.ToBoolean(List.Get("ShowConnectFormAtStartup", true));
            }
            set
            {
                List["ShowConnectFormAtStartup"].Value = value;
            }
        }

        private object StrToEnum(object p, Type type)
        {
            return Enum.Parse(type, p.ToString());
        }

    }

    #endregion

    public enum AppViewSkin
    {
        Black = 0,
        Blue = 1,
        Caramel = 2,
        MoneyTwins = 3,
        Lilian = 4,
        iMaginary = 5
    }

}
