using System;

namespace OpenShell
{
    // =================================================================================================

    #region AppState - состояние приложения

    /// <summary>
    /// перечисление AppState - состояние приложения
    /// </summary>
    public enum AppState
    {
        Initializing,
        Running,
        Closing
    }

    #endregion
}
