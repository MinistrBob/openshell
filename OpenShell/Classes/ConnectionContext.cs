﻿// =================================================================================================
//  ConnectionContext.cs
//  Контекст одного соединения в универсальной оболочке
// =================================================================================================

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraNavBar;
using OpenShellClasses;
using OpenShell.Forms;
using Action = OpenShellClasses.Action;

namespace OpenShell
{

    // =================================================================================================

    #region ConnectionContext - класс-обертка УО над соединением

    /// <summary>
    /// ConnectionContext - класс-обертка УО над соединением
    /// </summary>
    internal class ConnectionContext : IConnectionContext
    {
        private Connection connection;
        private AppContext appContext;
        private frmConnection form;
        private ModuleContextList _Modules;
        private NavBarControl _Navigator;
        private ConnectionNavBarGroup _NavGroup;
        private ActionList _Actions;
        private ActionsController _ActionsNavigator;
        private ActionsController _ActionsMainMenu;
        private ActionsController _ActionsMainMenuWorkMenu;
        private ActionsController _ActionsMainToolBar;
        private ConnectionContextBarButtonItem ConnectionBarButtonItem;
        private bool _Active;
        protected BarItemLinksUIActionsController ctl_toolbar;
        private bool _Initializing;
        private ConnectionContextProperties _Properties;
        private ModuleContextList implicitAddedModules;
        public event EventHandler Updated;

        internal ConnectionContext(Connection Connection, AppContext AppContext)
        {
            appContext = AppContext;
            connection = Connection;
            _Modules = new ModuleContextList();
            _Navigator = App.MainForm.Navigator;

            _Actions = new ActionList();
            _ActionsNavigator = new ActionsController();
            _ActionsMainMenu = new ActionsController();
            _ActionsMainMenuWorkMenu = new ActionsController();
            _ActionsMainToolBar = new ActionsController();

            ctl_toolbar = new BarItemLinksUIActionsController(this, App.MainForm.barManager1,
                App.MainForm.barMainToolBar.ItemLinks);
            implicitAddedModules = new ModuleContextList();
        }

        internal virtual void Initialize()
        {
            _Initializing = true;
            _Properties = new ConnectionContextProperties(this);
            form = new frmConnection(this);

            form.Activated += new EventHandler(form_Activated);
            form.Deactivate += new EventHandler(form_Deactivate);

            _NavGroup = new ConnectionNavBarGroup(this);
            _Navigator.ActiveGroupChanged += new NavBarGroupEventHandler(_Navigator_ActiveGroupChanged);
            _Navigator.Groups.Add(_NavGroup);
            _Properties.Fill();
            _NavGroup.Initialize();
            ConnectionBarButtonItem = new ConnectionContextBarButtonItem(this);
            App.Current._MainForm.btnConnections.AddItem(ConnectionBarButtonItem);
            ctl_toolbar.Actions = ActionsMainToolbar;
            LoadImplicitModules();
            LoadModulesForConnection();
            _Properties.Fill();

            _Initializing = false;

            if (!string.IsNullOrEmpty(_Properties.SelectedActionId))
                SelectedActionId = _Properties.SelectedActionId;
            else if (ActionsNavigator.List.Count > 0)
            {
                SelectedActionId = ActionsNavigator.List[0].Id;
            }

            if (CanActivate())
                Activate();
        }

        private void LoadImplicitModules()
        {
            foreach (ModuleContext mc in implicitAddedModules)
            {
                mc.Module.SetContext(mc);
                mc.Module.Load();
            }
        }

        void form_Deactivate(object sender, EventArgs e)
        {
            _Active = false;
            ActiveChanged();
        }

        void form_Activated(object sender, EventArgs e)
        {
            if (Active && ((sender as Form) == form))
                return;

            if (CanActivate())
            {
                _Active = true;
                ActiveChanged();
            }
            else
                App.Current.Context.ActiveConnection.form.Activate();
        }

        private void ActiveChanged()
        {
            if (Active)
                Activate();
            else
                ctl_toolbar.Clear();
        }

        void _Navigator_ActiveGroupChanged(object sender, NavBarGroupEventArgs e)
        {
            if ((e.Group == _NavGroup) && !Active)
            {
                if (CanActivate())
                    Activate();
                else
                    _Navigator.ActiveGroup = App.Current.Context.ActiveConnection._NavGroup;
            }
        }

        public IAppContext AppContext
        {
            get { return appContext; }
        }

        public bool Active
        {
            get { return _Active; }
        }

        public Connection Connection
        {
            get { return connection; }
        }

        public ActionsController ActionsNavigator
        {
            get { return _ActionsNavigator; }
        }

        public frmConnection Form
        {
            get { return form; }
        }

        public virtual void Deinitialize()
        {
            _Properties.NavigatorPanelBottom = (_NavGroup.panel.BottomToolStripPanel.Controls.Count > 0);
            _Properties.Update();
            foreach (ModuleContext mc in Modules)
                mc.Module.Unload();
            if (_Navigator.Groups.IndexOf(_NavGroup) >= 0)
                _Navigator.Groups.Remove(_NavGroup);
            App.Current._MainForm.barManager1.Items.Remove(ConnectionBarButtonItem);
            form.Dispose();
        }

        public ModuleContextList Modules
        {
            get { return _Modules; }
        }

        public ActionsController GetActionsController(ActionPlacement placement)
        {
            if (placement == ActionPlacement.Navigator)
                return ActionsNavigator;
            else if (placement == ActionPlacement.MainMenu)
                return ActionsMainMenu;
            else if (placement == ActionPlacement.MainToolBar)
                return ActionsMainToolbar;
            else
                return null;
        }

        private bool _FormActivatedCall;

        internal bool CanActivate()
        {
            if (appContext.ActiveConnection != this && appContext.ActiveConnection != null)
            {
                if (!appContext.ActiveConnection.CanLeaveCurrentAction())
                {
                    return false;
                }
            }
            return true;
        }

        internal void Activate()
        {
            if (_Initializing) return;

            //if (CanActivate())
            {
                if (!_FormActivatedCall && form != null && _NavGroup != null)
                {
                    _Active = true;
                    _FormActivatedCall = true;
                    Form.Activate();
                    _Navigator.ActiveGroup = _NavGroup;
                    ctl_toolbar.Fill();
                    _FormActivatedCall = false;
                    appContext.ActiveConnection = this;
                }
            }
        }

        #region IConnectionContext Members

        public ActionsController ActionsMainMenu
        {
            get { return _ActionsMainMenu; }
        }

        public ActionsController ActionsMainMenuWorkMenu
        {
            get { return _ActionsMainMenuWorkMenu; }
        }

        public ActionsController ActionsMainToolbar
        {
            get { return _ActionsMainToolBar; }
        }

        #endregion

        #region IConnectionContext Members


        public Page CurrentPage
        {
            get
            {
                return Form.PageContainer.CurrentPage;
            }
            set
            {
                if (Form != null)
                if (Form.PageContainer.CurrentPage != value)
                    Form.PageContainer.CurrentPage = value;
            }
        }

        public Page ConnectionPage { get; set; }

        #endregion

        #region IServiceProvider Members

        public object GetService(Type serviceType)
        {
            return null;
        }

        #endregion

        public Action SelectedAction
        {
            get
            {
                if (_NavGroup != null && _NavGroup.TreeView != null)
                {
                    if (_NavGroup.TreeView.SelectedAction != null)
                        return _NavGroup.TreeView.SelectedAction;
                }
                return null;
            }
        }

        public string SelectedActionId
        {
            get
            {
                if (_NavGroup != null && _NavGroup.TreeView != null)
                {
                    if (_NavGroup.TreeView.SelectedAction != null)
                        return _NavGroup.TreeView.SelectedAction.Id;
                }
                return string.Empty;
            }
            set
            {
                if (!_Initializing)
                {
                    foreach (Action action in ActionsNavigator.GetAllActions())
                        if (action.Id == value)
                        {
                            _NavGroup.TreeView.SelectedAction = action;
                            break;
                        }
                }
            }
        }


        internal void setNavigatorAction(Action Action)
        {
            foreach (Action action in ActionsNavigator.GetAllActions())
            {
                if (action == Action || 
                    ((action is PageAction) && (Action is PageAction) &&
                    (action as PageAction).Page == (Action as PageAction).Page))
                {
                    _NavGroup.TreeView.selectAction(action);
                }
            }
        }

        internal bool CanLeaveCurrentAction()
        {
            if (_NavGroup != null && !_NavGroup.TreeView.CanLeaveCurrentNode())
                return false;
            return true;
        }

        internal void LoadModulesForConnection()
        {
             List<ModuleContext> modules_unload = new List<ModuleContext>();
            foreach (ModuleContext mc in Modules)
                if (!implicitAddedModules.Contains(mc))
                if (!appContext._ModuleConnTypeBindings.Contains(Connection.Type, mc.ModuleType))
                    modules_unload.Add(mc);

            foreach (ModuleContext mc in modules_unload)
            {
                mc.Unload();
                Modules.Remove(mc);
            }

            foreach (ModuleConnetionTypeBinding binding in appContext._ModuleConnTypeBindings)
                if ((binding.ConnectionType == Connection.Type) ||
                    (binding.Inherit && Connection.Type.GetType().IsSubclassOf(binding.ConnectionType.GetType())))
                    if (!Modules.Contains(binding.ModuleType))
                    {
                        IModule module = Activator.CreateInstance(binding.ModuleType) as IModule;
                        if (module != null)
                        {
                            ModuleContext mc = new ModuleContext(this, module,
                                binding.Class);
                            module.SetContext(mc);
                            module.Load();
                            Modules.Add(mc);
                        }
                    }
            ModuleListChanged();
        }

        private void ModuleListChanged()
        {
            if (_NavGroup != null)
            _NavGroup.TreeView.FillTreeList();
        }


        #region IConnectionContext Members

        public RepositoryClass ConnectionClass
        {
            get 
            {
                return AppContext.Repository.Classes.GetConnectionClass(Connection.Type.GetType(), 
                    App.Current.Context.ClassPathLoader);
            }
        }

        public RepositoryStorage GetStorage(string Scope)
        {
            return AppContext.Repository.GetStorage(Connection.RepositoryUserConnection, Scope);
        }

        #endregion

        internal void SelectedActionChanged()
        {
            if (!_Initializing)
            {
                if (SelectedAction != null && SelectedAction.StoreSelectionState)
                    _Properties.SelectedActionId = SelectedActionId;
            }
        }

        public ConnectionContextProperties Properties
        {
            get { return _Properties; }
        }

        #region IConnectionContext Members


        public void AddModule(IModule module)
        {
            ModuleContext mc = new ModuleContext(this, module, null);
            Modules.Add(mc);
            implicitAddedModules.Add(mc);
        }

        #endregion

        public void Update()
        {
            foreach (ModuleContext mc in Modules)
                mc.Update();
            if (Updated != null)
                Updated(this, EventArgs.Empty);
        }
    }

    #endregion

}
