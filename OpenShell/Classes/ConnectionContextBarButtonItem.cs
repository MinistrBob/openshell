using System;
using DevExpress.XtraBars;

namespace OpenShell
{
    // =================================================================================================

    internal class ConnectionContextBarButtonItem : BarButtonItem
    {
        private ConnectionContext _Context;

        public ConnectionContextBarButtonItem(ConnectionContext Context)
        {
            _Context = Context;
            _Context.Updated += new EventHandler(_Context_Updated);
            Caption = Context.Connection.Name;
            if (Context.Connection.Image != null)
            {
                App.Current._MainForm.imageListActions.Images.Add(Context.Connection.Image);
                ImageIndex = App.Current._MainForm.imageListActions.Images.Count - 1;
            }
            ItemClick += new ItemClickEventHandler(ConnectionContextBarButtonItem_ItemClick);
        }

        void _Context_Updated(object sender, EventArgs e)
        {
            if (Caption != Context.Connection.Name)
            {
                Caption = Context.Connection.Name;
                foreach (BarItemLink x in Links)
                {
                    x.Caption = Caption;
                    x.UserCaption = Caption;
                }
            }

        }

        void ConnectionContextBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (Context.CanActivate())
                Context.Activate();
        }

        public ConnectionContext Context
        {
            get { return _Context; }
        }
    }
}
