using System.Collections;
using System.Collections.Generic;
using OpenShellClasses;

namespace OpenShell
{
    // =================================================================================================

    #region ConnectionContext - класс-обертка УО над соединением

    public class ConnectionContextList : IEnumerable
    {
        private ConnectionList _Connections;
        private Dictionary<Connection, ConnectionContext> _List;

        public ConnectionContextList()
        {
            _Connections = new ConnectionList();
            _List = new Dictionary<Connection, ConnectionContext>();
        }

        public ConnectionList Connections
        {
            get { return _Connections; }
        }

        internal void Add(ConnectionContext cc)
        {
            _Connections.Add(cc.Connection);
            _List.Add(cc.Connection, cc);
        }

        internal ConnectionContext GetContext(Connection Connection)
        {
            if (_List.ContainsKey(Connection))
                return _List[Connection];
            else
                return null;
        }

        internal Connection[] ConnectionsArray()
        {
            return _Connections.ToArray();
        }

        internal void Remove(ConnectionContext cc)
        {
            _Connections.Remove(cc.Connection);
            _List.Remove(cc.Connection);
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return _List.Values.GetEnumerator();
        }

        #endregion
    }

    #endregion
}
