﻿// =================================================================================================
//  ConnectionContextNav.cs
//  Интерфейсные элементы, добавляемые в контекст соединения
// =================================================================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Drawing;
using System.Text;
using DevExpress.XtraBars;
using DevExpress.XtraNavBar;
using DevExpress.XtraGrid;
using OpenShellClasses;
using Action = OpenShellClasses.Action;

namespace OpenShell
{

    // =================================================================================================

    internal class ConnectionNavBarGroup : NavBarGroup
    {
        internal ConnectionContext context;
        private ConnectionContextTreeView _TreeView;
        private ToolStrip _ToolStrip;
        private ToolStripButton btnCollapse;
        private ToolStripButton btnExpand;

        public ConnectionNavBarGroup(ConnectionContext context)
        {
            this.context = context;

            context.Updated += new EventHandler(context_Updated);
            this.Caption = context.Connection.Name;
            this.Hint = context.Connection.Hint;
            this.Expanded = true;
            GroupCaptionUseImage = NavBarImage.Small;

            if (context.Connection.Image != null)
            {
                App.MainForm.imageListNavigator.Images.Add(context.Connection.Image);
                int ix = App.MainForm.imageListNavigator.Images.Count - 1;

                this.LargeImageIndex = ix;
                this.SmallImageIndex = ix;
            }


            _TreeView = new ConnectionContextTreeView(this);
            GroupStyle = NavBarGroupStyle.ControlContainer;
            _ToolStrip = new ToolStrip();
            _ToolStrip.Dock = DockStyle.Top;
            _ToolStrip.GripStyle = ToolStripGripStyle.Visible;

            btnCollapse = new ToolStripButton("свернуть");
            btnCollapse.TextImageRelation = TextImageRelation.ImageBeforeText;
            btnCollapse.Image = Resources.Images.FillUp;
            btnCollapse.ImageTransparentColor = Color.Magenta;
            btnCollapse.Click += new EventHandler(btnCollapse_Click);

            btnExpand = new ToolStripButton("развернуть");
            btnExpand.TextImageRelation = TextImageRelation.ImageBeforeText;
            btnExpand.Image = Resources.Images.FillDown;
            btnExpand.ImageTransparentColor = Color.Magenta;
            btnExpand.Click += new EventHandler(btnExpand_Click);

            _ToolStrip.Items.Add(btnCollapse);
            _ToolStrip.Items.Add(btnExpand);
        }

        void context_Updated(object sender, EventArgs e)
        {
            if (Caption != context.Connection.Name)
            {
                Caption = context.Connection.Name;
            }
        }

        void btnCollapse_Click(object sender, EventArgs e)
        {
            _TreeView.CollapseAll();
        }

        void btnExpand_Click(object sender, EventArgs e)
        {
            _TreeView.ExpandAll();
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();
        }

        public ToolStripContainer panel;

        internal void Initialize()
        {
            ControlContainer = new DevExpress.XtraNavBar.NavBarGroupControlContainer();

            panel = new ToolStripContainer();
            panel.Dock = DockStyle.Fill;
            panel.Parent = ControlContainer;
            panel.LeftToolStripPanelVisible = false;
            panel.RightToolStripPanelVisible = false;

            _TreeView.Parent = panel.ContentPanel;
            _TreeView.Dock = DockStyle.Fill;

            _ToolStrip.Height = 20;
            //panel.TopToolStripPanel = new ToolStripPanel();

            if (context.Properties.NavigatorPanelBottom)
                panel.BottomToolStripPanel.Controls.Add(_ToolStrip);
            else
                panel.TopToolStripPanel.Controls.Add(_ToolStrip);
            //_ToolStrip.Parent = panel.ContentPanel;

            this.GroupClientHeight = 500;
        }

        public ConnectionContextTreeView TreeView
        {
            get { return _TreeView as ConnectionContextTreeView; }
        }
    }

    // =================================================================================================

    internal class ConnectionContextTreeView : TreeView
    {
        private ConnectionNavBarGroup navGroup;
        internal ConnectionContext context;
        internal ImageList _ImageList;
        private ActionTreeViewNode currentNode;
        private Dictionary<Action, ActionTreeViewNode> dicNodes;

        public ConnectionContextTreeView(ConnectionNavBarGroup navGroup)
        {
            this.navGroup = navGroup;
            this.context = navGroup.context;
            dicNodes = new Dictionary<Action, ActionTreeViewNode>();

            Dock = System.Windows.Forms.DockStyle.Fill;
            Show();

            _ImageList = new ImageList();
            _ImageList.TransparentColor = System.Drawing.Color.Magenta;

            this.ImageList = _ImageList;

            BeforeSelect += new TreeViewCancelEventHandler(ConnectionContextTreeView_BeforeSelect);
            AfterSelect += new TreeViewEventHandler(ConnectionContextTreeView_AfterSelect);
            BeforeCollapse += new TreeViewCancelEventHandler(ConnectionContextTreeView_BeforeCollapse);

            ShowNodeToolTips = true;
            ShowPlusMinus = true;
            //ShowRootLines = false;
            HideSelection = false;

            //FullRowSelect = false;
            ShowLines = true;

            FillTreeList();
        }

        void ConnectionContextTreeView_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
        {
            //e.Cancel = true;
        }

        void ConnectionContextTreeView_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (_in_select_action) return;
            try
            {
                ActionTreeViewNode node = e.Node as ActionTreeViewNode;
                if (node != null)
                {
                    if (!node.Action.Enabled)
                    {
                        e.Cancel = true;
                        return;
                    }

                    node.Action.Reset();
                    if (currentNode != null)
                        currentNode.Action.Reset();

                    // проверяем, можем ли мы войти в новое действие
                    if (!node.DoCanEnter())
                    {
                        e.Cancel = true;
                        return;
                    }

                    // грамотно выходим из текущей страницы
                    if (!LeaveCurrentNode())
                    {
                        e.Cancel = true;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                e.Cancel = true;
                App.ShowException(ex);
            }
        }

        private bool LeaveCurrentNode()
        {
            if (currentNode != null && CanLeaveCurrentNode())
            {
                currentNode.DoLeave();
                return true;
            }
            else if (currentNode == null)
                return true;
            else
                return false;
        }

        internal bool CanLeaveCurrentNode()
        {
            if (currentNode != null && !currentNode.DoCanLeave())
                return false;
            else
                return true;
        }

        void ConnectionContextTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (_in_select_action) return;
            try
            {
                ActionTreeViewNode node = e.Node as ActionTreeViewNode;
                if (node != null)
                {
                    node.DoEnter();
                }
                currentNode = node;
                context.SelectedActionChanged();
            }
            catch (Exception ex)
            {
                try
                {
                    SelectedNode = currentNode;
                }
                catch
                {
                }
                App.ShowException(ex);
            }
        }

        public void FillTreeList()
        {
            Nodes.Clear();

            foreach (ContainerAction ca in dicNodes.Keys.Where(x => x is ContainerAction))
            {
                ca.ChildChanged -= ContainerAction_ChildChanged;
            }
            

            dicNodes.Clear();
            _FillNodes(Nodes, context.ActionsNavigator.List);

            /*if (Nodes.Count == 0 && context.ConnectionPage != null)
            {
                Nodes.Add(new ActionTreeViewNode(this, 
                    new PageAction(
                        context.Connection.GetType().Name + ":" + 
                        context.Connection.ConnectionPath + ":ConnectionPage")))
            }*/

            ExpandAll();
            context.SelectedActionChanged();
        }

        private void _FillNodes(TreeNodeCollection Nodes, IList<Action> actionsController)
        {
            foreach (Action action in actionsController)
            {
                _AddNodeAction(Nodes, action);
            }
        }

        private void _AddNodeAction(TreeNodeCollection Nodes, Action action)
        {
            var node = new ActionTreeViewNode(this, action);

            if (node.Action.Visible)
                Nodes.Add(node);

            if (!dicNodes.ContainsKey(action))
                dicNodes.Add(action, node);

            if (action is ContainerAction)
            {
                (action as ContainerAction).ChildChanged += ContainerAction_ChildChanged;
                _FillNodes(node.Nodes, (action as ContainerAction).Childs);
            }
        }

        private void RemoveNode(TreeNodeCollection nodes, Action action)
        {
            if (dicNodes.ContainsKey(action))
            {
                var node = dicNodes[action];

                if (action is ContainerAction)
                {
                    var actContainer = (action as ContainerAction);
                    actContainer.ChildChanged -= ContainerAction_ChildChanged;
                    foreach (var a in actContainer.Childs)
                    {
                        RemoveNode(node.Nodes, a);
                    }
                }

                if (nodes.Contains(node))
                    nodes.Remove(node);
                dicNodes.Remove(action);
            }
        }

        void ContainerAction_ChildChanged(object sender, ContainerActionChildChangeEventArgs e)
        {
            var parAction = ((ContainerAction)sender);
            if (e.Change == ListChangedType.ItemAdded)
            {
                if (dicNodes.ContainsKey(parAction) && !dicNodes.ContainsKey(e.Action))
                {
                    var parNode = dicNodes[parAction];
                    _AddNodeAction(parNode.Nodes, e.Action);
                    parNode.ExpandAll();
                }
            }
            else if (e.Change == ListChangedType.ItemDeleted)
            {
                if (dicNodes.ContainsKey(parAction) && dicNodes.ContainsKey(e.Action))
                    RemoveNode(dicNodes[parAction].Nodes, e.Action);
            }
        }

        public Action SelectedAction
        {
            get
            {
                if (SelectedNode != null)
                    return (SelectedNode as ActionTreeViewNode).Action;
                return null;
            }
            set
            {
                _DoSelectAction(value);
            }
        }

        private bool _in_select_action = false;

        internal void selectAction(Action action)
        {
            _in_select_action = true;
            _DoSelectAction(action);
            _in_select_action = false;
        }

        private void _DoSelectAction(Action action)
        {
            if (dicNodes.ContainsKey(action) && LeaveCurrentNode())
            {
                SelectedNode = dicNodes[action];
                currentNode = SelectedNode as ActionTreeViewNode;
            }
        }
    }

    // =================================================================================================

    //internal class ConnectionContextTreeList : TreeList
    //{
    //    private ConnectionNavBarGroup navGroup;
    //    private ConnectionContext context;
    //    internal ImageList _ImageList;

    //    public ConnectionContextTreeList(ConnectionNavBarGroup navGroup)
    //    {
    //        this.navGroup = navGroup;
    //        this.context = navGroup.context;

    //        Dock = System.Windows.Forms.DockStyle.Fill;
    //        Show();

    //        _ImageList = new ImageList();
    //        _ImageList.TransparentColor = System.Drawing.Color.Magenta;

    //        this.

    //        OptionsBehavior.Editable = false;
    //        OptionsView.ShowColumns = false;
    //        OptionsView.ShowHorzLines = false;
    //        OptionsView.ShowIndicator = false;
    //        OptionsView.ShowVertLines = false;
    //        OptionsView.ShowButtons = false;
    //        OptionsSelection.EnableAppearanceFocusedCell = false;
    //        OptionsSelection.EnableAppearanceFocusedRow = true;

    //        DevExpress.XtraTreeList.Columns.TreeListColumn col = Columns.Add();
    //        col.Visible = true;
    //        col.FieldName = "treeListColumn1";
    //        col.Name = "treeListColumn1";
    //        col.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;

    //        this.FocusedRowIndex = -1;
    //        this.AfterFocusNode += new NodeEventHandler(ConnectionContextTreeList_AfterFocusNode);

    //        FillTreeList();
    //    }

    //    void ConnectionContextTreeList_AfterFocusNode(object sender, NodeEventArgs e)
    //    {
    //        (e.Node as ActionTreeListNode).Action.Execute();
    //    }

    //    protected override TreeListNode CreateNode(int nodeID, TreeListNodes owner)
    //    {
    //        return new ActionTreeListNode(owner);
    //    }

    //    public void FillTreeList()
    //    {
    //        ClearNodes();
    //        _FillNodes(Nodes, context.Actions);
    //        ExpandAll();
    //    }

    //    private void _FillNodes(TreeListNodes Nodes, ActionList actionsController)
    //    {
    //        foreach (Action action in actionsController)
    //        {
    //            ActionTreeListNode node = AppendNode(new object[] { action.Name },
    //                (Nodes.ParentNode != null) ? Nodes.ParentNode.Id : -1) as ActionTreeListNode;
    //            node.Initialize(this, action);

    //            if (action is ContainerAction)
    //                _FillNodes(node.Nodes, (action as ContainerAction).Childs);
    //        }
    //    }
    //}

    // =================================================================================================

    public delegate void VoidDelegate();

    internal class ActionTreeViewNode : TreeNode
    {
        private Action _Action;
        private ConnectionContextTreeView _TreeView;
        private int? _SavedIndex;
        private bool _init;

        internal ActionTreeViewNode(ConnectionContextTreeView TreeView, Action Action)
        {
            _init = true;
            _TreeView = TreeView;
            _Action = Action;
            Action.PropertyChanged += new ActionPropertyChangedEventHandler(action_PropertyChanged);
            UpdateImage();
            Update();
            _init = false;
        }

        void action_PropertyChanged(object sender, ActionPropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Image")
                App.MainForm.Invoke(new VoidDelegate(UpdateImage));
            App.MainForm.Invoke(new VoidDelegate(Update));
        }

        private void UpdateImage()
        {
            if (Action.Image != null)
            {
                var imgName = Action.Image.GetMd5Hash();
                if (_TreeView._ImageList.Images.ContainsKey(imgName))
                {
                    ImageIndex = _TreeView._ImageList.Images.IndexOfKey(imgName);
                }
                else
                {
                    _TreeView._ImageList.Images.Add(imgName, Action.Image);
                    ImageIndex = _TreeView._ImageList.Images.Count - 1;
                }
            }
            else
                ImageIndex = 99999;
            this.SelectedImageIndex = ImageIndex;
            _TreeView.Refresh();
        }


        private void Update()
        {
            _TreeView.BeginUpdate();
            try
            {
                Text = _Action.Name;
                ToolTipText = _Action.Hint;

                if (!_init)
                {
                    TreeNodeCollection nodes = TreeView == _TreeView && Parent != null ? Parent.Nodes : _TreeView.Nodes;

                    if (!Action.Visible && nodes.Contains(this))
                    {
                        _SavedIndex = Index;
                        nodes.Remove(this);
                    }

                    if (Action.Visible && !nodes.Contains(this))
                    {
                        if (_SavedIndex.HasValue)
                            nodes.Insert(_SavedIndex.Value, this);
                        else
                            nodes.Add(this);
                    }
                }
            }
            finally
            {
                _TreeView.EndUpdate();
            }
        }

        public Action Action
        {
            get { return _Action; }
        }

        internal bool DoCanLeave()
        {
            return Action.DoCanLeave();
        }

        internal bool DoCanEnter()
        {
            return Action.DoCanEnter();
        }

        internal void DoLeave()
        {
            Action.DoLeave();
            _TreeView.context.Update();
        }

        internal void DoEnter()
        {
            Action.DoEnter();
            _TreeView.context.Update();
        }
    }

    //public class ActionTreeListNode : TreeListNode
    //{
    //    private Action _Action;
    //    private ConnectionContextTreeList _TreeList;

    //    public ActionTreeListNode(TreeListNodes owner) :
    //        base(owner.Count, owner)
    //    {
    //    }

    //    internal void Initialize(ConnectionContextTreeList TreeList, Action Action)
    //    {
    //        _TreeList = TreeList;
    //        _Action = Action;
    //        Action.PropertyChanged += new ActionPropertyChangedEventHandler(action_PropertyChanged);
    //        Update();
    //    }

    //    void action_PropertyChanged(object sender, ActionPropertyChangedEventArgs e)
    //    {
    //        if (e.PropertyName == "Image")
    //            UpdateImage();
    //        Update();
    //    }

    //    private void UpdateImage()
    //    {
    //        throw new NotImplementedException();
    //    }

    //    private void Update()
    //    {
    //        SetValue(0, _Action.Name);
    //    }

    //    public Action Action
    //    {
    //        get { return _Action; }
    //    }
    //}

    // =================================================================================================

    /// <summary>
    /// контроллер отображения действий на панели инструментов
    /// </summary>
    internal class BarItemLinksUIActionsController
    {
        private BarManager barMgr;
        private BarItemLinkCollection barLinks;
        private ActionsController _Actions;
        private List<BarButtonItem> _CreatedButtons;
        private List<BarButtonItemLink> _CreatedButtonLinks;
        private Dictionary<Action, int> _ImageIndexes;
        private Dictionary<Action, int> _ImageIndexesLarge;
        private ConnectionContext _ConnectionContext;

        public BarItemLinksUIActionsController(ConnectionContext context, BarManager barMgr, BarItemLinkCollection barLinks)
        {
            this._ConnectionContext = context;
            _CreatedButtons = new List<BarButtonItem>();
            _CreatedButtonLinks = new List<BarButtonItemLink>();
            _ImageIndexes = new Dictionary<Action, int>();
            _ImageIndexesLarge = new Dictionary<Action, int>();
            this.barLinks = barLinks;
            this.barMgr = barMgr;
        }

        public ActionsController Actions
        {
            get { return _Actions; }
            set
            {
                if (_Actions != value)
                {
                    _Actions = value;
                    Fill();
                }
            }
        }

        public ImageList ImageList
        {
            get
            {
                return barMgr.Images as ImageList;
            }
        }

        public ImageList ImageListLarge
        {
            get
            {
                return barMgr.LargeImages as ImageList;
            }
        }

        internal ConnectionContext ConnectionContext
        {
            get { return _ConnectionContext; }
        }

        public void Fill()
        {
            Clear();
            if (_Actions != null)
                foreach (Action action in _Actions)
                {
                    ActionBarButtonItem item = new ActionBarButtonItem(this, action);
                    barMgr.Items.Add(item);
                    _CreatedButtons.Add(item);

                    BarButtonItemLink link = barLinks.Add(item) as BarButtonItemLink;
                    if (_CreatedButtonLinks.Count == 0)
                        link.BeginGroup = true;
                    _CreatedButtonLinks.Add(link);
                }
        }

        public void Clear()
        {
            foreach (BarButtonItemLink link in _CreatedButtonLinks)
                barLinks.Remove(link);
            _CreatedButtonLinks.Clear();

            foreach (BarButtonItem item in _CreatedButtons)
            {
                if (item is ActionBarButtonItem)
                    (item as ActionBarButtonItem).RemoveEventHandlers();

                barMgr.Items.Remove(item);
            }
            _CreatedButtonLinks.Clear();
        }

        internal int GetImageIndex(Action Action)
        {
            if (ImageList != null && Action.Image != null)
            {
                if (!_ImageIndexes.ContainsKey(Action))
                {
                    ImageList.Images.Add(Action.Image);
                    int new_index = ImageList.Images.Count - 1;
                    _ImageIndexes.Add(Action, new_index);
                }
                return _ImageIndexes[Action];
            }
            return -1;
        }

        internal int GetImageIndexLarge(Action Action)
        {
            if (ImageListLarge != null && Action.ImageLarge != null)
            {
                if (!_ImageIndexesLarge.ContainsKey(Action))
                {
                    ImageListLarge.Images.Add(Action.ImageLarge);
                    int new_index = ImageListLarge.Images.Count - 1;
                    _ImageIndexesLarge.Add(Action, new_index);
                }
                return _ImageIndexesLarge[Action];
            }
            return -1;
        }
    }

    // =================================================================================================

    internal class ActionBarButtonItem : BarLargeButtonItem
    {
        private Action _Action;
        private BarItemLinksUIActionsController _Controller;
        private ActionCancelEventHandler ActionCanEnterHandler;
        private ActionNotifyEventHandler ActionEnterHandler;

        public ActionBarButtonItem(BarItemLinksUIActionsController Controller, Action Action)
        {
            _Controller = Controller;
            _Action = Action;
            _Action.PropertyChanged += new ActionPropertyChangedEventHandler(_Action_PropertyChanged);
            Fill();
            ItemClick += new ItemClickEventHandler(ActionBarButtonItem_ItemClick);
            ActionCanEnterHandler = new ActionCancelEventHandler(Action_CanEnter);
            ActionEnterHandler = new ActionNotifyEventHandler(Action_Enter);
            _Action.CanEnter += ActionCanEnterHandler;
            _Action.Enter += ActionEnterHandler;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

        }

        void Action_CanEnter(object sender, ActionCancelEventArgs e)
        {
            e.Cancel = !_Controller.ConnectionContext.CanLeaveCurrentAction();
        }

        void Action_Enter(object sender, ActionNotifyEventArgs e)
        {
            _Controller.ConnectionContext.setNavigatorAction(Action);
        }

        private void Fill()
        {
            this.Caption = Action.Name;
            this.Hint = Action.Hint;
            this.Enabled = Action.Enabled;
            if (Action.Visible)
                this.Visibility = BarItemVisibility.Always;
            else
                this.Visibility = BarItemVisibility.Never;
            this.ImageIndex = _Controller.GetImageIndex(Action);
            this.LargeImageIndex = _Controller.GetImageIndexLarge(Action);
            this.ShowCaptionOnBar = Action.ShowCaption;
            this.CaptionAlignment = BarItemCaptionAlignment.Right;
        }

        void _Action_PropertyChanged(object sender, ActionPropertyChangedEventArgs e)
        {
            Fill();
            _Controller.ConnectionContext.Update();
        }

        public Action Action
        {
            get { return _Action; }
        }

        void ActionBarButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (Action.DoCanEnter())
                Action.DoEnter();
        }

        internal void RemoveEventHandlers()
        {
            _Action.CanEnter -= ActionCanEnterHandler;
            _Action.Enter -= ActionEnterHandler;
        }
    }

    // =================================================================================================
}
