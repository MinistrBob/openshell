﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenShellClasses;

namespace OpenShell
{
    internal class ConnectionContextProperties 
    {
        private ConnectionContext context;
        private RepositoryStorage storage;
        private PropertyList list;

        public ConnectionContextProperties(ConnectionContext context)
        {
            this.context = context;
            list = new PropertyList();
            storage = context.GetStorage("CONNECTION_PROPERTIES");
        }

        public virtual void Fill()
        {
            list = PropertyList.DeserializeSchema(
                System.Convert.ToString(storage.ReadValue("CONNECTION_PROPERTIES")));
        }

        public virtual void Update()
        {
            storage.WritePropertyValues("CONNECTION_PROPERTIES", list);
        }

        public string SelectedActionId
        {
            get { return list["SelectedActionId"].AsString; }
            set { list["SelectedActionId"].AsString = value; }
        }

        public bool NavigatorPanelBottom
        {
            get { return list["NavigatorPanelBottom"].AsString == "1"; }
            set
            {
                if (value)
                    list["NavigatorPanelBottom"].AsString = "1";
                else
                    list["NavigatorPanelBottom"].AsString = "0";
            }
        }
    }
}
