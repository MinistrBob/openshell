﻿using System;
using System.Xml.Serialization;
using OpenShellClasses;

namespace OpenShell
{
    [Serializable]
    public class LocalSavedRepositoryConnection
    {
        private string _TypeName;
        private string _Name;
        private string _ConnectionPath;
        private DateTime _TimeCreated;
        private DateTime _TimeLogin;
        private string _LoginData;
        private string _User;
        private string _Mode;
        private string _Database;

        public LocalSavedRepositoryConnection()
        {

        }

        public LocalSavedRepositoryConnection(Connection connection)
        {
            _TypeName = connection.Type.GetType().FullName;
            _Name = connection.Name;
            _ConnectionPath = connection.ConnectionPath;
            _TimeCreated = DateTime.Now;
            _TimeLogin = DateTime.Now;
            _LoginData = connection.LoginData;

            _Database = connection.GetPropertyString(ConnectionPropertyStringKind.Database);
            _Mode = connection.GetPropertyString(ConnectionPropertyStringKind.Mode);
            _User = connection.GetPropertyString(ConnectionPropertyStringKind.User);
        }

        [XmlAttribute]
        public string TypeName
        {
            get { return _TypeName; }
            set { _TypeName = value; }
        }

        [XmlAttribute]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        [XmlAttribute]
        public string ConnectionPath
        {
            get { return _ConnectionPath; }
            set { _ConnectionPath = value; }
        }

        [XmlAttribute]
        public DateTime TimeCreated
        {
            get { return _TimeCreated; }
            set { _TimeCreated = value; }
        }
        
        public string LoginData
        {
            get { return _LoginData; }
            set { _LoginData = value; }
        }
        
        [XmlAttribute]
        public DateTime TimeLogin
        {
            get { return _TimeLogin; }
            set { _TimeLogin = value; }
        }

        [XmlAttribute]
        public string Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }

        [XmlAttribute]
        public string User
        {
            get { return _User; }
            set { _User = value; }
        }

        [XmlAttribute]
        public string Database
        {
            get { return _Database; }
            set { _Database = value; }
        }

        internal bool EqualConnection(LocalSavedRepositoryConnection conn)
        {
            return ((string.Compare(TypeName, conn.TypeName, true) == 0) &&
                    (string.Compare(ConnectionPath, conn.ConnectionPath, true) == 0));            
        }

        internal void UpdateProps(LocalSavedRepositoryConnection conn)
        {
            _TypeName = conn._TypeName;
            _Name = conn._Name;
            _ConnectionPath = conn._ConnectionPath;
            _TimeLogin = DateTime.Now;
            _LoginData = conn.LoginData;
            _ConnectionPath = conn.ConnectionPath;
        }
    }
}
