using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OpenShell
{
    [Serializable]
    public class LocalSavedRepositoryConnectionList : List<LocalSavedRepositoryConnection>
    {
        public static void AddToFile(string fileName, LocalSavedRepositoryConnection conn)
        {
            LocalSavedRepositoryConnectionList list = Deserialize(fileName);
            if (list.FindConnection(conn) != null)
            {
                list.FindConnection(conn).UpdateProps(conn);
            }
            else
                list.Insert(0, conn);
            list.Serialize(fileName);
        }

        public void Serialize(string fileName)
        {
            using (StreamWriter sw = new StreamWriter(fileName, false, Encoding.GetEncoding(1251)))
            {
                XmlSerializer xs = new XmlSerializer(GetType());
                xs.Serialize(sw, this);
            }
        }

        private LocalSavedRepositoryConnection FindConnection(LocalSavedRepositoryConnection conn)
        {
            foreach (LocalSavedRepositoryConnection c in this)
                if (c.EqualConnection(conn))
                {
                    return c;
                }
            return null;
        }

        public static LocalSavedRepositoryConnectionList Deserialize(string fileName)
        {
            try
            {
                if (File.Exists(fileName))
                {
                    using (StreamReader sr = new StreamReader(fileName, Encoding.GetEncoding(1251)))
                    {
                        XmlSerializer xs = new XmlSerializer(typeof(LocalSavedRepositoryConnectionList));
                        return xs.Deserialize(new XmlTextReader(sr)) as LocalSavedRepositoryConnectionList;
                    }
                }
            }
            catch
            {
            }
            return new LocalSavedRepositoryConnectionList();
        }
    }
}
