using System;
using System.Drawing;
using System.Windows.Forms;
using OpenShellClasses;

namespace OpenShell
{
    // ==================================================================================================

    public class MainFormProperties
    {
        private AppProperties _AppProperties;
        private PropertyList proplist;

        public MainFormProperties(AppProperties AppProperties)
        {
            _AppProperties = AppProperties;
            proplist = AppProperties.List;
        }

        private object GetValue(string Name, object DefaultValue)
        {
            return proplist.Get("MainForm:" + Name, DefaultValue);
        }

        private void SetValue(string Name, object Value)
        {
            proplist["MainForm:" + Name].Value = Value;
        }

        public FormWindowState WindowState
        {
            get
            {
                return (FormWindowState)StrToEnum(GetValue("WindowState", FormWindowState.Maximized), typeof(FormWindowState));
            }
            set
            {
                SetValue("WindowState", value);
            }
        }

        public Rectangle WindowRect
        {
            get
            {
                return StrToRect(GetValue("Rect", RectToStr(App.MainForm.Bounds)).ToString());
            }
            set
            {
                SetValue("Rect", RectToStr(value));
            }
        }

        public bool ShowNavigator
        {
            get { return System.Convert.ToBoolean(GetValue("ShowNavigator", true)); }
            set { SetValue("ShowNavigator", value); }
        }

        public bool UseMdiTabs
        {
            get { return System.Convert.ToBoolean(GetValue("UseMdiTabs", App.MainForm.UseMdiTabs)); }
            set { SetValue("UseMdiTabs", value); }
        }

        public string DockingManagerState
        {
            get { return System.Convert.ToString(GetValue("DockingManagerState", string.Empty)); }
            set { SetValue("DockingManagerState", value); }
        }

        private string RectToStr(Rectangle value)
        {
            return string.Format("{0};{1};{2};{3}",
                    value.X, value.Y, value.Width, value.Height);
        }

        private Rectangle StrToRect(string value)
        {
            string[] ss = value.Split(';');
            if (ss.Length == 4)
            {
                int x = System.Convert.ToInt32(ss[0]);
                int y = System.Convert.ToInt32(ss[1]);
                int w = System.Convert.ToInt32(ss[2]);
                int h = System.Convert.ToInt32(ss[3]);
                return new Rectangle(x, y, w, h);
            }
            else
                return new Rectangle();
        }

        private object StrToEnum(object p, Type type)
        {
            return Enum.Parse(type, p.ToString());
        }
    }
}
