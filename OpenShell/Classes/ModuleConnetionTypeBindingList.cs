using System;
using System.Collections.Generic;
using OpenShellClasses;

namespace OpenShell
{
    public class ModuleConnetionTypeBindingList : List<ModuleConnetionTypeBinding>
    {
        internal bool Contains(ConnectionType connectionType, Type type)
        {
            return (Find(connectionType, type) != null);
        }

        internal ModuleConnetionTypeBinding Find(ConnectionType connectionType, Type type)
        {
            return Find(delegate(ModuleConnetionTypeBinding binding)
            {
                return ((binding.ModuleType == type) &&
                    ((binding.ConnectionType == connectionType) ||
                    (binding.Inherit && connectionType.GetType().IsSubclassOf(binding.ConnectionType.GetType())))
                    );
            });
        }
    }
}
