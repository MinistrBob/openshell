﻿using System;
using System.Collections.Generic;
using OpenShellClasses;
using Action = OpenShellClasses.Action;

namespace OpenShell
{
    public class ModuleConnetionTypeBinding
    {
        private ConnectionType _ConnectionType;
        private Type _ModuleType;
        private bool _Inherit;
        private RepositoryClass _Class;

        public ModuleConnetionTypeBinding(ConnectionType ConnectionType, Type ModuleType, bool Inherit, RepositoryClass Class)
        {
            _ConnectionType = ConnectionType;
            _ModuleType = ModuleType;
            _Inherit = Inherit;
            _Class = Class;
        }

        public ConnectionType ConnectionType
        {
            get { return _ConnectionType; }
        }

        public Type ModuleType
        {
            get { return _ModuleType; }
        }

        public RepositoryClass Class
        {
            get { return _Class; }
        }

        public bool Inherit
        {
            get { return _Inherit; }
        }
    }

    internal class ModuleContext : IModuleContext
    {
        private ConnectionContext _ConnectionContext;
        private IModule _Module;
        private RepositoryClass _ModuleClass;
        private ActionList _ActionList;

        internal ModuleContext(ConnectionContext cc, IModule module, RepositoryClass ModuleClass)
        {
            _ConnectionContext = cc;
            _Module = module;
            _ModuleClass = ModuleClass;
            _ActionList = new ActionList();
        }

        public Type ModuleType
        {
            get { return _Module.GetType(); }
        }

        public IModule Module
        {
            get { return _Module; }
        }

        #region IModuleContext Members

        public IAppContext AppContext
        {
            get { return _ConnectionContext.AppContext; }
        }

        public IConnectionContext ConnectionContext
        {
            get { return _ConnectionContext; }
        }

        #endregion

        #region IServiceProvider Members

        public object GetService(Type serviceType)
        {
            return null;
        }

        #endregion

        #region IModuleContext Members

        public RepositoryClass ModuleClass
        {
            get { return this._ModuleClass; }
        }

        #endregion

        #region IModuleContext Members

        public void AddAction(Action action)
        {
            CheckActionNotExists(action);
            action.Context = this;
            _ActionList.Add(action);
            action.Controller = (ConnectionContext as ConnectionContext).GetActionsController(action.Placement);
            action.Initialize();
        }

        private void CheckActionNotExists(Action action)
        {            
        }

        #endregion

        internal void Unload()
        {
            Module.Unload();
            foreach (Action action in _ActionList)
            {
                action.Deinitialize();
            }
        }

        public void Update()
        {
            Module.Update();
            foreach (Action action in _ActionList)
                action.Update();
        }
    }

    internal class ModuleContextList : List<ModuleContext>
    {
        internal bool Contains(Type type)
        {
            return (Find(delegate(ModuleContext mc)
            {
                return (mc.ModuleType == type);
            }) != null);
        }
    }
}
