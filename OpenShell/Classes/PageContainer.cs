﻿using System;
using System.Windows.Forms;
using OpenShellClasses;

namespace OpenShell.Classes
{
    public class PageContainer : UserControl
    {
        private Page _CurrentPage;
        private Page _PrevPage;

        internal ConnectionContext Connection { get; set; }

        public Page CurrentPage
        {
            get { return _CurrentPage; }
            set
            {
                SetPage(value);
            }
        }

        private bool _SetPage;

        private void SetPage(Page value)
        {
            try
            {
                if (!_SetPage)
                {
                    try
                    {
                        _PrevPage = _CurrentPage;
                        try
                        {
                            _SetPage = true;
                            if (_CurrentPage != null)
                                if (!ReleaseCurrentPage())
                                    throw new Exception("abort");

                            _CurrentPage = value;

                            if (_CurrentPage != null)
                                SetupCurrentPage();
                        }
                        catch (Exception ex)
                        {
                            _CurrentPage = _PrevPage;
                            DrawPage();
                            if (ex.Message != "abort")
                                throw;
                        }
                    }
                    finally
                    {
                        _SetPage = false;
                    }
                }
            }
            catch (Exception ex)
            {
                App.ShowException(ex);
            }
        }

        private void SetupCurrentPage()
        {
            if (CurrentPage.Control != null)
            {
                DrawPage();
                //Focus();
                //CurrentPage.Control.Focus();
            }
        }

        private void DrawPage()
        {
            Controls.Clear();
            if (CurrentPage != null)
            {
                if (CurrentPage.Control is Form)
                    PrepareFormAndShow((CurrentPage.Control as Form));
                Controls.Add(CurrentPage.Control);
                CurrentPage.Control.Dock = DockStyle.Fill;
                ActiveControl = CurrentPage.Control;
            }
        }

        private void PrepareFormAndShow(Form form)
        {
            form.TopLevel = false;
            form.ShowIcon = false;
            form.ShowInTaskbar = false;
            form.ControlBox = false;
            form.MinimizeBox = false;
            form.Text = string.Empty;
            form.MaximizeBox = false;
            form.FormBorderStyle = FormBorderStyle.None;
            form.Show();
        }

        private bool ReleaseCurrentPage()
        {
            if (CurrentPage != null)
            {
                //if (CurrentPage.Leave(_Connection))
                    Controls.Clear();
                    CurrentPage = null;
            }
            return true;
        }

    }
}
