using OpenShellClasses;

namespace OpenShell
{
    internal class RepositoryConnectionContext : ConnectionContext
    {
        internal RepositoryConnectionContext(Connection Connection, AppContext AppContext)
            : base(Connection, AppContext)
        {
        }

        internal override void Initialize()
        {
            //base.Initialize();
        }

        public virtual void InitializeLoadModules()
        {
            LoadModulesForConnection();
            ctl_toolbar.Actions = ActionsMainToolbar;
            Activate();
        }

        public override void Deinitialize()
        {
            //base.Deinitialize();
        }
    }
}
