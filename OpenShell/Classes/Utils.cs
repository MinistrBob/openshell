﻿using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using DevExpress.XtraBars;

namespace OpenShell
{
    public static class Utils
    {
        public static DevExpress.XtraBars.BarItemVisibility BoolToVisibility(bool value)
        {
            if (value)
                return BarItemVisibility.Always;
            else
                return BarItemVisibility.Never;
        }

        public static string GetMd5Hash(this Image target)
        {
            if (target != null)
            {
                byte[] imgHash =
                    (new MD5CryptoServiceProvider()).ComputeHash(
                        (byte[])(new ImageConverter()).ConvertTo(target, typeof(byte[])));

                var sb = new StringBuilder();
                for (int i = 0; i < imgHash.Length; i++)
                {
                    sb.Append(imgHash[i].ToString("X2"));
                }

                return sb.ToString();
            }
            return string.Empty;
        }
    }

}