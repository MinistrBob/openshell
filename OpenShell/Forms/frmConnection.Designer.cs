﻿namespace OpenShell.Forms
{
    partial class frmConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConnection));
            this.PageContainer = new OpenShell.Classes.PageContainer();
            this.SuspendLayout();
            // 
            // PageContainer
            // 
            this.PageContainer.CurrentPage = null;
            this.PageContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PageContainer.Location = new System.Drawing.Point(0, 0);
            this.PageContainer.Name = "PageContainer";
            this.PageContainer.Size = new System.Drawing.Size(880, 622);
            this.PageContainer.TabIndex = 0;
            // 
            // frmConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(880, 622);
            this.Controls.Add(this.PageContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmConnection";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Соединение";
            this.Activated += new System.EventHandler(this.frmConnection_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmConnection_FormClosing);
            this.Shown += new System.EventHandler(this.frmConnection_Shown);
            this.Resize += new System.EventHandler(this.frmConnection_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        internal OpenShell.Classes.PageContainer PageContainer;

    }
}