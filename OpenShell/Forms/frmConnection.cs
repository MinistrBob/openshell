﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenShellClasses;
using OpenShell.Resources;

namespace OpenShell.Forms
{
    internal partial class frmConnection : Form
    {
        private ConnectionContext _ConnectionContext;

        internal frmConnection(ConnectionContext ConnectionContext)
        {
            _ConnectionContext = ConnectionContext;
            this.MdiParent = App.MainForm;
            InitializeComponent();
            InitializeConnectionForm();
            PageContainer.Connection = ConnectionContext;
            Show();
            App.MainForm.ActivateMdiChild(this);
        }

        private void InitializeConnectionForm()
        {
            Text = Connection.Name;
        }

        public Connection Connection
        {
            get { return _ConnectionContext.Connection; }
        }

        private void frmConnection_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason. WindowsShutDown)
            {
                App.Current.Context.CloseConnection(Connection);
                App.Current.Quit();
            }
            else
                if (App.Current.State == AppState.Running)
                {
                    var result = MessageBox.Show(string.Format("Вы действительно желаете завершить соединение {0}" + " и закрыть универсальную оболочку?", Connection) + Environment.NewLine +
                                                  "\"Да\" - Закрыть программу, со всеми октрытыми соединениями." + Environment.NewLine +
                                                  "\"Нет\" - Закрыть только текущее соединение." + Environment.NewLine +
                                                  "\"Отмена\" - Отменить действие.",
                        Strings.Msg_Confirmation, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning); //, MessageBoxDefaultButton.Button2) == DialogResult.Cancel)
                    if (result == DialogResult.Cancel)
                    {
                        e.Cancel = true;
                    }
                    else if (result == DialogResult.Yes)
                    {
                        App.Current.Context.CloseConnection(Connection);
                        App.Current.Quit();
                    }
                    else
                    {
                        App.Current.Context.CloseConnection(Connection);
                    }
                }
        }

        private void frmConnection_Resize(object sender, EventArgs e)
        {
        }

        private void frmConnection_Activated(object sender, EventArgs e)
        {
            this._ConnectionContext.Activate();
        }

        private void frmConnection_Shown(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
        }
    }
}
