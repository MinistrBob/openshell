﻿// =================================================================================================
//  frmMainForm.cs
//  Главная форма приложения
// =================================================================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using System.Threading;
using System.Globalization;
using OpenShellClasses;
using System.IO;
using System.Xml.Serialization;
using OpenShell.Resources;
using DevExpress.XtraBars.Docking;

namespace OpenShell
{

    // =================================================================================================
    
    #region MainForm - главная форма приложения

    /// <summary>
    /// MainForm - главная форма приложения
    /// </summary>
    public partial class MainForm : Form
    {
        #region закрытые поля

        private App app;
        private Dictionary<Connection, int> imgIndices;
        private bool _NavigatorVisible;
        private bool _NavigatorAutoHide;
        #endregion

        public MainForm(App app)
        {
            this.app = app;
            InitializeComponent();
        }

        private void Initialize()
        {
            if (!DesignMode)
            {
                btnConnections.Glyph = imageListLarge.Images[4];
                Status = string.Empty;
                imgIndices = new Dictionary<Connection, int>();
                if (app.State == AppState.Initializing)
                    timerFixNavigator.Enabled = true;
                _UpdateNavigatorVisibility();
                //navBarControl1.Visible = true;
                app.Context.ConnectionClosed += new ConnectionEventHandler(Context_ConnectionClosed);
                app.Context.ConnectionOpened += new ConnectionEventHandler(Context_ConnectionOpened);

            }
        }

        void Context_ConnectionOpened(object sender, ConnectionEventArgs e)
        {
            _UpdateNavigatorVisibility();
        }

        void panelNavigator_DockChanged(object sender, EventArgs e)
        {
            timerDockChanged.Enabled = true;
        }

        #region свойства


        void Context_ConnectionClosed(object sender, ConnectionEventArgs e)
        {
            _UpdateNavigatorVisibility();
        }

        public string Status
        {
            get { return barStatus.Caption; }
            set { barStatus.Caption = value; }
        }

        public string StatusRepository
        {
            get { return barStatusRepository.Caption; }
            set { barStatusRepository.Caption = value; }
        }

        public bool NavigatorVisible
        {
            get { return _NavigatorVisible; }
            set
            {
                _NavigatorVisible = value;
                _UpdateNavigatorVisibility();
            }
        }

        private void _UpdateNavigatorVisibility()
        {
            if (panelNavigator != null)
            {
                if (App.Current.Context.Connections.Connections.Count > 0)
                {
                    if (!_NavigatorVisible)
                        panelNavigator.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
                    else
                    {
                        if (!_NavigatorAutoHide)
                            panelNavigator.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
                        else
                            panelNavigator.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
                    }
                }
                else
                    panelNavigator.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }

        public bool UseMdiTabs
        {
            get { return (xtraTabbedMdiManager1.MdiParent != null); }
            set
            {
                if (value)
                    xtraTabbedMdiManager1.MdiParent = this;
                else
                    xtraTabbedMdiManager1.MdiParent = null;
            }
        }

        public DevExpress.XtraNavBar.NavBarControl Navigator
        {
            get { return navBarControl1; }
        }

        #endregion

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown)            
                e.Cancel = false;
            else
                e.Cancel = (!App.Current.Actions.DoAskQuit());
        }
   
        public virtual void UpdateUI()
        {
            if (DesignMode)
                return;
            StatusRepository = (app.Context.RepositoryConnected) ? 
                app.Context.Repository.ToString() : OpenShell.Resources.Strings.MainForm_StatusNoRepository;
            barWindows.Enabled = (MdiChildren.Length > 0);
            //barConnectionOpen.Visibility = Utils.BoolToVisibility(!app.Context.RepositoryConnected);
            btnConnectionClose.Enabled = (App.Current.Context.Connections.Connections.Count > 0);
            btnConnections.Enabled = btnConnectionClose.Enabled;
        }

        private void barClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            app.Actions.DoAskQuit();
        }

        private void barCmdAbout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            app.Actions.DoShowAbout();
        }

        private void barManager1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            UpdateUI();
        }

        private void barManager1_EndCustomization(object sender, EventArgs e)
        {
            UpdateUI();
        }

        private void barShowNavigator_ItemClick(object sender, ItemClickEventArgs e)
        {
            NavigatorVisible = barShowNavigator.Down;
            UpdateMainFormProperties();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UpdateUI();
        }

        private void barRepositoryDisconnect_ItemClick(object sender, ItemClickEventArgs e)
        {
            app.Actions.DoConnectionClose();
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            UpdateMainFormProperties();
        }

        public AppViewSkin ViewSkin
        {
            get
            {
                switch (this.defaultLookAndFeel1.LookAndFeel.SkinName)
                {
                    case "Black":
                        return AppViewSkin.Black;
                    case "Blue":
                        return AppViewSkin.Blue;
                    case "Caramel":
                        return AppViewSkin.Caramel;
                    case "iMaginary":
                        return AppViewSkin.iMaginary;
                    case "Lilian":
                        return AppViewSkin.Lilian;
                    case "MoneyTwins":
                        return AppViewSkin.MoneyTwins;
                    default:
                        return AppViewSkin.Black;
                }
            }
            set
            {
                switch (value)
                {
                    case AppViewSkin.Black:
                        defaultLookAndFeel1.LookAndFeel.SkinName = "Black";
                        break;
                    case AppViewSkin.Blue:
                        defaultLookAndFeel1.LookAndFeel.SkinName = "Blue";
                        break;
                    case AppViewSkin.Caramel:
                        defaultLookAndFeel1.LookAndFeel.SkinName = "Caramel";
                        break;
                    case AppViewSkin.iMaginary:
                        defaultLookAndFeel1.LookAndFeel.SkinName = "iMaginary";
                        break;
                    case AppViewSkin.Lilian:
                        defaultLookAndFeel1.LookAndFeel.SkinName = "Lilian";
                        break;
                    case AppViewSkin.MoneyTwins:
                        defaultLookAndFeel1.LookAndFeel.SkinName = "MoneyTwins";
                        break;

                }
            }
        }

        private bool updating;

        private void UpdateMainFormProperties()
        {
            if (!updating && Visible)
            {
                MainFormProperties mp = App.Current.Properties.MainForm;
                mp.WindowState = WindowState;
                mp.WindowRect = Bounds;
                mp.ShowNavigator = NavigatorVisible;
                mp.UseMdiTabs = UseMdiTabs;
                mp.DockingManagerState = DockManagerState;
            }
        }
        
        internal void ApplyMainFormProperties()
        {
            updating = true;
            MainFormProperties mp = App.Current.Properties.MainForm;
            WindowState = mp.WindowState;
            Bounds = mp.WindowRect;
            barShowNavigator.Down = mp.ShowNavigator;
            ViewSkin = App.Current.Properties.ViewSkin;
            navBarControl1.View = 
                new DevExpress.XtraNavBar.ViewInfo.StandardSkinNavigationPaneViewInfoRegistrator(this.defaultLookAndFeel1.LookAndFeel.SkinName);
            UseMdiTabs = mp.UseMdiTabs;
            DockManagerState = mp.DockingManagerState;
            NavigatorVisible = mp.ShowNavigator;
            updating = false;
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            //ApplyMainFormProperties();
        }

        private void MainForm_Move(object sender, EventArgs e)
        {
            UpdateMainFormProperties();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                Initialize();
                // применяем настройки
                App.Current.Properties.Apply();
            }
            UpdateUI();
        }

        void panelNavigator_VisibilityChanged(object sender, DevExpress.XtraBars.Docking.VisibilityChangedEventArgs e)
        {
            if (e.Visibility == DevExpress.XtraBars.Docking.DockVisibility.AutoHide)
                _NavigatorAutoHide = true;
            else if (e.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Visible) 
                _NavigatorAutoHide = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timerFixNavigator.Enabled = false;
            if (app.Properties.ShowConnectFormAtStartup)
                app.Actions.DoConnectionOpen();
        }

        private void xtraTabbedMdiManager1_PageAdded(object sender, DevExpress.XtraTabbedMdi.MdiTabPageEventArgs e)
        {
            if (e.Page.MdiChild is Forms.frmConnection)
            {
                Connection conn = (e.Page.MdiChild as Forms.frmConnection).Connection;
                if (conn.Image != null)
                {
                    if (!imgIndices.ContainsKey(conn))
                        imgIndices.Add(conn, imageListConnections.Images.Add(conn.Image, Color.Magenta));
                    e.Page.ImageIndex = imgIndices[conn];
                }
                e.Page.Tooltip = conn.Hint;
                e.Page.ShowCloseButton = DevExpress.Utils.DefaultBoolean.True; ;
            }
        }

        internal void ActivateMdiChild(OpenShell.Forms.frmConnection frmConnection)
        {
            base.ActivateMdiChild(frmConnection);
        }

        private void btnConnectionOpen_ItemClick(object sender, ItemClickEventArgs e)
        {
            app.Actions.DoConnectionOpen();
        }

        private void btnConnectionClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            app.Actions.DoConnectionClose(); 
        }

        private void btnShelSettings_ItemClick(object sender, ItemClickEventArgs e)
        {
            App.Current.Actions.DoShellSettings();
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            string xmlFile = "MainState.xml";
            dockManager1.SaveLayoutToXml(xmlFile);
        }

        private void barButtonItem5_ItemClick(object sender, ItemClickEventArgs e)
        {
            string xmlFile = "MainState.xml";
            dockManager1.RestoreLayoutFromXml(xmlFile);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
        }

        private string DockManagerState
        {
            get
            {
                //return string.Empty;
                MemoryStream ms = new MemoryStream();
                dockManager1.SaveLayoutToStream(ms);
                return System.Convert.ToBase64String(ms.ToArray());
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    try
                    {
                        MemoryStream ms = new MemoryStream(
                            System.Convert.FromBase64String(value));
                        //panelNavigator.SuspendLayout();
                        //this.SuspendLayout();
                        dockManager1.RestoreLayoutFromStream(ms);
                        _UpdateNavigatorVisibility();
                        //if ((dockManager1.Panels.Count > 0) && (dockManager1.Panels[0] != panelNavigator))
                        //{
                        //    panelNavigator = dockManager1.Panels[0];
                        //}
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void barButtonItem2_ItemClick_1(object sender, ItemClickEventArgs e)
        {
        }

        private void dockManager1_ActiveChildChanged(object sender, DevExpress.XtraBars.Docking.DockPanelEventArgs e)
        {
            UpdateMainFormProperties();
        }

        private void dockManager1_ActivePanelChanged(object sender, DevExpress.XtraBars.Docking.ActivePanelChangedEventArgs e)
        {
            UpdateMainFormProperties();
        }

        private void dockManager1_EndDocking(object sender, DevExpress.XtraBars.Docking.EndDockingEventArgs e)
        {
            UpdateMainFormProperties();
        }

        private void dockManager1_Sizing(object sender, DevExpress.XtraBars.Docking.SizingEventArgs e)
        {
            UpdateMainFormProperties();

        }

        private void barButtonItem2_ItemClick_2(object sender, ItemClickEventArgs e)
        {
            this.panelNavigator.Height = 200;
        }

        private void timerDockChanged_Tick(object sender, EventArgs e)
        {
            if ((panelNavigator.Dock == DockingStyle.Bottom) ||
                 (panelNavigator.Dock == DockingStyle.Top))
            {
                if (panelNavigator.Height > (ClientSize.Height / 2))
                    panelNavigator.Height = 200;    
            }

            if ((panelNavigator.Dock == DockingStyle.Left) ||
                 (panelNavigator.Dock == DockingStyle.Right))
            {
                if (panelNavigator.Width > (ClientSize.Width / 2))
                    panelNavigator.Width = 200;
            }

            timerDockChanged.Enabled = false;
        }

        private void MainForm_DockChanged(object sender, EventArgs e)
        {
        }

        private void panelNavigator_DockChanged_1(object sender, EventArgs e)
        {
            timerDockChanged.Enabled = true;
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            btnConnections.Glyph = imageListLarge.Images[0];
        }

        private void panelNavigator_VisibilityChanged_1(object sender, VisibilityChangedEventArgs e)
        {
        }

        private void dockManager1_Load(object sender, EventArgs e)
        {

        }

        private void barLargeButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            App.Current.Actions.DoRepositoryControl();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }
    }
    #endregion
}
