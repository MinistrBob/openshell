﻿namespace OpenShell
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.imageListActions = new System.Windows.Forms.ImageList(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barMainToolBar = new DevExpress.XtraBars.Bar();
            this.btnConnectionOpen = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnConnectionClose = new DevExpress.XtraBars.BarLargeButtonItem();
            this.btnConnections = new DevExpress.XtraBars.BarSubItem();
            this.btnShelSettings = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barLargeButtonItem2 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barMainMenu = new DevExpress.XtraBars.Bar();
            this.barRepositoryMenu = new DevExpress.XtraBars.BarSubItem();
            this.barClose = new DevExpress.XtraBars.BarButtonItem();
            this.barViewMenu = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            this.barShowNavigator = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barToolbarsListItem1 = new DevExpress.XtraBars.BarToolbarsListItem();
            this.barWindows = new DevExpress.XtraBars.BarSubItem();
            this.barMdiChildrenListItem1 = new DevExpress.XtraBars.BarMdiChildrenListItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barCmdAbout = new DevExpress.XtraBars.BarLargeButtonItem();
            this.barStatusBar = new DevExpress.XtraBars.Bar();
            this.barStatusRepository = new DevExpress.XtraBars.BarStaticItem();
            this.barStatus = new DevExpress.XtraBars.BarStaticItem();
            this.barAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.panelNavigator = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.imageListNavigator = new System.Windows.Forms.ImageList(this.components);
            this.BarLargeButtonItem1 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.BarLargeButtonItem3 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.BarLargeButtonItem4 = new DevExpress.XtraBars.BarLargeButtonItem();
            this.imageListLarge = new System.Windows.Forms.ImageList(this.components);
            this.timerFixNavigator = new System.Windows.Forms.Timer(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.imageListConnections = new System.Windows.Forms.ImageList(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.defaultBarAndDockingController1 = new DevExpress.XtraBars.DefaultBarAndDockingController(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timerDockChanged = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.panelNavigator.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // imageListActions
            // 
            this.imageListActions.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListActions.ImageStream")));
            this.imageListActions.TransparentColor = System.Drawing.Color.Magenta;
            this.imageListActions.Images.SetKeyName(0, "Help.bmp");
            this.imageListActions.Images.SetKeyName(1, "Home.bmp");
            this.imageListActions.Images.SetKeyName(2, "Organizer.bmp");
            this.imageListActions.Images.SetKeyName(3, "Task.bmp");
            this.imageListActions.Images.SetKeyName(4, "about16.bmp");
            this.imageListActions.Images.SetKeyName(5, "exit16.bmp");
            this.imageListActions.Images.SetKeyName(6, "db_settings16_h.bmp");
            this.imageListActions.Images.SetKeyName(7, "home_green16.bmp");
            this.imageListActions.Images.SetKeyName(8, "views16.bmp");
            this.imageListActions.Images.SetKeyName(9, "Control_DataConnector.bmp");
            this.imageListActions.Images.SetKeyName(10, "Control_ImageList.bmp");
            this.imageListActions.Images.SetKeyName(11, "Control_ToolBar.bmp");
            this.imageListActions.Images.SetKeyName(12, "mdi_cascade16.bmp");
            this.imageListActions.Images.SetKeyName(13, "mdi_tilehztl16.bmp");
            this.imageListActions.Images.SetKeyName(14, "mdi_tilevert16.bmp");
            this.imageListActions.Images.SetKeyName(15, "connect16.bmp");
            this.imageListActions.Images.SetKeyName(16, "disconnect16.bmp");
            this.imageListActions.Images.SetKeyName(17, "db16.bmp");
            this.imageListActions.Images.SetKeyName(18, "db_find16_h.bmp");
            this.imageListActions.Images.SetKeyName(19, "db_sql_procedure16.bmp");
            this.imageListActions.Images.SetKeyName(20, "sql_script_new16.bmp");
            this.imageListActions.Images.SetKeyName(21, "table16.bmp");
            this.imageListActions.Images.SetKeyName(22, "view16.bmp");
            this.imageListActions.Images.SetKeyName(23, "prop16.bmp");
            this.imageListActions.Images.SetKeyName(24, "db_settings16.bmp");
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.AllowMoveBarOnToolbar = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.AllowShowToolbarsPopup = false;
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barMainToolBar,
            this.barMainMenu,
            this.barStatusBar});
            this.barManager1.CloseButtonAffectAllTabs = false;
            this.barManager1.Controller = this.barAndDockingController1;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Form = this;
            this.barManager1.Images = this.imageListActions;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStatus,
            this.barStatusRepository,
            this.barClose,
            this.barRepositoryMenu,
            this.barViewMenu,
            this.barSubItem3,
            this.barWindows,
            this.barCmdAbout,
            this.barMdiChildrenListItem1,
            this.barSubItem5,
            this.barToolbarsListItem1,
            this.barShowNavigator,
            this.BarLargeButtonItem1,
            this.BarLargeButtonItem3,
            this.btnConnectionOpen,
            this.btnConnectionClose,
            this.btnShelSettings,
            this.BarLargeButtonItem4,
            this.btnConnections,
            this.barLargeButtonItem2});
            this.barManager1.LargeImages = this.imageListLarge;
            this.barManager1.MainMenu = this.barMainMenu;
            this.barManager1.MaxItemId = 61;
            this.barManager1.StatusBar = this.barStatusBar;
            this.barManager1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barManager1_ItemClick);
            this.barManager1.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.barManager1_ItemClick);
            this.barManager1.EndCustomization += new System.EventHandler(this.barManager1_EndCustomization);
            // 
            // barMainToolBar
            // 
            this.barMainToolBar.BarName = "Tools";
            this.barMainToolBar.DockCol = 0;
            this.barMainToolBar.DockRow = 1;
            this.barMainToolBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barMainToolBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnConnectionOpen),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnConnectionClose),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnConnections, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnShelSettings, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barLargeButtonItem2, DevExpress.XtraBars.BarItemPaintStyle.Standard)});
            this.barMainToolBar.OptionsBar.DisableClose = true;
            this.barMainToolBar.OptionsBar.DisableCustomization = true;
            this.barMainToolBar.OptionsBar.UseWholeRow = true;
            resources.ApplyResources(this.barMainToolBar, "barMainToolBar");
            // 
            // btnConnectionOpen
            // 
            resources.ApplyResources(this.btnConnectionOpen, "btnConnectionOpen");
            this.btnConnectionOpen.Id = 46;
            this.btnConnectionOpen.ImageIndex = 15;
            this.btnConnectionOpen.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1));
            this.btnConnectionOpen.LargeImageIndex = 0;
            this.btnConnectionOpen.Name = "btnConnectionOpen";
            this.btnConnectionOpen.ShowCaptionOnBar = false;
            this.btnConnectionOpen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnConnectionOpen_ItemClick);
            // 
            // btnConnectionClose
            // 
            resources.ApplyResources(this.btnConnectionClose, "btnConnectionClose");
            this.btnConnectionClose.Id = 48;
            this.btnConnectionClose.ImageIndex = 16;
            this.btnConnectionClose.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F2));
            this.btnConnectionClose.LargeImageIndex = 3;
            this.btnConnectionClose.Name = "btnConnectionClose";
            this.btnConnectionClose.ShowCaptionOnBar = false;
            this.btnConnectionClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnConnectionClose_ItemClick);
            // 
            // btnConnections
            // 
            resources.ApplyResources(this.btnConnections, "btnConnections");
            this.btnConnections.Id = 51;
            this.btnConnections.ImageIndex = 9;
            this.btnConnections.LargeGlyph = global::OpenShell.Properties.Resources.db_sql_procedure24;
            this.btnConnections.LargeImageIndex = 4;
            this.btnConnections.Name = "btnConnections";
            this.btnConnections.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // btnShelSettings
            // 
            resources.ApplyResources(this.btnShelSettings, "btnShelSettings");
            this.btnShelSettings.Id = 49;
            this.btnShelSettings.ImageIndex = 23;
            this.btnShelSettings.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.J));
            this.btnShelSettings.LargeImageIndex = 6;
            this.btnShelSettings.Name = "btnShelSettings";
            this.btnShelSettings.ShowCaptionOnBar = false;
            this.btnShelSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnShelSettings_ItemClick);
            // 
            // barLargeButtonItem2
            // 
            resources.ApplyResources(this.barLargeButtonItem2, "barLargeButtonItem2");
            this.barLargeButtonItem2.Id = 60;
            this.barLargeButtonItem2.ImageIndex = 24;
            this.barLargeButtonItem2.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.M));
            this.barLargeButtonItem2.LargeImageIndex = 5;
            this.barLargeButtonItem2.Name = "barLargeButtonItem2";
            this.barLargeButtonItem2.ShowCaptionOnBar = false;
            this.barLargeButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barLargeButtonItem2_ItemClick);
            // 
            // barMainMenu
            // 
            this.barMainMenu.BarName = "Main menu";
            this.barMainMenu.DockCol = 0;
            this.barMainMenu.DockRow = 0;
            this.barMainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barMainMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barRepositoryMenu, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barViewMenu, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItem3, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barClose, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barMainMenu.OptionsBar.DisableClose = true;
            this.barMainMenu.OptionsBar.DrawDragBorder = false;
            this.barMainMenu.OptionsBar.MultiLine = true;
            this.barMainMenu.OptionsBar.UseWholeRow = true;
            resources.ApplyResources(this.barMainMenu, "barMainMenu");
            // 
            // barRepositoryMenu
            // 
            resources.ApplyResources(this.barRepositoryMenu, "barRepositoryMenu");
            this.barRepositoryMenu.Id = 14;
            this.barRepositoryMenu.ImageIndex = 7;
            this.barRepositoryMenu.LargeImageIndex = 0;
            this.barRepositoryMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnConnectionOpen, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnConnectionClose),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnShelSettings, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barLargeButtonItem2, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barClose, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barRepositoryMenu.Name = "barRepositoryMenu";
            // 
            // barClose
            // 
            this.barClose.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            resources.ApplyResources(this.barClose, "barClose");
            this.barClose.Id = 5;
            this.barClose.ImageIndex = 5;
            this.barClose.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4));
            this.barClose.Name = "barClose";
            this.barClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barClose_ItemClick);
            // 
            // barViewMenu
            // 
            resources.ApplyResources(this.barViewMenu, "barViewMenu");
            this.barViewMenu.Id = 15;
            this.barViewMenu.ImageIndex = 8;
            this.barViewMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnConnections),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barSubItem5, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barWindows)});
            this.barViewMenu.Name = "barViewMenu";
            // 
            // barSubItem5
            // 
            resources.ApplyResources(this.barSubItem5, "barSubItem5");
            this.barSubItem5.Id = 25;
            this.barSubItem5.ImageIndex = 11;
            this.barSubItem5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barShowNavigator),
            new DevExpress.XtraBars.LinkPersistInfo(this.barToolbarsListItem1)});
            this.barSubItem5.Name = "barSubItem5";
            // 
            // barShowNavigator
            // 
            this.barShowNavigator.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            resources.ApplyResources(this.barShowNavigator, "barShowNavigator");
            this.barShowNavigator.Down = true;
            this.barShowNavigator.Id = 30;
            this.barShowNavigator.Name = "barShowNavigator";
            this.barShowNavigator.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barShowNavigator_ItemClick);
            // 
            // barToolbarsListItem1
            // 
            resources.ApplyResources(this.barToolbarsListItem1, "barToolbarsListItem1");
            this.barToolbarsListItem1.Id = 26;
            this.barToolbarsListItem1.Name = "barToolbarsListItem1";
            // 
            // barWindows
            // 
            resources.ApplyResources(this.barWindows, "barWindows");
            this.barWindows.Id = 20;
            this.barWindows.ImageIndex = 10;
            this.barWindows.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barMdiChildrenListItem1, true)});
            this.barWindows.Name = "barWindows";
            // 
            // barMdiChildrenListItem1
            // 
            resources.ApplyResources(this.barMdiChildrenListItem1, "barMdiChildrenListItem1");
            this.barMdiChildrenListItem1.Id = 24;
            this.barMdiChildrenListItem1.Name = "barMdiChildrenListItem1";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            resources.ApplyResources(this.barSubItem3, "barSubItem3");
            this.barSubItem3.Id = 16;
            this.barSubItem3.ImageIndex = 4;
            this.barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barCmdAbout)});
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barCmdAbout
            // 
            resources.ApplyResources(this.barCmdAbout, "barCmdAbout");
            this.barCmdAbout.Id = 21;
            this.barCmdAbout.ImageIndex = 0;
            this.barCmdAbout.Name = "barCmdAbout";
            this.barCmdAbout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barCmdAbout_ItemClick);
            // 
            // barStatusBar
            // 
            this.barStatusBar.BarName = "StatusBar";
            this.barStatusBar.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatusBar.DockCol = 0;
            this.barStatusBar.DockRow = 0;
            this.barStatusBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatusBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStatusRepository),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStatus)});
            this.barStatusBar.OptionsBar.AllowQuickCustomization = false;
            this.barStatusBar.OptionsBar.DrawDragBorder = false;
            this.barStatusBar.OptionsBar.DrawSizeGrip = true;
            this.barStatusBar.OptionsBar.UseWholeRow = true;
            resources.ApplyResources(this.barStatusBar, "barStatusBar");
            // 
            // barStatusRepository
            // 
            this.barStatusRepository.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            resources.ApplyResources(this.barStatusRepository, "barStatusRepository");
            this.barStatusRepository.Id = 3;
            this.barStatusRepository.Name = "barStatusRepository";
            this.barStatusRepository.TextAlignment = System.Drawing.StringAlignment.Center;
            // 
            // barStatus
            // 
            this.barStatus.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.barStatus.Id = 2;
            this.barStatus.Name = "barStatus";
            this.barStatus.TextAlignment = System.Drawing.StringAlignment.Near;
            resources.ApplyResources(this.barStatus, "barStatus");
            // 
            // barAndDockingController1
            // 
            this.barAndDockingController1.LookAndFeel.SkinName = "Black";
            this.barAndDockingController1.LookAndFeel.UseWindowsXPTheme = true;
            this.barAndDockingController1.PaintStyleName = "Skin";
            this.barAndDockingController1.PropertiesBar.AllowLinkLighting = false;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            resources.ApplyResources(this.barDockControlTop, "barDockControlTop");
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            resources.ApplyResources(this.barDockControlBottom, "barDockControlBottom");
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            resources.ApplyResources(this.barDockControlLeft, "barDockControlLeft");
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            resources.ApplyResources(this.barDockControlRight, "barDockControlRight");
            // 
            // dockManager1
            // 
            this.dockManager1.Controller = this.barAndDockingController1;
            this.dockManager1.DockingOptions.ShowCloseButton = false;
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.panelNavigator});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            this.dockManager1.Load += new System.EventHandler(this.dockManager1_Load);
            this.dockManager1.ActivePanelChanged += new DevExpress.XtraBars.Docking.ActivePanelChangedEventHandler(this.dockManager1_ActivePanelChanged);
            this.dockManager1.ClosedPanel += new DevExpress.XtraBars.Docking.DockPanelEventHandler(this.dockManager1_ActiveChildChanged);
            this.dockManager1.ActiveChildChanged += new DevExpress.XtraBars.Docking.DockPanelEventHandler(this.dockManager1_ActiveChildChanged);
            this.dockManager1.EndDocking += new DevExpress.XtraBars.Docking.EndDockingEventHandler(this.dockManager1_EndDocking);
            this.dockManager1.Sizing += new DevExpress.XtraBars.Docking.SizingEventHandler(this.dockManager1_Sizing);
            // 
            // panelNavigator
            // 
            this.panelNavigator.Controls.Add(this.dockPanel1_Container);
            this.panelNavigator.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.panelNavigator.ID = new System.Guid("eb0e543f-f346-478a-bf02-e7d8850321fb");
            resources.ApplyResources(this.panelNavigator, "panelNavigator");
            this.panelNavigator.Name = "panelNavigator";
            this.panelNavigator.Options.AllowDockFill = false;
            this.panelNavigator.Options.ShowCloseButton = false;
            this.panelNavigator.Options.ShowMaximizeButton = false;
            this.panelNavigator.OriginalSize = new System.Drawing.Size(200, 200);
            this.panelNavigator.VisibilityChanged += new DevExpress.XtraBars.Docking.VisibilityChangedEventHandler(this.panelNavigator_VisibilityChanged_1);
            this.panelNavigator.DockChanged += new System.EventHandler(this.panelNavigator_DockChanged_1);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.navBarControl1);
            resources.ApplyResources(this.dockPanel1_Container, "dockPanel1_Container");
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = null;
            this.navBarControl1.AllowDrop = false;
            this.navBarControl1.AllowSelectedLink = true;
            this.navBarControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.navBarControl1.ContentButtonHint = null;
            resources.ApplyResources(this.navBarControl1, "navBarControl1");
            this.navBarControl1.EachGroupHasSelectedLink = true;
            this.navBarControl1.HideGroupCaptions = true;
            this.navBarControl1.LargeImages = this.imageListNavigator;
            this.navBarControl1.LookAndFeel.SkinName = "Blue";
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.NavigationPaneGroupClientHeight = 200;
            this.navBarControl1.OptionsNavPane.ExpandedWidth = ((int)(resources.GetObject("resource.ExpandedWidth")));
            this.navBarControl1.OptionsNavPane.ShowExpandButton = false;
            this.navBarControl1.OptionsNavPane.ShowSplitter = false;
            this.navBarControl1.ShowGroupHint = false;
            this.navBarControl1.SmallImages = this.imageListNavigator;
            this.navBarControl1.View = new DevExpress.XtraNavBar.ViewInfo.StandardSkinExplorerBarViewInfoRegistrator("Black");
            // 
            // imageListNavigator
            // 
            this.imageListNavigator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListNavigator.ImageStream")));
            this.imageListNavigator.TransparentColor = System.Drawing.Color.Magenta;
            this.imageListNavigator.Images.SetKeyName(0, "Help.bmp");
            this.imageListNavigator.Images.SetKeyName(1, "Home.bmp");
            this.imageListNavigator.Images.SetKeyName(2, "Organizer.bmp");
            this.imageListNavigator.Images.SetKeyName(3, "Task.bmp");
            this.imageListNavigator.Images.SetKeyName(4, "about16.bmp");
            this.imageListNavigator.Images.SetKeyName(5, "exit16.bmp");
            this.imageListNavigator.Images.SetKeyName(6, "db_settings16_h.bmp");
            this.imageListNavigator.Images.SetKeyName(7, "home_green16.bmp");
            this.imageListNavigator.Images.SetKeyName(8, "views16.bmp");
            this.imageListNavigator.Images.SetKeyName(9, "Control_DataConnector.bmp");
            this.imageListNavigator.Images.SetKeyName(10, "Control_ImageList.bmp");
            this.imageListNavigator.Images.SetKeyName(11, "Control_ToolBar.bmp");
            this.imageListNavigator.Images.SetKeyName(12, "mdi_cascade16.bmp");
            this.imageListNavigator.Images.SetKeyName(13, "mdi_tilehztl16.bmp");
            this.imageListNavigator.Images.SetKeyName(14, "mdi_tilevert16.bmp");
            this.imageListNavigator.Images.SetKeyName(15, "connect16.bmp");
            this.imageListNavigator.Images.SetKeyName(16, "disconnect16.bmp");
            this.imageListNavigator.Images.SetKeyName(17, "prop16.bmp");
            this.imageListNavigator.Images.SetKeyName(18, "sql_script_new16.bmp");
            // 
            // BarLargeButtonItem1
            // 
            this.BarLargeButtonItem1.Id = 37;
            this.BarLargeButtonItem1.Name = "BarLargeButtonItem1";
            // 
            // BarLargeButtonItem3
            // 
            this.BarLargeButtonItem3.Id = 39;
            this.BarLargeButtonItem3.Name = "BarLargeButtonItem3";
            // 
            // BarLargeButtonItem4
            // 
            this.BarLargeButtonItem4.Id = 50;
            this.BarLargeButtonItem4.Name = "BarLargeButtonItem4";
            // 
            // imageListLarge
            // 
            this.imageListLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLarge.ImageStream")));
            this.imageListLarge.TransparentColor = System.Drawing.Color.Magenta;
            this.imageListLarge.Images.SetKeyName(0, "connect24.bmp");
            this.imageListLarge.Images.SetKeyName(1, "db24.bmp");
            this.imageListLarge.Images.SetKeyName(2, "db_register24.bmp");
            this.imageListLarge.Images.SetKeyName(3, "disconnect24.bmp");
            this.imageListLarge.Images.SetKeyName(4, "db_sql_procedure24.bmp");
            this.imageListLarge.Images.SetKeyName(5, "db_settings24.bmp");
            this.imageListLarge.Images.SetKeyName(6, "prop24.bmp");
            // 
            // timerFixNavigator
            // 
            this.timerFixNavigator.Interval = 500;
            this.timerFixNavigator.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "iMaginary";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageAndTabControlHeader;
            this.xtraTabbedMdiManager1.Controller = this.barAndDockingController1;
            this.xtraTabbedMdiManager1.HeaderButtons = ((DevExpress.XtraTab.TabButtons)(((DevExpress.XtraTab.TabButtons.Prev | DevExpress.XtraTab.TabButtons.Next) 
            | DevExpress.XtraTab.TabButtons.Close)));
            this.xtraTabbedMdiManager1.Images = this.imageListConnections;
            this.xtraTabbedMdiManager1.MdiParent = null;
            this.xtraTabbedMdiManager1.ShowHeaderFocus = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabbedMdiManager1.ShowToolTips = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabbedMdiManager1.PageAdded += new DevExpress.XtraTabbedMdi.MdiTabPageEventHandler(this.xtraTabbedMdiManager1_PageAdded);
            // 
            // imageListConnections
            // 
            this.imageListConnections.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            resources.ApplyResources(this.imageListConnections, "imageListConnections");
            this.imageListConnections.TransparentColor = System.Drawing.Color.Magenta;
            // 
            // splitter1
            // 
            resources.ApplyResources(this.splitter1, "splitter1");
            this.splitter1.Name = "splitter1";
            this.splitter1.TabStop = false;
            // 
            // defaultBarAndDockingController1
            // 
            // 
            // 
            // 
            this.defaultBarAndDockingController1.Controller.LookAndFeel.SkinName = "Money Twins";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            resources.ApplyResources(this.contextMenuStrip1, "contextMenuStrip1");
            // 
            // timerDockChanged
            // 
            this.timerDockChanged.Tick += new System.EventHandler(this.timerDockChanged_Tick);
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panelNavigator);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.DockChanged += new System.EventHandler(this.MainForm_DockChanged);
            this.Move += new System.EventHandler(this.MainForm_Move);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barAndDockingController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.panelNavigator.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Bar barMainMenu;
        private DevExpress.XtraBars.Bar barStatusBar;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarStaticItem barStatus;
        private DevExpress.XtraBars.BarStaticItem barStatusRepository;
        private DevExpress.XtraBars.BarButtonItem barClose;
        private DevExpress.XtraBars.BarAndDockingController barAndDockingController1;
        private DevExpress.XtraBars.BarSubItem barRepositoryMenu;
        private DevExpress.XtraBars.BarSubItem barViewMenu;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarSubItem barWindows;
        private DevExpress.XtraBars.BarLargeButtonItem barCmdAbout;
        private DevExpress.XtraBars.BarMdiChildrenListItem barMdiChildrenListItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem5;
        private DevExpress.XtraBars.BarToolbarsListItem barToolbarsListItem1;
        private DevExpress.XtraBars.BarLargeButtonItem barShowNavigator;
        private System.Windows.Forms.Timer timerFixNavigator;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private System.Windows.Forms.ImageList imageListConnections;
        private System.Windows.Forms.Splitter splitter1;
        internal System.Windows.Forms.ImageList imageListNavigator;
        private DevExpress.XtraBars.BarLargeButtonItem BarLargeButtonItem1;
        private DevExpress.XtraBars.BarLargeButtonItem BarLargeButtonItem3;
        private DevExpress.XtraBars.DefaultBarAndDockingController defaultBarAndDockingController1;
        private System.Windows.Forms.ImageList imageListLarge;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private DevExpress.XtraBars.BarLargeButtonItem btnConnectionOpen;
        private DevExpress.XtraBars.BarLargeButtonItem btnConnectionClose;
        private DevExpress.XtraBars.BarLargeButtonItem btnShelSettings;
        private DevExpress.XtraBars.BarLargeButtonItem BarLargeButtonItem4;
        internal DevExpress.XtraBars.BarSubItem btnConnections;
        internal System.Windows.Forms.ImageList imageListActions;
        internal DevExpress.XtraBars.BarManager barManager1;
        internal DevExpress.XtraBars.Bar barMainToolBar;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Timer timerDockChanged;
        private DevExpress.XtraBars.Docking.DockPanel panelNavigator;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private DevExpress.XtraBars.BarLargeButtonItem barLargeButtonItem2;
    }
}

