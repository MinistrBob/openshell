﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenShellClasses;
using OpenShell;
using OpenShell.Resources;

namespace OpenShell.Forms
{
    public partial class frmNewConnection : Form
    {
        private bool _ConnectToRepository;
        private ImageList imageList;
        internal Dictionary<ConnectionType, int> imgListIndices;
        private Control loginControl;
        internal Connection NewConnection;
        //private object loginData;
        private bool _filling;
        private ListViewItemSorter _ListViewItemSorter;

        public frmNewConnection()
        {
            InitializeComponent();
            imageList = new ImageList();
            imageList.TransparentColor = Color.Magenta;
            imgListIndices = new Dictionary<ConnectionType, int>();
            _ConnectToRepository = !App.Current.Context.RepositoryConnected;

            _ListViewItemSorter = new ListViewItemSorter();
            _ListViewItemSorter.ColumnIndex = 5;
            _ListViewItemSorter.Descending = true;

            listView1.ListViewItemSorter = _ListViewItemSorter;
            listView1.Sort();

            FillConnectionTypeList();
            FillUserConnectonList();

            ActiveControl = cbConnectionTypeList;
            if (cbConnectionTypeList.Items.Count <= 1)
                ActiveControl = loginControl;
        }

        private void FillUserConnectonList()
        {
            _filling = true;
            
            listView1.SmallImageList = imageList;
            listView1.Items.Clear();

            this.repositoryUserConnectionBindingSource.DataSource = App.Current.Context.Repository.GetUserConnectionList();
            repositoryUserConnectionBindingSource.ResetBindings(false);
            repositoryUserConnectionBindingSource.Position = -1;

            if (!_ConnectToRepository)
            {
                RepositoryUserConnectionList list = App.Current.Context.Repository.GetUserConnectionList();
                foreach (RepositoryUserConnection uc in list)
                {
                    listView1.Items.Add(new UserConnectionListViewItem(uc, this));
                }
            }
            else
            {
                LocalSavedRepositoryConnectionList list =
                    LocalSavedRepositoryConnectionList.Deserialize(App.Current.Properties.LocalRepositoryConnectionsFile);
                foreach (LocalSavedRepositoryConnection conn in list)
                {
                    listView1.Items.Add(new LocalSavedConnectionListViewItem(conn, this));
                }
            }

            _filling = false;
        }

        private void FillConnectionTypeList()
        {
            imageList.Images.Clear();
            imgListIndices.Clear();
            cbConnectionTypeList.Items.Clear();
            foreach (ConnectionType ct in AppContext.Current.ConnectionsTypes)
                if (IsAllowedConnectionType(ct))
                {
                    cbConnectionTypeList.Items.Add(ct);
                    if (ct.Image != null)
                    {
                        imageList.Images.Add(ct.Image);
                        imgListIndices.Add(ct, imageList.Images.Count - 1);
                    }
                }
            if (cbConnectionTypeList.Items.Count > 0)
                cbConnectionTypeList.SelectedItem = cbConnectionTypeList.Items[0];
            UpdateUI();
        }

        private bool IsAllowedConnectionType(ConnectionType ct)
        {
            if (_ConnectToRepository)
                return (ConnectionTypeRepositoryAttribute.IsDefined(ct));
            else
                return (!ConnectionTypeRepositoryAttribute.IsDefined(ct));
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            DoConnect();
        }

        private void DoConnect()
        {
            try
            {
                if (Selected == null)
                    throw new ArgumentException("Не выбран тип соединения!");

                if (Selected.LoginControl_Validate(loginControl))
                {
                    //loginData = Selected.LoginControl_GetData(loginControl);
                    NewConnection = Selected.CreateConnection(loginControl);
                    NewConnection.PersistInfo = cbSaveConnection.Checked;
                    NewConnection.LoginData = Selected.LoginControl_GetData(loginControl);
                    DialogResult = DialogResult.OK;
                }
            }
            catch (Exception ex)
            {
                App.ShowException(ex);
            }
        }

        private void cbConnectionTypeList_SelectedValueChanged(object sender, EventArgs e)
        {
            ConnectionTypeChanged();
        }

        private void ConnectionTypeChanged()
        {
            UpdateLoginPanel();
            UpdateUI();
            //listView1.SelectedIndices.Clear();
        }

        public ConnectionType Selected
        {
            get
            {
                if (cbConnectionTypeList.SelectedItem != null)
                    return (cbConnectionTypeList.SelectedItem as ConnectionType);
                else
                    return null;
            }
        }

        private void UpdateLoginPanel()
        {
            pnlLogin.Controls.Clear();
            Control loginControl;
            if ((Selected != null) && ((loginControl = Selected.GetLoginControl()) != null))
            {
                loginControl.Dock = DockStyle.Fill;
                pnlLogin.Controls.Add(loginControl);
                this.loginControl = loginControl;
            }
            else
                this.loginControl = null;
        }

        private void UpdateUI()
        {
            if (cbConnectionTypeList.SelectedItem != null)
                lblConnectionType.Text = (cbConnectionTypeList.SelectedItem as IDisplayItem).Hint;
            else
                lblConnectionType.Text = Strings.frmNewConnection_ConnectionTypeNotSelected;
        }

        private void cbConnectionTypeList_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index >= 0)
            {
                e.DrawBackground();
                ConnectionType item = cbConnectionTypeList.Items[e.Index] as ConnectionType;
                Point drawTextPoint = e.Bounds.Location;
                drawTextPoint.Y += 1;

                if (imgListIndices.ContainsKey(item))
                {
                    imageList.Draw(e.Graphics, e.Bounds.Location, imgListIndices[item]);
                    drawTextPoint.X += imageList.ImageSize.Width;
                }

                e.Graphics.DrawString(item.ToString(), e.Font, new SolidBrush(e.ForeColor), drawTextPoint);
            }
        }

        private void repositoryUserConnectionBindingSource_CurrentItemChanged(object sender, EventArgs e)
        {
            if (_filling) return;
            RepositoryUserConnection conn = (this.repositoryUserConnectionBindingSource.Current as RepositoryUserConnection);
            if (conn != null)
            {
                ConnectionType selectedConnectionType = null;
                foreach (ConnectionType ct in cbConnectionTypeList.Items)
                    if (ct.ClassId == conn.ConnectionTypeClass.Code)
                    {
                        selectedConnectionType = ct;
                        break;
                    }
                if (selectedConnectionType != null)
                {
                    cbConnectionTypeList.SelectedItem = selectedConnectionType;
                    cbConnectionTypeList_SelectedValueChanged(cbConnectionTypeList, EventArgs.Empty);

                    if (loginControl != null)
                    {
                        Selected.LoginControl_SetData(loginControl, conn.LoginData);
                    }
                }
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DoConnect();
        }

        internal ConnectionType GetConnectionType(RepositoryUserConnection conn)
        {
            foreach (ConnectionType ct in cbConnectionTypeList.Items)
                if (ct.ClassId == conn.ConnectionTypeClass.Code)
                    return ct;
            return null;
        }

        internal int GetImageIndex(ConnectionType ct)
        {
            if (imgListIndices.ContainsKey(ct))
                return imgListIndices[ct];
            else
                return -1;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                if (_filling) return;

                if (listView1.SelectedItems[0] is UserConnectionListViewItem)
                {
                    RepositoryUserConnection conn = ((listView1.SelectedItems[0] as UserConnectionListViewItem).Connection);
                    if (conn != null)
                    {
                        ConnectionType selectedConnectionType = null;
                        foreach (ConnectionType ct in cbConnectionTypeList.Items)
                            if (ct.ClassId == conn.ConnectionTypeClass.Code)
                            {
                                selectedConnectionType = ct;
                                break;
                            }
                        if (selectedConnectionType != null)
                        {
                            cbConnectionTypeList.SelectedItem = selectedConnectionType;
                            cbConnectionTypeList_SelectedValueChanged(cbConnectionTypeList, EventArgs.Empty);

                            if (loginControl != null)
                            {
                                Selected.LoginControl_SetData(loginControl, conn.LoginData);
                            }
                        }
                        //listView1.SelectedIndices.Clear();
                    }
                }
                else if (listView1.SelectedItems[0] is LocalSavedConnectionListViewItem)
                {
                    LocalSavedRepositoryConnection conn = ((listView1.SelectedItems[0] as LocalSavedConnectionListViewItem).Connection);
                    if (conn != null)
                    {
                        ConnectionType selectedConnectionType = null;
                        foreach (ConnectionType ct in cbConnectionTypeList.Items)
                            if ((listView1.SelectedItems[0] as LocalSavedConnectionListViewItem).ConnectionType == ct)
                            {
                                selectedConnectionType = ct;
                                break;
                            }
                        if (selectedConnectionType != null)
                        {
                            cbConnectionTypeList.SelectedItem = selectedConnectionType;
                            cbConnectionTypeList_SelectedValueChanged(cbConnectionTypeList, EventArgs.Empty);

                            if (loginControl != null)
                            {
                                Selected.LoginControl_SetData(loginControl, conn.LoginData);
                            }
                        }
                        //listView1.SelectedIndices.Clear();
                    }
                }
            }

        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            DoConnect();
        }

        internal ConnectionType GetLocalConnectionType(LocalSavedRepositoryConnection conn)
        {
            foreach (ConnectionType ct in cbConnectionTypeList.Items)
                if (ct.GetType().FullName == conn.TypeName)
                    return ct;
            return null;
        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column != _ListViewItemSorter.ColumnIndex)
            {
                _ListViewItemSorter.ColumnIndex = e.Column;
                _ListViewItemSorter.Descending = false;
            }
            else
                _ListViewItemSorter.Descending = !_ListViewItemSorter.Descending;
            listView1.Sort();

        }

        public class ListViewItemSorter : IComparer
        {
            public int ColumnIndex;
            public bool Descending;

            #region IComparer Members

            public int Compare(object x, object y)
            {
                UserConnectionListViewItem ux = x as UserConnectionListViewItem;
                UserConnectionListViewItem uy = y as UserConnectionListViewItem;
                if (ux != null && uy != null)
                {
                    if (Descending)
                    {
                        UserConnectionListViewItem t = ux;
                        ux = uy;
                        uy = t;
                    }

                    if (ColumnIndex == 0)
                        return string.Compare(ux.Connection.ConnectionTypeClass.Name, uy.Connection.ConnectionTypeClass.Name);
                    else if (ColumnIndex == 1)
                        return string.Compare(ux.Connection.Database, uy.Connection.Database);
                    else if (ColumnIndex == 2)
                        return string.Compare(ux.Connection.User, uy.Connection.User);
                    else if (ColumnIndex == 3)
                        return string.Compare(ux.Connection.Mode, uy.Connection.Mode);
                    else if (ColumnIndex == 4)
                        return string.Compare(ux.Connection.ConnectionPath, uy.Connection.ConnectionPath);
                    else if (ColumnIndex == 5)
                        return DateTime.Compare(ux.Connection.TimeLogin, uy.Connection.TimeLogin);

                }
                return 0;
            }

            #endregion
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ((listView1.SelectedItems.Count > 0) && 
                (listView1.SelectedItems[0] is UserConnectionListViewItem))
            {                
                //UserConnectionListViewItem item = listView1.SelectedItems[0] as UserConnectionListViewItem;
                if (MessageBox.Show("Вы действительно желаете удалить выбранные элементы?", "Удаление",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    foreach (UserConnectionListViewItem item in listView1.SelectedItems)
                    {
                        App.Current.Context.Repository.RemoveUserConnection(item.Connection);
                        FillUserConnectonList();
                        //LocalSavedRepositoryConnectionList list =
                        //    LocalSavedRepositoryConnectionList.Deserialize(App.Current.Properties.LocalRepositoryConnectionsFile);
                        //LocalSavedRepositoryConnection exists = null;
                        //foreach (var c in list)
                        //    if (c.EqualConnection(item.Connection))
                        //    {
                        //        exists = c;
                        //        break;
                        //    }
                        //if (exists != null)
                        //{
                        //    list.Remove(exists);
                        //    list.Serialize(App.Current.Properties.LocalRepositoryConnectionsFile);
                        //    FillUserConnectonList();
                        //}
                    }
                }
            }
        }
    }

    internal class UserConnectionListViewItem : ListViewItem
    {
        private RepositoryUserConnection _Connection;
        private ConnectionType ct;

        public UserConnectionListViewItem(RepositoryUserConnection Connection, frmNewConnection frm)
        {
            this._Connection = Connection;
            ct = frm.GetConnectionType(Connection);
            Text = Connection.ConnectionTypeClass.Name;
            SubItems.Add(Connection.Database);
            SubItems.Add(Connection.User);
            SubItems.Add(Connection.Mode);
            SubItems.Add(Connection.ConnectionPath);
            //SubItems.Add(Connection.Name);
            //SubItems.Add(new ListViewSubItem(this, Connection.TimeCreated.ToString("dd.MM.yyyy HH:mm:ss")));
            SubItems.Add(new ListViewSubItem(this, Connection.TimeLogin.ToString("dd.MM.yy HH:mm:ss")));
            if (ct != null)
                ImageIndex = frm.GetImageIndex(ct);
        }
        
        public RepositoryUserConnection Connection
        {
            get { return _Connection; }
        }
    }

    internal class LocalSavedConnectionListViewItem : ListViewItem
    {
        private LocalSavedRepositoryConnection _Connection;
        private ConnectionType ct;

        public LocalSavedConnectionListViewItem(LocalSavedRepositoryConnection Connection, frmNewConnection frm)
        {
            this._Connection = Connection;
            ct = frm.GetLocalConnectionType(Connection);
            Text = Connection.Name;
            SubItems.Add(ct.Name);
            SubItems.Add(new ListViewSubItem(this, Connection.TimeCreated.ToString("dd.MM.yyyy HH:mm:ss")));
            SubItems.Add(new ListViewSubItem(this, Connection.TimeLogin.ToString("dd.MM.yyyy HH:mm:ss")));
            if (ct != null)
                ImageIndex = frm.GetImageIndex(ct);
        }

        public ConnectionType ConnectionType
        {
            get { return ct; }
        }

        public LocalSavedRepositoryConnection Connection
        {
            get { return _Connection; }
        }
    }
}
