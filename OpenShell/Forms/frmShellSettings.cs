﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using OpenShellClasses;
using System.Windows.Forms;

namespace OpenShell.Forms
{
    public partial class frmShellSettings : Form
    {
        internal PropertyList propList;

        public frmShellSettings()
        {
            InitializeComponent();
            propList = App.Current.Properties.List.Clone();
            propertyGrid1.SelectedObject = new PropertyListTypeDescriptor(propList);
            ActiveControl = propertyGrid1;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            propertyGrid1.Validate();
        }

        private void MoveSplitter(PropertyGrid propertyGrid, int x)
        {
            object propertyGridView = typeof(PropertyGrid).InvokeMember("gridView", BindingFlags.GetField | BindingFlags.NonPublic | BindingFlags.Instance, null, propertyGrid, null);
            propertyGridView.GetType().InvokeMember("MoveSplitterTo", BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Instance, null, propertyGridView, new object[] { x });
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void frmShellSettings_Shown(object sender, EventArgs e)
        {
            MoveSplitter(propertyGrid1, 400);
        }
    }

    public class PropertyGridController
    {
        // This is a copied hack to set the property grid label width I found somewhere
        // online.  However, it does come with some usability issues with where
        // the user has to click to launch the editor and some paint issues.
        // I'll placed it in here for you to play with.

        #region Set Label Width
        public static void SetLabelWidth(PropertyGrid propertyGrid, int width)
        {
            Control grid = propertyGrid;
            Control propertyGridView = grid.Controls[2];

            Type propertyGridViewType = propertyGridView.GetType();

            FieldInfo fldLabelWidth = propertyGridViewType.GetField("labelWidth",
                                                 BindingFlags.Instance | BindingFlags.NonPublic);
            fldLabelWidth.SetValue(propertyGridView, width);
        }
        #endregion

        #region Set Label Width
        public static void SetLabelWidth(PropertyGrid propertyGrid, double percentage)
        {

            try
            {

                Control propertyGridView = propertyGrid.Controls[2];

                Type propertyGridViewType = propertyGridView.GetType();

                FieldInfo fldLabelWidth = propertyGridViewType.GetField("labelWidth",
                                                     BindingFlags.Instance
                                                     | BindingFlags.NonPublic);

                if (propertyGridView.Width < 1) { return; }

                fldLabelWidth.SetValue(propertyGridView, (int)((propertyGridView.Width * percentage)));

                propertyGrid.Invalidate();

            }
            catch (Exception) { throw; }

        }
        #endregion
    }

}
