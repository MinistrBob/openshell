using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OpenShell.Forms
{
    public partial class frmWaitDialog : Form
    {
        public frmWaitDialog()
        {
            InitializeComponent();
            TopText = string.Empty;
            BottomText = string.Empty;
        }

        public string TopText
        {
            get { return lblTop.Text; }
            set
            {
                lblTop.Text = value;
                UpdateUI();
            }
        }

        public string BottomText
        {
            get { return lblBottom.Text; }
            set
            {
                lblBottom.Text = value;
                UpdateUI();
            }
        }

        public void Status(string s)
        {
            TopText = s;
        }

        public ProgressBar ProgressBar
        {
            get { return progressBar1; }
        }

        public void UpdateUI()
        {
            lblBottom.Visible = (lblBottom.Text.Length > 0);
            progressBar1.Visible = (progressBar1.Maximum > 0);
            if (!lblBottom.Visible)
            {
                if (!progressBar1.Visible)
                    Height = 60;
                else
                    Height = 70;
            }
            else
                Height = 90;

            Application.DoEvents();
        }

        public static frmWaitDialog Start(string topText)
        {
            frmWaitDialog result = new frmWaitDialog();
            result.Show();
            result.TopText = topText;
            return result;
        }
    }
}