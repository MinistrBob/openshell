﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using OpenShellClasses;

namespace OpenShell.Classes
{
    [ModuleInfo("Проверка информации модуля")]
    [ModuleConnectionBinding(typeof(DbRepositoryConnectionType), false)]
    [ModuleConnectionBinding(typeof(LocalRepositoryConnectionType), false)]
    public class RepositoryControlModule : ModuleBase
    {
        #region зарегистрированные действия

        ContainerAction actionContainer;
        PageAction actionRepositorySettings;

        #endregion

        #region IModule Members

        public override void Load()
        {
            actionContainer = 
                new ContainerAction("Db:Settings", "Репозиторий", Resources.Images.Properties1);

            Page page = new Page(typeof(ctlRepositorySettings));
            actionRepositorySettings = 
                new PageAction("RepositoryControlModule:Modules", page,
                    "Модули");
            actionRepositorySettings.Image = Resources.Images.preferences16;
            actionRepositorySettings.Hint = "Элемент настроек репозитория";

            actionContainer.Childs.Add(actionRepositorySettings);

            PageAction actionRepositorySettingsToolBar =
                new PageAction("RepositoryControlModule:ModulesToolBar", page, "Модули");
            actionRepositorySettingsToolBar.Image = Resources.Images.preferences16;
            actionRepositorySettingsToolBar.ImageLarge = Resources.Images.preferences24;
            actionRepositorySettingsToolBar.Hint = "Элемент настроек репозитория";
            actionRepositorySettingsToolBar.Placement = ActionPlacement.MainToolBar;
            

            Context.AddAction(actionContainer);
            Context.AddAction(actionRepositorySettingsToolBar);
        }

        public override void Unload()
        {
        }

        #endregion
    }
}
