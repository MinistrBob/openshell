﻿namespace OpenShell.Classes
{
    partial class ctlRepositorySettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctlRepositorySettings));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.ctlUserModules1 = new OpenShell.RepositoryControlModule.ctlUserModules();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList1.Images.SetKeyName(0, "insert16.bmp");
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 1;
            this.bar1.FloatLocation = new System.Drawing.Point(337, 288);
            this.bar1.Offset = 39;
            this.bar1.Text = "Tools";
            // 
            // ctlUserModules1
            // 
            this.ctlUserModules1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlUserModules1.Location = new System.Drawing.Point(0, 0);
            this.ctlUserModules1.Name = "ctlUserModules1";
            this.ctlUserModules1.Size = new System.Drawing.Size(712, 553);
            this.ctlUserModules1.TabIndex = 0;
            // 
            // ctlRepositorySettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ctlUserModules1);
            this.Name = "ctlRepositorySettings";
            this.Size = new System.Drawing.Size(712, 553);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraBars.Bar bar1;
        private OpenShell.RepositoryControlModule.ctlUserModules ctlUserModules1;
    }
}
