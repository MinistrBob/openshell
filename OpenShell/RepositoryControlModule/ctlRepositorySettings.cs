﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace OpenShell.Classes
{
    public partial class ctlRepositorySettings : UserControl, OpenShellClasses.IPageEvents
    {
        public ctlRepositorySettings()
        {
            InitializeComponent();
            ctlUserModules1.Close += new EventHandler(ctlUserModules1_Close);
        }

        public event EventHandler Close;

        void ctlUserModules1_Close(object sender, EventArgs e)
        {
            if (Close != null)
                Close(this, EventArgs.Empty);
        }




        #region IPageEvents Members

        public void CreatePageControl(OpenShellClasses.IModuleContext context)
        {
        }

        public bool CanEnterPage(OpenShellClasses.IModuleContext context)
        {
            return true;
        }

        public bool CanLeavePage(OpenShellClasses.IModuleContext context)
        {
            return ctlUserModules1.Leave();
        }

        public void EnterPage(OpenShellClasses.IModuleContext context)
        {
        }

        public void LeavePage(OpenShellClasses.IModuleContext context)
        {
        }

        #endregion
    }
}
