﻿namespace OpenShell.RepositoryControlModule
{
    partial class ctlUserModules
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label assemblyVersionLabel;
            System.Windows.Forms.Label authorLabel;
            System.Windows.Forms.Label commentLabel;
            System.Windows.Forms.Label copyrightLabel;
            System.Windows.Forms.Label descriptionLabel;
            System.Windows.Forms.Label nameLabel;
            System.Windows.Forms.Label versionLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctlUserModules));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.btnSave1 = new DevExpress.XtraBars.BarButtonItem();
            this.btnCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.btnSave = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryControlModuleDataSet1 = new OpenShell.RepositoryControlModule.RepositoryControlModuleDataSet2();
            this.panel1 = new System.Windows.Forms.Panel();
            this.commentTextBox = new System.Windows.Forms.TextBox();
            this.moduleInfoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.assemblyVersionTextBox = new System.Windows.Forms.TextBox();
            this.authorTextBox = new System.Windows.Forms.TextBox();
            this.copyrightTextBox = new System.Windows.Forms.TextBox();
            this.versionTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeList = new DevExpress.XtraTreeList.TreeList();
            this.cNAME = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cCLASS = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cDEFAULT = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.cSTATE = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.repositoryItemRadioGroup2 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            assemblyVersionLabel = new System.Windows.Forms.Label();
            authorLabel = new System.Windows.Forms.Label();
            commentLabel = new System.Windows.Forms.Label();
            copyrightLabel = new System.Windows.Forms.Label();
            descriptionLabel = new System.Windows.Forms.Label();
            nameLabel = new System.Windows.Forms.Label();
            versionLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryControlModuleDataSet1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moduleInfoBindingSource)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            this.SuspendLayout();
            // 
            // assemblyVersionLabel
            // 
            assemblyVersionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            assemblyVersionLabel.AutoSize = true;
            assemblyVersionLabel.Location = new System.Drawing.Point(610, 48);
            assemblyVersionLabel.Name = "assemblyVersionLabel";
            assemblyVersionLabel.Size = new System.Drawing.Size(86, 13);
            assemblyVersionLabel.TabIndex = 0;
            assemblyVersionLabel.Text = "Версия сборки:";
            // 
            // authorLabel
            // 
            authorLabel.AutoSize = true;
            authorLabel.Location = new System.Drawing.Point(10, 48);
            authorLabel.Name = "authorLabel";
            authorLabel.Size = new System.Drawing.Size(75, 13);
            authorLabel.TabIndex = 2;
            authorLabel.Text = "Разработчик:";
            // 
            // commentLabel
            // 
            commentLabel.AutoSize = true;
            commentLabel.Location = new System.Drawing.Point(10, 128);
            commentLabel.Name = "commentLabel";
            commentLabel.Size = new System.Drawing.Size(80, 13);
            commentLabel.TabIndex = 4;
            commentLabel.Text = "Комментарий:";
            // 
            // copyrightLabel
            // 
            copyrightLabel.AutoSize = true;
            copyrightLabel.Location = new System.Drawing.Point(255, 48);
            copyrightLabel.Name = "copyrightLabel";
            copyrightLabel.Size = new System.Drawing.Size(101, 13);
            copyrightLabel.TabIndex = 6;
            copyrightLabel.Text = "Правообладатель:";
            // 
            // descriptionLabel
            // 
            descriptionLabel.AutoSize = true;
            descriptionLabel.Location = new System.Drawing.Point(10, 89);
            descriptionLabel.Name = "descriptionLabel";
            descriptionLabel.Size = new System.Drawing.Size(60, 13);
            descriptionLabel.TabIndex = 8;
            descriptionLabel.Text = "Описание:";
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.Location = new System.Drawing.Point(10, 6);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(126, 13);
            nameLabel.TabIndex = 10;
            nameLabel.Text = "Наименование модуля:";
            // 
            // versionLabel
            // 
            versionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            versionLabel.AutoSize = true;
            versionLabel.Location = new System.Drawing.Point(610, 6);
            versionLabel.Name = "versionLabel";
            versionLabel.Size = new System.Drawing.Size(87, 13);
            versionLabel.TabIndex = 12;
            versionLabel.Text = "Версия модуля:";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Images = this.imageList1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1,
            this.btnCancel,
            this.barButtonItem3,
            this.btnSave,
            this.btnSave1,
            this.barButtonItem2,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6});
            this.barManager1.MaxItemId = 9;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnSave1, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnCancel, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem6, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem5, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem2, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.DrawSizeGrip = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Tools";
            // 
            // btnSave1
            // 
            this.btnSave1.Caption = "Сохранить изменения";
            this.btnSave1.Id = 4;
            this.btnSave1.ImageIndex = 0;
            this.btnSave1.Name = "btnSave1";
            this.btnSave1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // btnCancel
            // 
            this.btnCancel.Caption = "Отменить изменения";
            this.btnCancel.Id = 1;
            this.btnCancel.ImageIndex = 1;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCancel_ItemClick);
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Вкл. все";
            this.barButtonItem6.Id = 8;
            this.barButtonItem6.ImageIndex = 3;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Выкл. все";
            this.barButtonItem5.Id = 7;
            this.barButtonItem5.ImageIndex = 4;
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Закрыть";
            this.barButtonItem2.Id = 5;
            this.barButtonItem2.ImageIndex = 2;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick_1);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(762, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 559);
            this.barDockControlBottom.Size = new System.Drawing.Size(762, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 528);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(762, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 528);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList1.Images.SetKeyName(0, "save16.bmp");
            this.imageList1.Images.SetKeyName(1, "delete16.bmp");
            this.imageList1.Images.SetKeyName(2, "closewindow16.bmp");
            this.imageList1.Images.SetKeyName(3, "tick_all16.bmp");
            this.imageList1.Images.SetKeyName(4, "cross16.bmp");
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Сохранить изменения";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Сохранить изменения";
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.ImageIndex = 0;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // btnSave
            // 
            this.btnSave.Caption = "Сохранить изменения";
            this.btnSave.Id = 3;
            this.btnSave.ImageIndex = 0;
            this.btnSave.Name = "btnSave";
            this.btnSave.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Вкл. все";
            this.barButtonItem4.Id = 6;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "RepositoryModuleInfo";
            this.bindingSource1.DataSource = this.repositoryControlModuleDataSet1;
            this.bindingSource1.CurrentItemChanged += new System.EventHandler(this.bindingSource1_CurrentItemChanged);
            // 
            // repositoryControlModuleDataSet1
            // 
            this.repositoryControlModuleDataSet1.DataSetName = "RepositoryControlModuleDataSet";
            this.repositoryControlModuleDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.commentTextBox);
            this.panel1.Controls.Add(this.descriptionTextBox);
            this.panel1.Controls.Add(this.assemblyVersionTextBox);
            this.panel1.Controls.Add(this.authorTextBox);
            this.panel1.Controls.Add(this.copyrightTextBox);
            this.panel1.Controls.Add(this.versionTextBox);
            this.panel1.Controls.Add(this.nameTextBox);
            this.panel1.Controls.Add(assemblyVersionLabel);
            this.panel1.Controls.Add(authorLabel);
            this.panel1.Controls.Add(commentLabel);
            this.panel1.Controls.Add(copyrightLabel);
            this.panel1.Controls.Add(descriptionLabel);
            this.panel1.Controls.Add(nameLabel);
            this.panel1.Controls.Add(versionLabel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(762, 176);
            this.panel1.TabIndex = 6;
            // 
            // commentTextBox
            // 
            this.commentTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.commentTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moduleInfoBindingSource, "Comment", true));
            this.commentTextBox.Location = new System.Drawing.Point(13, 144);
            this.commentTextBox.Name = "commentTextBox";
            this.commentTextBox.ReadOnly = true;
            this.commentTextBox.Size = new System.Drawing.Size(731, 20);
            this.commentTextBox.TabIndex = 22;
            // 
            // moduleInfoBindingSource
            // 
            this.moduleInfoBindingSource.DataSource = typeof(OpenShellClasses.ModuleInfo);
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.descriptionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moduleInfoBindingSource, "Description", true));
            this.descriptionTextBox.Location = new System.Drawing.Point(13, 105);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.ReadOnly = true;
            this.descriptionTextBox.Size = new System.Drawing.Size(731, 20);
            this.descriptionTextBox.TabIndex = 21;
            // 
            // assemblyVersionTextBox
            // 
            this.assemblyVersionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.assemblyVersionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moduleInfoBindingSource, "AssemblyVersion", true));
            this.assemblyVersionTextBox.Location = new System.Drawing.Point(613, 64);
            this.assemblyVersionTextBox.Name = "assemblyVersionTextBox";
            this.assemblyVersionTextBox.ReadOnly = true;
            this.assemblyVersionTextBox.Size = new System.Drawing.Size(131, 20);
            this.assemblyVersionTextBox.TabIndex = 20;
            // 
            // authorTextBox
            // 
            this.authorTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moduleInfoBindingSource, "Author", true));
            this.authorTextBox.Location = new System.Drawing.Point(13, 64);
            this.authorTextBox.Name = "authorTextBox";
            this.authorTextBox.ReadOnly = true;
            this.authorTextBox.Size = new System.Drawing.Size(239, 20);
            this.authorTextBox.TabIndex = 19;
            // 
            // copyrightTextBox
            // 
            this.copyrightTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.copyrightTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moduleInfoBindingSource, "Copyright", true));
            this.copyrightTextBox.Location = new System.Drawing.Point(258, 64);
            this.copyrightTextBox.Name = "copyrightTextBox";
            this.copyrightTextBox.ReadOnly = true;
            this.copyrightTextBox.Size = new System.Drawing.Size(349, 20);
            this.copyrightTextBox.TabIndex = 18;
            // 
            // versionTextBox
            // 
            this.versionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.versionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moduleInfoBindingSource, "Version", true));
            this.versionTextBox.Location = new System.Drawing.Point(613, 25);
            this.versionTextBox.Name = "versionTextBox";
            this.versionTextBox.ReadOnly = true;
            this.versionTextBox.Size = new System.Drawing.Size(131, 20);
            this.versionTextBox.TabIndex = 16;
            // 
            // nameTextBox
            // 
            this.nameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.moduleInfoBindingSource, "Name", true));
            this.nameTextBox.Location = new System.Drawing.Point(13, 25);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.ReadOnly = true;
            this.nameTextBox.Size = new System.Drawing.Size(594, 20);
            this.nameTextBox.TabIndex = 15;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 31);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeList);
            this.splitContainer1.Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel1_Paint);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(762, 528);
            this.splitContainer1.SplitterDistance = 348;
            this.splitContainer1.TabIndex = 7;
            // 
            // treeList
            // 
            this.treeList.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.cNAME,
            this.cCLASS,
            this.cDEFAULT,
            this.cSTATE});
            this.treeList.DataSource = this.bindingSource1;
            this.treeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeList.KeyFieldName = "Code";
            this.treeList.Location = new System.Drawing.Point(0, 0);
            this.treeList.Name = "treeList";
            this.treeList.ParentFieldName = "Parent";
            this.treeList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemRadioGroup2});
            this.treeList.Size = new System.Drawing.Size(762, 348);
            this.treeList.TabIndex = 6;
            this.treeList.NodeCellStyle += new DevExpress.XtraTreeList.GetCustomNodeCellStyleEventHandler(this.treeList_NodeCellStyle);
            // 
            // cNAME
            // 
            this.cNAME.Caption = "Наименование";
            this.cNAME.FieldName = "Name";
            this.cNAME.Name = "cNAME";
            this.cNAME.OptionsColumn.AllowEdit = false;
            this.cNAME.OptionsColumn.AllowFocus = false;
            this.cNAME.OptionsColumn.ReadOnly = true;
            this.cNAME.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this.cNAME.Visible = true;
            this.cNAME.VisibleIndex = 0;
            this.cNAME.Width = 203;
            // 
            // cCLASS
            // 
            this.cCLASS.Caption = "Класс";
            this.cCLASS.FieldName = "ClassName";
            this.cCLASS.Name = "cCLASS";
            this.cCLASS.OptionsColumn.AllowEdit = false;
            this.cCLASS.OptionsColumn.AllowFocus = false;
            this.cCLASS.OptionsColumn.FixedWidth = true;
            this.cCLASS.OptionsColumn.ReadOnly = true;
            this.cCLASS.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this.cCLASS.Visible = true;
            this.cCLASS.VisibleIndex = 1;
            this.cCLASS.Width = 181;
            // 
            // cDEFAULT
            // 
            this.cDEFAULT.Caption = "По умолчан.";
            this.cDEFAULT.FieldName = "DefaultStatus";
            this.cDEFAULT.Name = "cDEFAULT";
            this.cDEFAULT.OptionsColumn.AllowEdit = false;
            this.cDEFAULT.OptionsColumn.AllowFocus = false;
            this.cDEFAULT.OptionsColumn.FixedWidth = true;
            this.cDEFAULT.OptionsColumn.ReadOnly = true;
            this.cDEFAULT.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this.cDEFAULT.Visible = true;
            this.cDEFAULT.VisibleIndex = 2;
            this.cDEFAULT.Width = 92;
            // 
            // cSTATE
            // 
            this.cSTATE.Caption = "Состояние";
            this.cSTATE.ColumnEdit = this.repositoryItemRadioGroup2;
            this.cSTATE.FieldName = "Status";
            this.cSTATE.Name = "cSTATE";
            this.cSTATE.OptionsColumn.FixedWidth = true;
            this.cSTATE.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.Integer;
            this.cSTATE.Visible = true;
            this.cSTATE.VisibleIndex = 3;
            this.cSTATE.Width = 260;
            // 
            // repositoryItemRadioGroup2
            // 
            this.repositoryItemRadioGroup2.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Включен"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Выключен"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "По умолчан.")});
            this.repositoryItemRadioGroup2.Name = "repositoryItemRadioGroup2";
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.DataSource = this.moduleInfoBindingSource;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(180, 120);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(180, 120);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "tick_all16.bmp");
            this.imageList2.Images.SetKeyName(1, "cross16.bmp");
            // 
            // ctlUserModules
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ctlUserModules";
            this.Size = new System.Drawing.Size(762, 559);
            this.Load += new System.EventHandler(this.ctlUserModules_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryControlModuleDataSet1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moduleInfoBindingSource)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private RepositoryControlModuleDataSet2 repositoryControlModuleDataSet1;
        private System.Windows.Forms.BindingSource moduleInfoBindingSource;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem btnCancel;
        private DevExpress.XtraBars.BarButtonItem btnSave;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem btnSave1;
        private System.Windows.Forms.TextBox copyrightTextBox;
        private System.Windows.Forms.TextBox versionTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.TextBox commentTextBox;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.TextBox assemblyVersionTextBox;
        private System.Windows.Forms.TextBox authorTextBox;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private System.Windows.Forms.ImageList imageList2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraTreeList.TreeList treeList;
        private DevExpress.XtraTreeList.Columns.TreeListColumn cNAME;
        private DevExpress.XtraTreeList.Columns.TreeListColumn cCLASS;
        private DevExpress.XtraTreeList.Columns.TreeListColumn cDEFAULT;
        private DevExpress.XtraTreeList.Columns.TreeListColumn cSTATE;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup2;
    }
}
