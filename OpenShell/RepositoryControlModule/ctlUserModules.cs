﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using OpenShellClasses;
using DevExpress.XtraTreeList.Nodes;

namespace OpenShell.RepositoryControlModule
{
    public partial class ctlUserModules : UserControl
    {
        private ClassPathLoader loader;

        public event EventHandler Close;

        public ctlUserModules()
        {
            InitializeComponent();
            //colDefaultStatus.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            //colDefaultStatus.DisplayFormat.Format = new DefaultStatusFormatProvider();
            cDEFAULT.Format.FormatType = DevExpress.Utils.FormatType.Custom;
            cDEFAULT.Format.Format = new DefaultStatusFormatProvider();

            repositoryControlModuleDataSet1.RepositoryModuleInfo.Columns.Add("Code", typeof(int));
            repositoryControlModuleDataSet1.RepositoryModuleInfo.Columns.Add("Parent", typeof(int));
        }

        private void ctlUserModules_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
                Fill();
        }

        private void Fill()
        {
            OldFill();
            treeList.Update();
            treeList.ExpandAll();
          /*  if (!DesignMode && App.Current != null)
            {
                loader = App.Current.Context.ClassPathLoader;
                repositoryControlModuleDataSet1.AcceptChanges();

                treeList.BeginUpdate();
                treeList.Nodes.Clear();

                foreach (RepositoryClass Class in App.Current.Context.Repository.Classes)
                    if (Class.ClassType.Code == "CONNECTION_TYPE")
                    {
                        treeList.AppendNode(GetNodeData(Class), null);
                        foreach (RepositoryClass SubClass in App.Current.Context.Repository.Classes)
                        {
                            if (Class.ClassType.Code == "MODULE")
                            {
                                Class.Getc
                            }
                        }
                    }

                treeList.EndUpdate();

                UpdateUI();
            }*/
        }

        public object[] GetNodeData(RepositoryClass Class)
        {
            return new object[] { Class.Name, Class.ClassTypeName, 
                Util.ConvertDefaultStatus(Class.DefaultStatus), GetClassStatus(Class) };
        }

        private int GetClassStatus(RepositoryClass Class)
        {
            int newStatus = 2;
            foreach (RepositoryUserClassLink link in App.Current.Context.Repository.UserClasses)
                if (link.Class == Class)
                {
                    if (link.IsNew)
                        newStatus = 2;
                    else if (link.IsConnected)
                        newStatus = 1;
                    else
                        newStatus = 0;
                }
            return newStatus;
        }

        private void OldFill()
        {
            if (!DesignMode && App.Current != null)
            {
                loader = App.Current.Context.ClassPathLoader;
                repositoryControlModuleDataSet1.AcceptChanges();

                bindingSource1.SuspendBinding();
                repositoryControlModuleDataSet1.RepositoryModuleInfo.BeginLoadData();
                repositoryControlModuleDataSet1.RepositoryModuleInfo.Clear();

                RepositoryUserClassLinkList link_list = App.Current.Context.Repository.UserClasses;

                foreach (RepositoryClass Class in App.Current.Context.Repository.Classes)
                //if (Class.ClassType.Code == "MODULE")
                {
                    RepositoryControlModuleDataSet2.RepositoryModuleInfoRow row =
                        repositoryControlModuleDataSet1.RepositoryModuleInfo.NewRepositoryModuleInfoRow();
                    row.ClassInfo = Class;
                    row.ClassType = Class.ClassType;
                    row.ClassTypeName = Class.ClassTypeName;
                    row.ClassName = Class.ClassPath.ToString();
                    row.Name = Class.Name;
                    row.ModuleInfo = GetModuleInfo(Class);
                    row.Status = 2; // новый
                    row["Code"] = Class.Code;
                    row["Parent"] = Class.Parent;

                    foreach (RepositoryUserClassLink link in link_list)
                        if (link.Class == Class)
                        {
                            if (link.IsNew)
                                row.Status = 2;
                            else if (link.IsConnected)
                                row.Status = 1;
                            else
                                row.Status = 0;
                        }

                    repositoryControlModuleDataSet1.RepositoryModuleInfo.Rows.Add(row);
                    row.DefaultStatus = Util.ConvertDefaultStatus(Class.DefaultStatus);
                }

                repositoryControlModuleDataSet1.RepositoryModuleInfo.EndLoadData();

                bindingSource1.ResumeBinding();
                bindingSource1.ResetBindings(false);

                //gridView1.ExpandAllGroups();
                //gridView1.FocusedRowHandle = 0;

                repositoryControlModuleDataSet1.AcceptChanges();

                UpdateUI();
            }
        }

        private ModuleInfo GetModuleInfo(RepositoryClass Class)
        {
            Type type = loader.LoadType(Class.ClassPath);
            if (type != null)
                return ModuleInfo.GetModuleInfo(type);
            else
                return null;
        }

        private void bindingSource1_CurrentItemChanged(object sender, EventArgs e)
        {
            if (bindingSource1.Current != null)
            {
                RepositoryControlModuleDataSet2.RepositoryModuleInfoRow row =
                    ((bindingSource1.Current as DataRowView).Row as
                    RepositoryControlModuleDataSet2.RepositoryModuleInfoRow);
                if (!row.IsModuleInfoNull())
                    moduleInfoBindingSource.DataSource = row.ModuleInfo;
            }
            else
                moduleInfoBindingSource.DataSource = null;
            moduleInfoBindingSource.ResetBindings(false);
            UpdateUI();                
        }

        protected bool HasChanges
        {
            get
            {
                bindingSource1.EndEdit();
                return (repositoryControlModuleDataSet1.HasChanges());
            }
        }

        protected virtual void UpdateUI()
        {
            btnSave1.Enabled = HasChanges;
            btnCancel.Enabled = btnSave1.Enabled;
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SaveChanges();
            Fill();
        }

        private void SaveChanges()
        {
            bindingSource1.EndEdit();
            bindingSource1.MoveFirst();
            bindingSource1.MoveNext();
            bindingSource1.MoveFirst();
            foreach (RepositoryControlModuleDataSet2.RepositoryModuleInfoRow row in 
                repositoryControlModuleDataSet1.GetChanges().Tables[0].Rows)
            {
                App.Current.Context.Repository.UpdateUserClassLink(
                    row.ClassInfo, (RepositoryUpdateUserClassLinkAction)row.Status);
            }
            repositoryControlModuleDataSet1.AcceptChanges();
            AppContext.Current.Repository.LoadUserConfiguration();

            //AppContext.Current.LoadModules();
            //AppContext.Current.LoadModulesForConnection(AppContext.Current.ActiveConnection);
        }

        public class DefaultStatusFormatProvider : ICustomFormatter, IFormatProvider
        {
            #region ICustomFormatter Members

            public string Format(string format, object arg, IFormatProvider formatProvider)
            {
                RepositoryClassDefaultStatus status = Util.ConvertDefaultStatusStr(arg.ToString());
                switch (status)
                {
                    case RepositoryClassDefaultStatus.Enabled:
                        return "включен";
                    case RepositoryClassDefaultStatus.Disabled:
                        return "выключен";
                    case RepositoryClassDefaultStatus.Always:
                        return "всегда вкл.";
                    default:
                        return "";
                }
            }

            #endregion

            #region IFormatProvider Members

            public object GetFormat(Type formatType)
            {
                return this;
            }

            #endregion
        }

        private void btnCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Fill();
        }

        public new bool Leave()
        {
            if (HasChanges)
            {
                DialogResult result = MessageBox.Show(Resources.Strings.Msg_RepositoryControl_SaveChanges,
                    Resources.Strings.Msg_RepositoryControl_SaveChanges_Title,
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    SaveChanges();
                    return true;
                }
                else if (result == DialogResult.Cancel)
                    return false;
                else
                {
                    Fill();
                    return true;
                }
            }
            else
                return true;
        }

        private void gridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
        }

        private void repositoryItemRadioGroup1_EditValueChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("EditValue changed");
            int pos = bindingSource1.Position;
            bindingSource1.MoveLast();
            bindingSource1.MoveFirst();
            bindingSource1.Position = pos;
            bindingSource1.EndEdit();
        }

        private void barButtonItem2_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (repositoryControlModuleDataSet1.HasChanges())
            {
                if (MessageBox.Show("Сохранить изменения?", "Сохранение изменений",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                {
                    return;
                }
                else
                {
                    SaveChanges();
                }
            }
            if (Close != null)
                Close(this, EventArgs.Empty);

        }

        private void barButtonItem6_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            foreach (RepositoryControlModuleDataSet2.RepositoryModuleInfoRow row in repositoryControlModuleDataSet1.RepositoryModuleInfo.Rows)
            {
                row.Status = 1;
            }
            bindingSource1.ResetBindings(false);
        }

        private void barButtonItem5_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            foreach (RepositoryControlModuleDataSet2.RepositoryModuleInfoRow row in repositoryControlModuleDataSet1.RepositoryModuleInfo.Rows)
            {
                row.Status = 0;
            }
            bindingSource1.ResetBindings(false);
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void treeList_NodeCellStyle(object sender, DevExpress.XtraTreeList.GetCustomNodeCellStyleEventArgs e)
        {
            if (e.Column.FieldName == "DefaultStatus")
            {
                string DefaultStatus = System.Convert.ToString( e.Node.GetValue(e.Node.TreeList.Columns["DefaultStatus"]));
                if (DefaultStatus == "+")
                    e.Appearance.ForeColor = Color.Green;
                else if (DefaultStatus == "-")
                    e.Appearance.ForeColor = Color.Red;
                else if (DefaultStatus == "*")
                    e.Appearance.ForeColor = Color.Blue;
            }
            int Status = System.Convert.ToInt32(e.Node.GetValue(e.Node.TreeList.Columns["Status"]));
            if (Status == 0)
                e.Appearance.ForeColor = SystemColors.GrayText;
            else if (Status == 2)
            {
                e.Appearance.Font = new Font(e.Appearance.Font, FontStyle.Bold);
            }
        }
    }
}
