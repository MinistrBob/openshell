﻿namespace OpenShell.RepositoryControlModule
{
    partial class frmRepositoryControlModule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ctlRepositorySettings1 = new OpenShell.Classes.ctlRepositorySettings();
            this.SuspendLayout();
            // 
            // ctlRepositorySettings1
            // 
            this.ctlRepositorySettings1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlRepositorySettings1.Location = new System.Drawing.Point(0, 0);
            this.ctlRepositorySettings1.Name = "ctlRepositorySettings1";
            this.ctlRepositorySettings1.Size = new System.Drawing.Size(971, 486);
            this.ctlRepositorySettings1.TabIndex = 0;
            this.ctlRepositorySettings1.Load += new System.EventHandler(this.ctlRepositorySettings1_Load);
            // 
            // frmRepositoryControlModule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 486);
            this.Controls.Add(this.ctlRepositorySettings1);
            this.Name = "frmRepositoryControlModule";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Настройка доступных модулей";
            this.Load += new System.EventHandler(this.frmRepositoryControlModule_Load);
            this.Shown += new System.EventHandler(this.frmRepositoryControlModule_Shown);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmRepositoryControlModule_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private OpenShell.Classes.ctlRepositorySettings ctlRepositorySettings1;
    }
}