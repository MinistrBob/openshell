﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OpenShell.RepositoryControlModule
{
    public partial class frmRepositoryControlModule : Form
    {
        public static frmRepositoryControlModule Form;

        public frmRepositoryControlModule()
        {
            InitializeComponent();
            if (App.Current != null)
            {
                MdiParent = App.MainForm;
                ctlRepositorySettings1.Close += new EventHandler(ctlRepositorySettings1_Close);
            }
        }

        void ctlRepositorySettings1_Close(object sender, EventArgs e)
        {
            Close();
        }

        private void frmRepositoryControlModule_Load(object sender, EventArgs e)
        {
            
                
        }

        private void frmRepositoryControlModule_Shown(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
        }

        private void frmRepositoryControlModule_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form = null;
        }

        private void ctlRepositorySettings1_Load(object sender, EventArgs e)
        {

        }
    }
}
