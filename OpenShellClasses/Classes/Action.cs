﻿// =================================================================================================
//  Action.cs
//  Добавляемые модулями действия в УО
// =================================================================================================

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;

namespace OpenShellClasses
{
    public class Action : IDisplayItem
    {
        // ------------------------------------------------------------------------------------------------------------

        #region закрытые поля

        private string _Id;
        protected string _Name;
        protected string _Hint;
        protected Image _Image;
        protected Image _ImageLarge;
        protected bool _Visible = true;
        protected bool _Enabled = true;

        protected bool _CanEnterCalled;
        protected bool _CanLeaveCalled;

        private bool _CanEnterResult;
        private bool _CanLeaveResult;

        private ActionPlacement _Placement;
        private ActionsController _Controller;
        private IModuleContext _Context;

        private string _MenuPath;
        private bool _ShowCaption;
        private bool _StoreSelectionState = true;

        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region события

        public event ActionCancelEventHandler CanEnter;
        public event ActionNotifyEventHandler Enter;

        public event ActionCancelEventHandler CanLeave;
        public event ActionNotifyEventHandler Leave;

        public event ActionPropertyChangedEventHandler PropertyChanged;

        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region конструкторы

        public Action(string Id, string Name)
            : this(Id, Name, string.Empty, null)
        {
        }

        public Action(string Id, string Name, string Hint)
            : this(Id, Name, Hint, null)
        {
        }
        
        public Action(string Id, string Name, Image Image)
            : this(Id, Name, string.Empty, Image)
        {
        }

        public Action(string Id, string Name, string Hint, Image Image)
        {
            this._Id = Id;
            this._Name = Name;
            this._Hint = Hint;
            this._Image = Image;
        }

        #endregion

        // ------------------------------------------------------------------------------------------------------------

        public ActionPlacement Placement
        {
            get { return _Placement; }
            set 
            { 
                if (_Controller == null)
                    _Placement = value; 
            }
        }

        public IModuleContext Context
        {
            get { return _Context; }
            set 
            { 
                _Context = value;
                _ContextChanged();
            }
        }

        public string MenuPath
        {
            get { return _MenuPath; }
            set { _MenuPath = value; }
        }

        public bool ShowCaption
        {
            get { return _ShowCaption; }
            set { _ShowCaption = value; }
        }

        /// <summary>
        /// сохранять выбор данного действия в навигаторе для повторного возвращения после открытия соединения
        /// </summary>
        public bool StoreSelectionState
        {
            get { return _StoreSelectionState; }
            set { _StoreSelectionState = value; }
        }

        protected virtual void _ContextChanged()
        {
        }

        public ActionsController Controller
        {
            get { return _Controller; }
            set { _Controller = value; }
        }

        #region IDisplayItem Members

        public virtual string Id
        {
            get { return _Id; }
        }

        public virtual string Name
        {
            get { return _Name; }
            set 
            { 
                _Name = value;
                _PropertyChanged("Name", value);
            }
        }

        private void _PropertyChanged(string propertyName, object newValue)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new ActionPropertyChangedEventArgs(this, propertyName, newValue));
            }
        }

        public System.Drawing.Image Image
        {
            get { return _Image; }
            set
            {
                _Image = value;
                _PropertyChanged("Image", value);
            }
        }

        public System.Drawing.Image ImageLarge
        {
            get { return _ImageLarge; }
            set
            {
                _ImageLarge = value;
                _PropertyChanged("ImageLarge", value);
            }
        }

        public virtual string Hint
        {
            get { return _Hint; }
            set
            {
                _Hint = value;
                _PropertyChanged("Hint", value);
            }
        }

        public bool Visible
        {
            get { return _Visible; }
            set
            {
                bool _OldVisible = _Visible;
                _Visible = value;
                if (_OldVisible != _Visible)
                    _PropertyChanged("Visible", _Visible);
            }
        }

        public bool Enabled
        {
            get { return _Enabled; }
            set
            {
                _Enabled = value;
                _PropertyChanged("Enabled", value);
            }
        }

        #endregion

        public virtual bool DoCanLeave()
        {
            if (_CanLeaveCalled)
                return _CanLeaveResult;
            else
            {
                if (CanLeave != null)
                {
                    ActionCancelEventArgs args = new ActionCancelEventArgs(this, Context);
                    CanLeave(this, args);
                    _CanLeaveResult = !args.Cancel;
                }
                else
                    _CanLeaveResult = true;
                _CanLeaveCalled = true;
                return _CanLeaveResult;
            }
        }

        public virtual bool DoCanEnter()
        {
            if (_CanEnterCalled)
                return _CanEnterResult;
            else
            {
                if (CanEnter != null)
                {
                    ActionCancelEventArgs args = new ActionCancelEventArgs(this, Context);
                    CanEnter(this, args);
                    _CanEnterResult = !args.Cancel;
                }
                else
                    _CanEnterResult = true;
                _CanEnterCalled = true;
                return _CanEnterResult;
            }
        }

        public virtual void DoEnter()
        {
            if (Enter != null)
                Enter(this, new ActionNotifyEventArgs(this, Context));
        }

        public virtual void DoLeave()
        {
            if (Leave != null)
                Leave(this, new ActionNotifyEventArgs(this, Context));
        }

        public void Reset()
        {
            _CanEnterCalled = false;
            _CanLeaveCalled = false;
        }

        public void Initialize()
        {
            Controller.Add(this);
        }

        public void Deinitialize()
        {
            if (_Controller != null)
                _Controller.Remove(this);
        }

        public virtual void Update()
        {
        }
    }

    // =================================================================================================

    public delegate void ActionPropertyChangedEventHandler(object sender, ActionPropertyChangedEventArgs e);

    public delegate void ActionNotifyEventHandler(object sender, ActionNotifyEventArgs e);
    public delegate void ActionCancelEventHandler(object sender, ActionCancelEventArgs e);

    public delegate void ContainerActionChildChangeEventHandler(object sender, ContainerActionChildChangeEventArgs e);

    // =================================================================================================

    public class ActionNotifyEventArgs : EventArgs
    {
        private IModuleContext _ModuleContext;
        private Action _Action;

        public ActionNotifyEventArgs(Action Action, IModuleContext ModuleContext)
        {
            _Action = Action;
            _ModuleContext = ModuleContext;
        }

        public Action Action
        {
            get { return _Action; }
        }

        public IModuleContext ModuleContext
        {
            get { return _ModuleContext; }
        }
    }

    // =================================================================================================

    public class ActionCancelEventArgs : ActionNotifyEventArgs
    {
        private bool _Cancel = false;

        public ActionCancelEventArgs(Action Action, IModuleContext Connection) : base(Action, Connection)
        {

        }

        public bool Cancel
        {
            get { return _Cancel; }
            set { _Cancel = value; }
        }
    }

    public class ActionPropertyChangedEventArgs : EventArgs
    {
        public ActionPropertyChangedEventArgs(Action Action, string PropertyName, object NewValue)
        {
            this.Action = Action;
            this.PropertyName = PropertyName;
            this.NewValue = NewValue;
        }

        public Action Action
        {
            get;
            private set;
        }

        public string PropertyName
        {
            get;
            private set;
        }

        public object NewValue
        {
            get;
            private set;
        }
    }

    // =================================================================================================

    public class ContainerActionChildChangeEventArgs : EventArgs
    {
        public ContainerActionChildChangeEventArgs(Action action, ListChangedType changeType)
        {
            Action = action;
            Change = changeType;
        }

        public Action Action { get; private set; }

        public ListChangedType Change { get; private set; }
    }



    // =================================================================================================

    public class ActionList : List<Action>
    {
        public new Action Add(Action action)
        {
            base.Add(action);
            return action;
        }
    }

    // =================================================================================================

    public class EventActionList : BindingList<Action>
    {
        public new Action Add(Action action)
        {
            base.Add(action);
            return action;
        }
    }

    // =================================================================================================

    public class ContainerAction : PageAction
    {
        private EventActionList _Childs;

        #region Event
        
        public event ContainerActionChildChangeEventHandler ChildChanged;

        #endregion

        public ContainerAction(string Id, string Name)
            : this(Id, Name, string.Empty, null)
        {
        }

        public ContainerAction(string Id, string Name, string Hint)
            : this(Id, Name, Hint, null)
        {
        }

        public ContainerAction(string Id, string Name, Image Image)
            : this(Id, Name, string.Empty, Image)
        {
        }

        public ContainerAction(string Id, string Name, string Hint, Image Image)
            : base(Id, null, Name, Hint, Image)
        {
            _Childs = new EventActionList();
            _Childs.ListChanged += ChildAction_ListChanged;
        }

        void ChildAction_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.ItemAdded || e.ListChangedType == ListChangedType.ItemDeleted)
            {
                var act = _Childs[e.ListChangedType == ListChangedType.ItemAdded ? e.NewIndex : e.OldIndex];
                SetParentDataForChild(act, e.ListChangedType != ListChangedType.ItemAdded);
                if (ChildChanged != null)
                    ChildChanged(this, new ContainerActionChildChangeEventArgs(act, e.ListChangedType));
            }
        }

        private void SetParentDataForChild(Action action, bool setNull)
        {
            action.Context = setNull ? null : Context;
            action.Controller = setNull ? null : Controller;

            if (action is ContainerAction)
                foreach (var act in (action as ContainerAction).Childs )
                    SetParentDataForChild(act, setNull);
        }

        private void SetChildContext(Action action)
        {
            action.Context = Context;
            if (action is ContainerAction)
                foreach (var act in (action as ContainerAction).Childs )
                    SetChildContext(act);
        }

        public IList<Action> Childs
        {
            get { return _Childs; }
        }

        protected override void _ContextChanged()
        {
            foreach (Action a in _Childs)
                SetChildContext(a);
        }

        ~ContainerAction()
        {
            _Childs.ListChanged -= ChildAction_ListChanged;
        }
    }

    // =================================================================================================

    public class ActionsController : IEnumerable
    {
        private ActionList _List;

        public ActionsController()
        {
            _List = new ActionList();
        }

        public ActionList List
        {
            get { return _List; }
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        #endregion

        public Action Add(Action Action)
        {
            if (!List.Contains(Action))
                List.Add(Action);
            return Action;
        }

        public void Remove(Action Action)
        {
            List.Remove(Action);
        }

        public Action Find(string Id)
        {
            return List.Find(delegate(Action a)
            {
                return (string.Compare(a.Id, Id, true) == 0);
            });
        }

        public ContainerAction CreateContainer(string Id, string Name, Image Image)
        {
            ContainerAction result = Find(Id) as ContainerAction;
            if (result == null)
            {
                result = Add(new ContainerAction(Id, Name, Image)) as ContainerAction;
            }
            result.Name = Name;
            result.Image = Image;
            return result;
        }

        public ActionList GetAllActions()
        {
            ActionList result = new ActionList();
            FillAllActions(List, result);
            return result;
        }

        private void FillAllActions(IList<Action> List, ActionList result)
        {
            foreach (Action a in List)
            {
                result.Add(a);
                if (a is ContainerAction)
                    FillAllActions((a as ContainerAction).Childs, result);
            }
        }
    }

    // =================================================================================================

    public class PageAction : Action
    {
        public PageAction(string Id, Page Page, string Name)
            : this(Id, Page, Name, string.Empty, null)
        {
        }

        public PageAction(string Id, Page Page, string Name, string Hint)
            : this(Id, Page, Name, Hint, null)
        {
        }

        public PageAction(string Id, Page Page, string Name, Image Image)
            : this(Id, Page, Name, string.Empty, Image)
        {
        }

        public PageAction(string Id, Page Page, string Name, string Hint, Image Image)
            : base(Id, Name, Hint, Image)
        {
            this.Page = Page;
        }

        public Page Page
        {
            get;
            set;
        }

        public override bool DoCanEnter()
        {
            if (_CanEnterCalled)
                return base.DoCanEnter();
            else
            {
                bool result = base.DoCanEnter();
                if (!result)
                    return false;
                if (Page != null)
                {
                    return Page.DoCanEnter(Context);
                }
                return result;
            }
        }

        public override bool DoCanLeave()
        {
            if (_CanLeaveCalled)
                return base.DoCanLeave();
            else
            {
                bool result = base.DoCanLeave();
                if (!result)
                    return false;
                if (Page != null)
                {
                    return Page.DoCanLeave(Context);
                }
                return result;
            }
        }

        public override void DoEnter()
        {
            base.DoEnter();
            Context.ConnectionContext.CurrentPage = Page;
            if (Page != null)
                Page.DoEnter(Context);
        }

        public override void DoLeave()
        {
            if (Page != null)
            {
                Page.DoLeave(Context);
            }
            base.DoLeave();
        }
    }

    // =================================================================================================

}
