﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace OpenShellClasses
{
    public interface IConnection
    {
        RepositoryUserConnection RepositoryUserConnection { get; }
        void Open(IConnectionContext Context);
        void Close(IConnectionContext Context);
        string Name { get; }
        string ConnectionPath { get; }
        //string LoginData { get; }

    }

    // ================================================================================================================

    public enum ConnectionState
    {
        Closed,
        Open
    }

    // ================================================================================================================

    public enum ConnectionPropertyStringKind
    {
        Database,
        User,
        Mode
    }

    // ================================================================================================================

    public abstract class Connection : IDisplayItem, IConnection
    {
        protected ConnectionType _Type;
        private ConnectionState _State = ConnectionState.Closed;
        private Image _Image;
        private RepositoryUserConnection _RepositoryUserConnection;
        private bool _PersistInfo;

        protected Connection(ConnectionType Type) 
        {
            this._Type = Type;
            _Image = Type.Image;
        }
        
        public ConnectionType Type
        {
            get { return _Type; }
        }

        public ConnectionState State
        {
            get { return _State; }
        }

        protected virtual void SetState(ConnectionState NewState)
        {
            _State = NewState;
        }

        public abstract void Open(IConnectionContext Context);

        public abstract void Close(IConnectionContext Context);

        #region IDisplayItem Members

        public string Name
        {
            get { return GetName(); }
        }

        public Image Image
        {
            get { return _Image; }
            set { _Image = value; }
        }

        public string Hint
        {
            get { return string.Empty; }
        }

        public bool Visible
        {
            get { return true; }
        }

        public bool Enabled
        {
            get { return true; }
        }

        #endregion

        protected abstract string GetName();

        public virtual string ConnectionPath
        {
            get
            {
                return GetConnectionPath();
            }
        }

        protected abstract string GetConnectionPath();

        public override string ToString()
        {
            return Name;
        }

        #region IConnection Members

        public RepositoryUserConnection RepositoryUserConnection
        {
            get { return _RepositoryUserConnection; }
            set { _RepositoryUserConnection = value; }
        }

        public bool PersistInfo
        {
            get { return _PersistInfo; }
            set { _PersistInfo = value; }
        }

        #endregion

        public string LoginData;

        public virtual string GetPropertyString(ConnectionPropertyStringKind Kind)
        {
            return string.Empty;
        }
    }

    // ================================================================================================================

    public class ConnectionList : List<Connection>
    {
    }


    public delegate void ConnectionEventHandler(object sender, ConnectionEventArgs e);

    // =================================================================================================

    public class ConnectionEventArgs
    {
        private IConnectionContext _ConnectionContext;
        private Connection _Connection;

        public ConnectionEventArgs(IConnectionContext ConnectionContext)
        {
            this._ConnectionContext = ConnectionContext;
            this._Connection = ConnectionContext.Connection;
        }

        public IConnectionContext ConnectionContext
        {
            get { return _ConnectionContext; }
        }

        public Connection Connection
        {
            get { return _Connection; }
        }
    }


}
