﻿// =================================================================================================
//  ConnectionTypes.cs
//  Типы соединений
// =================================================================================================

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace OpenShellClasses
{
    // =================================================================================================

    /// <summary>
    /// абстрактный класс тип соединения
    /// </summary>
    public abstract class ConnectionType : IDisplayItem
    {
        private string _Name;
        private string _Hint;
        private Image _Image;
        private int _ClassId;

        public ConnectionType()
        {
        }

        #region IDisplayItem Members

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public Image Image
        {
            get { return _Image; }
            set { _Image = value; }
        }

        public string Hint
        {
            get { return _Hint; }
            set { _Hint = value; }
        }

        public bool Visible
        {
            get { return true; }
        }

        public bool Enabled
        {
            get { return true; }
        }

        public override string ToString()
        {
            return Name;
        }

        #endregion

        public int ClassId
        {
            get { return _ClassId; }
            set { _ClassId = value; }
        }

        public abstract Connection CreateConnection(Control loginControl);

        /// <summary>
        /// возвращает WinForms контрол для ввода регистрационной информации
        /// </summary>
        /// <returns></returns>
        public abstract Control GetLoginControl();

        public abstract bool LoginControl_Validate(Control control);

        public abstract string LoginControl_GetData(Control control);

        public abstract void LoginControl_SetData(Control control, string data);

        internal void UpdateProps(ConnectionType type)
        {
            this.ClassId = type.ClassId;
            this.Name = type.Name;
            this.Hint = type.Hint;
        }
    }

    // =================================================================================================

    public class ConnectionTypeList : List<ConnectionType>
    {

        public ConnectionTypeList()
        {

        }

        public ConnectionTypeList(IEnumerable<ConnectionType> list) : base(list)
        {

        }
        public virtual void AddRange(ConnectionType[] range)
        {
            foreach (ConnectionType ct in range)
                Add(ct);
        }

        public virtual new bool Add(ConnectionType type)
        {
            ConnectionType ct = Find(type.GetType().FullName);
            if (ct == null)
            {
                base.Add(type);
                return true;
            }
            else
            {
                ct.UpdateProps(type);
                return false;
            }
        }

        public ConnectionType Find(string type)
        {
            foreach (ConnectionType ct in this)
                if (ct.GetType().FullName == type)
                    return ct;
            return null;
        }

    }

    // =================================================================================================

    /// <summary>
    /// атрибут, показывающий на то, что тип соединения надо автоматически зарегистрировать как доступный при старте УО
    /// этим типом помечены по умолчанию два класса - DbConnection и DbRepositoryConnection
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ConnectionTypeAutoRegisterAttribute : Attribute
    {
        /// <summary>
        /// поиск всех типов соединений с атрибутом ConnectionTypeAutoRegister
        /// </summary>
        /// <returns></returns>
        public static ConnectionType[] GetList()
        {
            List<ConnectionType> result = new List<ConnectionType>();
            foreach (Assembly asm in AppDomain.CurrentDomain.GetAssemblies())
                foreach (Type type in asm.GetTypes())
                    if (type.GetCustomAttributes(typeof(ConnectionTypeAutoRegisterAttribute), false).Length > 0)
                    {
                        result.Add(Activator.CreateInstance(type) as ConnectionType);
                    }
            return result.ToArray();
        }
    }

    // =================================================================================================

    /// <summary>
    /// данным атрибутом помечаются все типы соединений, которые предоставляют соединение с репозиторием
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ConnectionTypeRepositoryAttribute : Attribute
    {
        public static bool IsDefined(ConnectionType ct)
        {
            return (ct.GetType().GetCustomAttributes(typeof(ConnectionTypeRepositoryAttribute), false).Length > 0);
        }
    }

    // =================================================================================================

}
