﻿// =================================================================================================
//  Module.cs
//  Классы для определения загружаемых модулей элементов отображения
// =================================================================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
using System.Xml.Serialization;
using OpenShellClasses.Utils;

namespace OpenShellClasses
{
    // ================================================================================================}

    #region Интерфейс IModule: Подключаемый к оболочке модуль

    /// <summary>
    /// Подключаемый к оболочке модуль
    /// </summary>
    public interface IModule
    {
        /// <summary>
        /// метод, в котором для модуля задается его контекст подключения
        /// </summary>
        /// <param name="Context"></param>
        void SetContext(IModuleContext Context);

        /// <summary>
        /// основной метод загрузки модуля
        /// </summary>
        void Load();

        /// <summary>
        /// основной метод выгрузки модуля
        /// </summary>
        void Unload();

        void Update();
    }

    #endregion

    // ================================================================================================

    public class IModuleList : List<IModule>
    {

    }

    // ================================================================================================

    public abstract class ModuleBase : IModule
    {
        private IModuleContext _Context;

        #region IModule Members

        public void SetContext(IModuleContext Context)
        {
            _Context = Context;
        }

        public IModuleContext Context
        {
            get { return _Context; }
        }

        public abstract void Load();

        public abstract void Unload();

        #endregion

        #region IModule Members


        public virtual void Update()
        {            
        }

        #endregion
    }


    #region Класс ModuleInfo: информация о модуле

    /// <summary>
    /// информация о модуле
    /// </summary>
    [Serializable]
    [XmlType(TypeName = "Info")]
    public class ModuleInfo
    {
        // ---------------------------------

        #region закрытые поля

        private string _Id = string.Empty;
        private string _Name                = string.Empty;
        private string _Description         = string.Empty;
        private string _Author              = string.Empty;
        private string _Version             = string.Empty;
        private string _AssemblyVersion     = string.Empty;
        private string _Comment             = string.Empty;
        private string _Copyright           = string.Empty;
        private PropertyList _ExtraProperties;

        #endregion

        // ---------------------------------

        #region конструкторы

        private ModuleInfo()
        {
            _ExtraProperties = new PropertyList();
        }

        // ---------------------------------

        public ModuleInfo(string Name) : this()
        {
            this.Name = Name;
        }

        // ---------------------------------

        public ModuleInfo(string Name, string Version)
            : this(Name)
        {
            this.Version = Version;
        }

        // ---------------------------------

        public ModuleInfo(string Name, string Version, string Description)
            : this(Name, Version)
        {
            this.Description = Description;
        }

        // ---------------------------------

        public ModuleInfo(string Name, string Version, string Description, string Author)
            : this(Name, Version, Description)
        {
            this.Author = Author;
        }

        // ---------------------------------

        public ModuleInfo(string Name, string Version, string Description, string Author, string Copyright)
            : this(Name, Version, Description, Author)
        {
            this.Copyright = Copyright;
        }

        public static ModuleInfo GetModuleInfo(Type moduleType)
        {
            ModuleInfoAttribute[] attrs = (ModuleInfoAttribute [])(moduleType.GetCustomAttributes(typeof(ModuleInfoAttribute), true));
            if (attrs.Length > 0)
                return (attrs[0].Info);
            else
            {
                return new ModuleInfo(moduleType.Name);
            }
        }

        #endregion

        // ---------------------------------

        #region свойства

        /// <summary>
        /// возвращает пустое описание модуля
        /// </summary>
        public static ModuleInfo Empty
        {
            get
            {
                return new ModuleInfo(
                    "<нет информации об имени модуля>", 
                    "<нет информации о версии модуля>",
                    "<не задан атрибут ModuleInfo в описании класса модуля>",
                    "<автор модуля неизвестен>");
            }
        }

        // ---------------------------------

        /// <summary>
        /// отображаемое пользователю наименование модуля
        /// </summary>
        [XmlAttribute]
        [DefaultValue("")]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        // ---------------------------------

        /// <summary>
        /// текстовое описание действий, выполняемых модулем
        /// </summary>
        [DefaultValue("")]
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        // ---------------------------------

        /// <summary>
        /// текстовая строка об авторе модуля
        /// </summary>
        [XmlAttribute]
        [DefaultValue("")]
        public string Author
        {
            get { return _Author; }
            set { _Author = value; }
        }

        // ---------------------------------

        /// <summary>
        /// строка версии модуля
        /// </summary>
        [XmlAttribute]
        [DefaultValue("")]
        public string Version
        {
            get { return _Version; }
            set { _Version = value; }
        }

        // ---------------------------------

        /// <summary>
        /// строка версии сборки (dll файла) модуля
        /// </summary>
        [XmlAttribute]
        [DefaultValue("")]
        public string AssemblyVersion
        {
            get { return _AssemblyVersion; }
            set { _AssemblyVersion = value; }
        }

        // ---------------------------------

        /// <summary>
        /// текстовое примечание модуля
        /// </summary>
        [DefaultValue("")]
        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        // ---------------------------------

        /// <summary>
        /// информация правового характера
        /// </summary>
        [DefaultValue("")]
        public string Copyright
        {
            get { return _Copyright; }
            set { _Copyright = value; }
        }

        // ---------------------------------

        public PropertyList ExtraProperties
        {
            get { return _ExtraProperties; }
        }

        #endregion

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Name))
                return Name;
            else
                return base.ToString();
        }
    }

    #endregion

    // ================================================================================================

    public class ModuleInfoAttribute : Attribute
    {
        private ModuleInfo _Info;

        public ModuleInfoAttribute(ModuleInfo Info)
        {
            this._Info = Info;
        }

        public ModuleInfoAttribute(string Name) : 
            this(new ModuleInfo(Name))
        {
        }

        // ---------------------------------

        public ModuleInfoAttribute(string Name, string Version) :
            this(new ModuleInfo(Name, Version))
        {
        }

        // ---------------------------------

        public ModuleInfoAttribute(string Name, string Version, string Description)
            : this(new ModuleInfo(Name, Version, Description))
        {
        }

        // ---------------------------------

        public ModuleInfoAttribute(string Name, string Version, string Description, string Author)
            : this(new ModuleInfo(Name, Version, Description, Author))
        {
        }

        // ---------------------------------

        public ModuleInfoAttribute(string Name, string Version, string Description, string Author, string Copyright)
            : this(new ModuleInfo(Name, Version, Description, Author, Copyright))
        {
        }

        // ---------------------------------
        
        public ModuleInfo Info
        {
            get { return _Info; }
        }
    }

    #region Класс ModuleInfoList - коллекция информации о модулях

    public class ModuleInfoList : List<ModuleInfo>
    {
    }

    #endregion

    // ================================================================================================

    #region Интерфейс IModuleInfoQuerable: запрос информации о модуле динамически

    public interface IModuleInfoQuerable
    {
        ModuleInfo GetModuleInfo();
    }

    #endregion

    // ================================================================================================

    #region ModuleConnectionTypeBindingAttribute - привязка модуля к типу соединения

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ModuleConnectionBindingAttribute: Attribute
    {
        private Type _ConnectionType;
        private bool _Inheritable;

        /// <summary>
        /// атрибут, определяющий тип соединения, используемый модулем
        /// </summary>
        /// <param name="ConnectionType">тип соединения, производный от ConnectionType</param>
        /// <param name="Inheritable">флаг, указывающий на то, учитывать привязку модуля к типу соединения в наследуемых типах соединений</param>
        public ModuleConnectionBindingAttribute(Type ConnectionType, bool Inheritable)
        {
            this._Inheritable = Inheritable;
            this._ConnectionType = ConnectionType;
            if (!ConnectionType.IsSubclassOf(typeof(ConnectionType)))
                throw new ArgumentException(Strings.ModuleConnectionBindingAttribute_ClassType_error);
        }

        public Type ConnectionType
        {
            get { return _ConnectionType; }
        }

        public bool Inheritable
        {
            get { return _Inheritable; }
        }
    }


    #endregion

    // ================================================================================================

}
