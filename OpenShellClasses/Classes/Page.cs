﻿using System;
using System.Windows.Forms;

namespace OpenShellClasses
{
    public class Page
    {
        private Type _ControlType;
        private object[] _ControlCreateParams;
        private Control _Control;
        private IPageEvents _Events;
        private ActionsController _MainToolBarActions;

        public event PageCancelEventHandler CanLeave;
        public event PageCancelEventHandler CanEnter;
        public event PageNotifyEventHandler Leave;
        public event PageNotifyEventHandler Enter;

        public Page(Type controlType)
        {
            _ControlType = controlType;
            _MainToolBarActions = new ActionsController();
        }

        public Page(Type controlType, params object[] createParams) : this(controlType)
        {
            _ControlCreateParams = createParams;
        }

        public Page(Control control)
        {
            _Control = control;
            _MainToolBarActions = new ActionsController();
            _Events = control as IPageEvents;
        }

        public Control Control
        {
            get 
            {
                if (_Control == null && _ControlType != null)
                {
                    _Control = Activator.CreateInstance(_ControlType, _ControlCreateParams) as Control;
                    _Events = _Control as IPageEvents;
                }
                return _Control; 
            }
        }

        public virtual bool DoCanEnter(IModuleContext Connection)
        {
            _InitControl(Connection);

            bool result = true;
            if (Control is IPageEvents)
                result = (Control as IPageEvents).CanEnterPage(Connection);
            if (CanEnter != null)
            {
                PageCancelEventArgs args = new PageCancelEventArgs(this, !result);
                CanEnter(this, args);
                result = !args.Cancel;
            }
            return result;
        }

        private void _InitControl(IModuleContext Connection)
        {
            if (_Control == null)
            {
                IPageEvents events = Control as IPageEvents;
                if (events != null)
                    events.CreatePageControl(Connection);
            }
        }

        public virtual bool DoCanLeave(IModuleContext Connection)
        {
            bool result = true;
            if (Control is IPageEvents)
                result = (Control as IPageEvents).CanLeavePage(Connection);
            if (CanLeave != null)
            {
                PageCancelEventArgs args = new PageCancelEventArgs(this, !result);
                CanLeave(this, args);
                result = !args.Cancel;
            }
            return result;
        }

        public virtual void DoEnter(IModuleContext Connection)
        {
            if (Control is IPageEvents)
                (Control as IPageEvents).EnterPage(Connection);

            if (Enter != null)
                Enter(this, new PageNotifyEventArgs(this));
        }

        public virtual void DoLeave(IModuleContext Connection)
        {
            if (Control is IPageEvents)
                (Control as IPageEvents).LeavePage(Connection);

            if (Leave != null)
                Leave(this, new PageNotifyEventArgs(this));
        }
    }

    public delegate void PageCancelEventHandler(object sender, PageCancelEventArgs e);
    public delegate void PageNotifyEventHandler(object sender, PageNotifyEventArgs e);

    public class PageNotifyEventArgs : EventArgs
    {
        private Page _Page;
        private Control _Control;

        public PageNotifyEventArgs(Page Page)
        {
            _Page = Page;
            _Control = Page.Control;
        }

        public Page Page
        {
            get { return _Page; }
        }

        public Control Control
        {
            get { return _Control; }
        }
    }

    public class PageCancelEventArgs : PageNotifyEventArgs
    {
        private bool _Cancel;

        public PageCancelEventArgs(Page Page, bool Cancel) : base(Page)
        {
            _Cancel = Cancel;
        }

        public bool Cancel
        {
            get { return _Cancel; }
            set { _Cancel = value; }
        }
    }

    public interface IPageEvents
    {
        void CreatePageControl(IModuleContext context);

        bool CanEnterPage(IModuleContext context);
        bool CanLeavePage(IModuleContext context);

        void EnterPage(IModuleContext context);
        void LeavePage(IModuleContext context);
    }
}
