﻿// =================================================================================================
//  Repository.cs
//  Служебные классы для определения интерфейса IRepository
// =================================================================================================

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace OpenShellClasses
{
    // =================================================================================================

    /// <summary>
    /// интерфейс управления репозиторием
    /// </summary>
    public interface IRepository : IConnection
    {
        /// <summary>
        /// метод, вызываемые при открытии репозитория 
        /// после регистрации его в качестве текущего для контекста приложения
        /// </summary>
        /// <param name="Context">контекст соединения</param>
        void OpenRepository(IConnectionContext Context);

        /// <summary>
        /// метод, вызываемые перед закрытием репозитория 
        /// </summary>
        /// <param name="Context">контекст соединения</param>
        void CloseRepository(IConnectionContext Context);

        /// <summary>
        /// глобальные настройки универсальной оболочки
        /// </summary>
        RepositoryStorage StorageShell { get; }

        RepositoryClassList Classes
        {
            get;
        }

        RepositoryUserClassLinkList UserClasses
        {
            get;
        }

        void UpdateUserClassLink(RepositoryClass Class, RepositoryUpdateUserClassLinkAction Action);

        RepositoryUserConnectionList GetUserConnectionList();

        void LoadUserConfiguration();

        /// <summary>
        /// сохранение соединения в репозиторий
        /// </summary>
        /// <param name="connection"></param>
        RepositoryUserConnection SaveConnection(IConnectionContext connection);

        RepositoryStorage GetStorage(RepositoryUserConnection conn, string scope);

        void RemoveUserConnection(RepositoryUserConnection repositoryUserConnection);
    }

    // =================================================================================================

    /// <summary>
    /// RepositoryClassType - ссылка на класс объекта в репозитории
    /// </summary>
    [Serializable]
    [XmlType(TypeName="ClassType")]
    public class RepositoryClassType
    {
        private string _Code;
        private string _Name;
        private string _SubclassOf;

        [XmlAttribute("id")]
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        [XmlAttribute("name")]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        [XmlAttribute("subclass_of")]
        public string SubclassOf
        {
            get { return _SubclassOf; }
            set { _SubclassOf = value; }
        }

        public override string ToString()
        {
            return string.Format("{0} ({1}) : {2}", Code, Name, SubclassOf);
        }
    }

    // =================================================================================================

    public class RepositoryClassTypeList : List<RepositoryClassType>
    {
        public RepositoryClassType this[string code]
        {
            get
            {
                return Find(delegate (RepositoryClassType ct)
                {
                    return (string.Compare(ct.Code, code, true) == 0);
                });
            }
        }

        public bool Contains(string code)
        {
            return (this[code] != null);
        }
    }

    // =================================================================================================

    public enum RepositoryClassDefaultStatus
    {
        Disabled,
        Enabled,
        Always
    }

    // =================================================================================================

    /// <summary>
    /// ссылка на класс (US_CLASSES)
    /// </summary>
    [Serializable]
    [XmlType("Class")]
    public class RepositoryClass
    {
        #region закрытые поля

        private int _Code;
        private string _Name;
        private RepositoryClassType _ClassType;
        private ClassPath _ClassPath;
        private bool _Enabled = true;
        private RepositoryClassDefaultStatus _DefaultStatus = RepositoryClassDefaultStatus.Disabled;
        private int _Parent;

        #endregion

        // -------------------------------------------------------------------------------------------

        #region конструктор

        public RepositoryClass()
        {
            _ClassPath = new ClassPath();
            _ClassPath.ClassRef = this;
        }

        #endregion

        // -------------------------------------------------------------------------------------------

        #region свойства

        [XmlAttribute(AttributeName="id")]
        public int Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        [XmlAttribute(AttributeName = "name")]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        [XmlIgnore]
        public RepositoryClassType ClassType
        {
            get { return _ClassType; }
            set { _ClassType = value; }
        }

        [XmlAttribute(AttributeName = "type")]
        public string ClassTypeCode
        {
            get
            {
                if (_ClassType != null)
                    return _ClassType.Code;
                else
                    return null;
            }
            set
            {
                _ClassType = new RepositoryClassType();
                _ClassType.Code = value;
            }
        }

        [XmlIgnore]
        public string ClassTypeName
        {
            get
            {
                if (ClassType != null)
                    return ClassType.Name;
                else
                    return "<ClassType = null>";
            }
        }

        [XmlIgnore]
        public ClassPath ClassPath
        {
            get { return _ClassPath; }
            set { _ClassPath = value; }
        }

        [XmlAttribute(AttributeName = "path")]
        public string ClassPathString
        {
            get { return _ClassPath.AsString; }
            set { _ClassPath.AsString = value; }
        }

        [XmlAttribute(AttributeName="enabled")]
        [System.ComponentModel.DefaultValue(true)]
        public bool Enabled
        {
            get { return _Enabled; }
            set { _Enabled = value; }
        }

        [XmlIgnore]
        public RepositoryClassDefaultStatus DefaultStatus
        {
            get { return _DefaultStatus; }
            set { _DefaultStatus = value; }
        }

        [XmlAttribute(AttributeName = "default_status")]
        [System.ComponentModel.DefaultValue("-")]
        public string DefaultStatusString
        {
            get
            {
                return Util.ConvertDefaultStatus(_DefaultStatus);
            }
            set
            {
                _DefaultStatus = Util.ConvertDefaultStatusStr(value);
            }
        }

        [XmlAttribute(AttributeName = "parent")]
        [System.ComponentModel.DefaultValue(0)]
        public int Parent
        {
            get
            {
                return _Parent;
            }
            set
            {
                _Parent = value;
            }
        }


        #endregion

        public override string ToString()
        {
            return string.Format("{0}: {1}", ClassTypeName, Name);
        }
    }

    // =================================================================================================

    [Serializable]
    public class RepositoryClassList : List<RepositoryClass>
    {
        public RepositoryClassList GetListByType(string code)
        {
            RepositoryClassList result = new RepositoryClassList();
            foreach (RepositoryClass cls in this)
                if (string.Compare(cls.ClassType.Code, code, true) == 0)
                    result.Add(cls);
            return result;
        }

        internal RepositoryClass Find(int id)
        {
            return base.Find(
                delegate(RepositoryClass cls)
                {
                    return (cls.Code == id);
                });
        }

        public RepositoryClass GetConnectionClass(Type type, ClassPathLoader loader)
        {
            foreach (RepositoryClass cls in this.GetListByType("CONNECTION_TYPE"))
                if (cls.ClassPath.TypeName.ToLower().Contains(type.Name.ToLower()))
                {
                    Type t = loader.LoadType(cls.ClassPath);
                    if (t == null)
                        throw new ArgumentException(string.Format(
                            "Невозможно загрузить класс {0} (путь к классу - {1})", cls.Name, cls.ClassPath));
                    if (t.FullName == type.FullName)
                        return cls;
                }
            return null;
        }
    }

    // =================================================================================================

    [Serializable]
    public class RepositoryUserClassLink
    {
        private RepositoryClass _Class;
        private bool _IsConnected = true;
        private bool _IsNew;

        public RepositoryUserClassLink()
        {

        }

        public RepositoryUserClassLink(RepositoryClass Class, bool IsConnected, bool IsNew)
        {
            _Class = Class;
            _IsConnected = IsConnected;
            _IsNew = IsNew;
        }

        [XmlIgnore]
        public RepositoryClass Class
        {
            get { return _Class; }
            set { _Class = value; }
        }

        [XmlAttribute(AttributeName = "class")]
        public int ClassId
        {
            get { return (_Class != null) ? _Class.Code : 0; }
            set
            {
                _Class = new RepositoryClass();
                _Class.Code = value;
            }
        }

        [XmlAttribute(AttributeName="connected")]
        [System.ComponentModel.DefaultValue(true)]
        public bool IsConnected
        {
            get { return _IsConnected; }
            set { _IsConnected = value; }
        }

        [XmlAttribute(AttributeName = "new")]
        [System.ComponentModel.DefaultValue(false)]
        public bool IsNew
        {
            get { return _IsNew; }
        }
    }

    // =================================================================================================

    [Serializable]
    public class RepositoryUserClassLinkList : List<RepositoryUserClassLink>
    {
        internal RepositoryUserClassLink[] GetEnabledLinks(RepositoryClassType repositoryClassType)
        {
            RepositoryUserClassLinkList result = new RepositoryUserClassLinkList();
            foreach (RepositoryUserClassLink l in this)
                if (l.Class.ClassType == repositoryClassType)
                    if (l.IsConnected || l.Class.DefaultStatus == RepositoryClassDefaultStatus.Always)
                        result.Add(l);
            return result.ToArray();
        }

        public RepositoryUserClassLink Find(int ClassId)
        {
            return Find(delegate(RepositoryUserClassLink l)
            {
                return l.ClassId == ClassId;
            });
        }

        internal void SortById()
        {
            Sort(_CompareClassLink);
        }

        private int _CompareClassLink(RepositoryUserClassLink x, RepositoryUserClassLink y)
        {
            return x.ClassId.CompareTo(y.ClassId);
        }
    }

    // =================================================================================================

    public enum RepositoryUpdateUserClassLinkAction
    {
        SetDisabled = 0,
        SetEnabled = 1,
        Clear = 2
    }

    // =================================================================================================

    [Serializable]
    public class RepositoryUserConnection
    {
        private long _Id;
        private RepositoryClass _ConnectionTypeClass;
        private string _LoginPath;
        private string _LoginData;
        private DateTime _TimeCreated;
        private string _Name;
        private DateTime _TimeLogin;
        private string _User;
        private string _Mode;
        private string _Database;

        public RepositoryUserConnection()
        {
        }

        public RepositoryUserConnection(IConnectionContext context)
        {
            this.ConnectionTypeClass = context.ConnectionClass;
            this.ConnectionPath = context.Connection.ConnectionPath;
            this.LoginData = context.Connection.LoginData;
            this.TimeCreated = DateTime.Now;
            this.TimeLogin = DateTime.Now;
            this.Name = context.Connection.Name;

            _Database = context.Connection.GetPropertyString(ConnectionPropertyStringKind.Database);
            _Mode = context.Connection.GetPropertyString(ConnectionPropertyStringKind.Mode);
            _User = context.Connection.GetPropertyString(ConnectionPropertyStringKind.User);
        }

        [XmlAttribute]
        public string Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }

        [XmlAttribute]
        public string User
        {
            get { return _User; }
            set { _User = value; }
        }

        [XmlAttribute]
        public string Database
        {
            get { return _Database; }
            set { _Database = value; }
        }

        [XmlAttribute(AttributeName="id")]
        public long Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        [XmlIgnore]
        public RepositoryClass ConnectionTypeClass
        {
            get { return _ConnectionTypeClass; }
            set { _ConnectionTypeClass = value; }
        }

        [XmlAttribute(AttributeName="conn_type_class")]
        public int ConnectionTypeClassCode
        {
            get
            {
                return _ConnectionTypeClass.Code;
            }
            set
            {
                _ConnectionTypeClass = new RepositoryClass();
                _ConnectionTypeClass.Code = value; 
            }
        }

        public string ConnectionPath
        {
            get { return _LoginPath; }
            set { _LoginPath = value; }
        }

        public string LoginData
        {
            get { return _LoginData; }
            set { _LoginData = value; }
        }

        public DateTime TimeCreated
        {
            get { return _TimeCreated; }
            set { _TimeCreated = value; }
        }


        public DateTime TimeLogin
        {
            get { return _TimeLogin; }
            set { _TimeLogin = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        internal void UpdateProps(RepositoryUserConnection result)
        {
            _LoginPath = result.ConnectionPath;
            _LoginData = result.LoginData;
            _Name = result.Name;
            _TimeLogin = DateTime.Now;

            _Database = result.Database;
            _Mode = result.Mode;
            _User = result.User;
        }
    }

    // =================================================================================================

    [Serializable]
    public class RepositoryUserConnectionList : List<RepositoryUserConnection>
    {
        internal void SortByTimeLoginDesc()
        {
            Sort(new Comparison<RepositoryUserConnection>(SortCompareTimeLogin));
        }

        private int SortCompareTimeLogin(RepositoryUserConnection c1, RepositoryUserConnection c2)
        {
            return DateTime.Compare(c2.TimeLogin, c1.TimeLogin);
        }
    }

    // =================================================================================================
}
