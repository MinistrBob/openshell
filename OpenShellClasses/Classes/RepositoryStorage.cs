﻿using System;

namespace OpenShellClasses
{
    public abstract class RepositoryStorage
    {
        public abstract object ReadValue(string Path);
        public abstract bool ValueExists(string Path);
        public abstract void WriteValue(string Path, object Value);

        public void ReadPropertyValues(string Path, PropertyList PropList)
        {
            PropList.DeserializeString(Convert.ToString(ReadValue(Path)));
        }

        public void WritePropertyValues(string Path, PropertyList PropList)
        {
            WriteValue(Path, PropList.Serialize(false));
        }
    }

/*    public class RepositoryStorageElementInfo
    {
        private long _Id;
        private string _Name;
        private string _Path;
        private RepositoryStorageElementInfo _Parent;
        private string _Caption;
        private string _Description;

        public RepositoryStorageElementInfo(string Path)
        {
            this._Path = Path;
            this._Name = Path;
        }

        public RepositoryStorageElementInfo(RepositoryStorageElementInfo Parent, string Name)
            : this(RepositoryStorage.PathCombine(Parent._Path, Name))
        {
            this._Name = Name;
        }

        public long Id
        {
            get { return _Id; }
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
    }

    public class RepositoryStorageKey
    {
        private IRepositoryStorageHandler _Handler;
        private RepositoryStorageElementInfo _Info;


        public RepositoryStorageKey(IRepositoryStorageHandler Handler, RepositoryStorageElementInfo Info)
        {
            _Handler = Handler;
            _Info = Info;
        }

        public RepositoryStorageKey CreateSubKey(string Path)
        {
            if (Path.Contains(RepositoryStorage.PathDelimiter.ToString()))
            {
                string[] path_parts = Path.Split(RepositoryStorage.PathDelimiter);
                RepositoryStorageKey parent_key = this;
                foreach (string path_part in path_parts)
                    parent_key = parent_key.CreateSubKey(path_part);
                return parent_key;
            }
            else
                return CreateSubKey(new RepositoryStorageElementInfo(this._Info, Path));
        }

        private RepositoryStorageKey CreateSubKey(RepositoryStorageElementInfo NewKeyInfo)
        {
            return _Handler.KeyCreate(this, NewKeyInfo);
        }

        public RepositoryStorageElementInfo Info
        {
            get { return _Info; }
        }
/*
        public RepositoryStorageKey OpenSubKey(string KeyName)
        {
            return OpenSubKey(KeyName, true);
        }

        public RepositoryStorageKey OpenSubKey(string KeyName, bool createIfNotExists)
        {
            if (createIfNotExists)
                return CreateSubKey(KeyName);
            else
            {
                if (_Handler.KeyExists(RepositoryStorage.PathCombine(_Path, KeyName)))
                    return new RepositoryStorageKey(_Handler, RepositoryStorage.PathCombine(_Path, KeyName));
                else
                    ThrowKeyNotExists();
            }
            return null;
        }

        private void ThrowKeyNotExists()
        {
            throw new ArgumentException("Repository storage key not exists!");
        }

        public string[] GetSubKeyNames(string KeyName)
        {
            if (!_Handler.KeyExists(RepositoryStorage.PathCombine(_Path, KeyName)))
                ThrowKeyNotExists();

            return _Handler.KeyGetNames(RepositoryStorage.PathCombine(_Path, KeyName));
        }*//*
    }

    public class RepositoryStorage 
    {
        private IRepositoryStorageHandler _Handler;
        private RepositoryStorageElementInfo _RootKeyInfo;
        private RepositoryStorageKey _RootKey;

        public const char PathDelimiter = '\\';

        public RepositoryStorage(IRepositoryStorageHandler Handler)
        {
            this._Handler = Handler;
            CreateRootKey();
            RootKey.CreateSubKey("test\\test123\\test234");
        }

        protected IRepositoryStorageHandler Handler
        {
            get { return _Handler; }
        }

        public RepositoryStorageKey RootKey
        {
            get { return _RootKey; }
        }

        private void CreateRootKey()
        {
            _RootKey = new RepositoryStorageKey(Handler, 
                new RepositoryStorageElementInfo("."));
        }

        public static string PathCombine(string path1, string path2)
        {
            return path1 + PathDelimiter + path2;
        }

    }

    public interface IRepositoryStorageHandler
    {
        RepositoryStorageKey KeyCreate(RepositoryStorageKey Parent, RepositoryStorageElementInfo Key);
        void KeyDelete(RepositoryStorageKey Parent, RepositoryStorageElementInfo Key);
        string[] KeyGetNames(string Path);
        bool KeyExists(string Path);
    }*/
}
