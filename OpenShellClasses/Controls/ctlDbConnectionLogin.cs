﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml;
using Devart.Data.Oracle;                                                                                                                             using OpenShellClasses.Utils;
using OpenShellClasses.Utils;

namespace OpenShellClasses
{
    public partial class ctlDbConnectionLogin : UserControl
    {
        public ctlDbConnectionLogin()
        {
            InitializeComponent();

            cbMode.SelectedIndex = 0;

            string[] tempArray = OpenShellClasses.Util.GetOracleDbNames();
            
            if (Properties.Settings.Default.UseOracleClient)
            {
                if (tempArray.Length!=0)
                {
                    tbDatabase.Items.AddRange(tempArray);

                    cbHomes.Items.Clear();
                    foreach (OracleHome Home in OracleConnection.Homes)
                        cbHomes.Items.Add(Home.Name);
                    cbHomes.SelectedItem = OracleConnection.Homes.DefaultHome.Name; 
                }
                else
                {
                    tbDatabase.Items.Add("Нет ORACLE_HOME");
                    tbDatabase.SelectedIndex = 0;

                    cbHomes.Items.Clear();
                    cbHomes.Items.Add("Нет ORACLE_HOME");
                    cbHomes.SelectedIndex = 0;
                }

            }
            else
            {
                tabControl1.TabPages.Remove(tabPage2);
            }

            UpdateUI();
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool Direct
        {
            get { return (tabControl1.SelectedTab == tabPage1); }
            set
            {
                if (value)
                    tabControl1.SelectedTab = tabPage1;
                else
                {
                    if (tabControl1.TabPages.Contains(tabPage2))
                        tabControl1.SelectedTab = tabPage2;
                }
            }
        }

        public DbConnectionParams ConnectionParams
        {
            get
            {
                return new DbConnectionParams(tbUserName.Text, tbPassword.Text, tbDatabase.Text,
                    cbSavePassword.Checked, cbMode.Text, cbHomes.Text, 
                    Direct, Convert.ToInt32(udPort.Value), tbSID.Text, tbADDR.Text);
            }
            set
            {
                if (value != null)
                {
                    tbUserName.Text = value.UserName;
                    tbPassword.Text = value.Password.Text;
                    tbDatabase.Text = value.Database;
                    cbMode.SelectedIndex = cbMode.Items.IndexOf(value.Mode);
                    if (cbMode.SelectedIndex == 0)
                        cbMode.SelectedIndex = 0;
                    Direct = value.Direct;
                    cbHomes.SelectedItem = value.Home;
                    tbSID.Text = value.SID;
                    tbADDR.Text = value.Server;
                    udPort.Value = value.Port;
                }
                else
                {
                    tbUserName.Text = string.Empty;
                    tbPassword.Text = string.Empty;
                    tbDatabase.Text = string.Empty;
                    cbMode.SelectedIndex = 0;
                    Direct = true;
                    cbHomes.SelectedIndex = 0;
                }
                UpdateUI(); 
            }
        }

        public bool ValidateData()
        {
            DbConnectionParams pars = ConnectionParams;

            errorProvider1.Clear();
            if (string.IsNullOrEmpty(pars.UserName))
            {
                tbUserName.Focus();
                errorProvider1.SetError(tbUserName, Strings.DbConnectionLogin_Validate_NoUserName);
            }
            else if (string.IsNullOrEmpty(pars.Password.Text))
            {
                tbPassword.Focus();
                errorProvider1.SetError(tbPassword, Strings.DbConnectionLogin_Validate_NoPassword);
            }
            else if (string.IsNullOrEmpty(pars.Database) && !pars.Direct)
            {
                tbDatabase.Focus();
                errorProvider1.SetError(tbDatabase, Strings.DbConnectionLogin_Validate_NoDatabase);
            }
            else if (pars.Direct && string.IsNullOrEmpty(pars.SID))
            {
                tbSID.Focus();
                errorProvider1.SetError(tbSID, Strings.DbConnectionLogin_Validate_Direct_NoSID);
            }
            else if (pars.Direct && string.IsNullOrEmpty(pars.Server))
            {
                tbSID.Focus();
                errorProvider1.SetError(tbSID, Strings.DbConnectionLogin_Validate_Direct_NoServer);
            }
            else
                return true;
            return false;
        }

        public void SetLoginData(string data)
        {
            if (data.Contains("@") && data.Contains("/"))
            {
                tbUserName.Text = data.Remove(data.IndexOf("/"));
                if (tbUserName.Text.Contains(" AS "))
                {
                    cbMode.SelectedIndex = cbMode.Items.IndexOf(
                        tbUserName.Text.Substring(
                        tbUserName.Text.IndexOf(" AS ") + 4));
                    tbUserName.Text = tbUserName.Text.Remove(tbUserName.Text.IndexOf(" AS "));
                }
                else
                    cbMode.SelectedIndex = 0;
                data = data.Substring(data.IndexOf("/") + 1);
                PasswordText pt = new PasswordText();
                pt.EncryptedText = data.Remove(data.IndexOf("@"));
                tbPassword.Text = pt.Text;
                data = data.Substring(data.IndexOf("@") + 1);
                tbDatabase.Text = data;
                cbSavePassword.Checked = (pt.Length > 0);
            }
            else if (data.Contains("<?xml"))
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(data);
                tbUserName.Text = doc["LoginData"]["Username"].InnerText;                
                PasswordText pt = new PasswordText();
                pt.EncryptedText = doc["LoginData"]["Password"].InnerText;
                tbPassword.Text = pt.Text;
                tbDatabase.Text = doc["LoginData"]["Database"].InnerText;
                cbSavePassword.Checked = (pt.Length > 0);
                int homeIndex = cbHomes.Items.IndexOf(doc["LoginData"]["Home"].InnerText);
                if (homeIndex >= 0)
                    cbHomes.SelectedIndex = homeIndex;
                Direct = (Convert.ToBoolean(doc["LoginData"]["Direct"].InnerText));
                tbSID.Text = doc["LoginData"]["SID"].InnerText;
                if (doc["LoginData"]["Server"] != null)
                    tbADDR.Text = doc["LoginData"]["Server"].InnerText;
                udPort.Value = Convert.ToInt32(doc["LoginData"]["Port"].InnerText);
                cbMode.SelectedIndex = (cbMode.Items.IndexOf(doc["LoginData"]["Mode"].InnerText));                     
            }
            UpdateUI();
        }

        private void rbConnectDirect_CheckedChanged(object sender, EventArgs e)
        {
            UpdateUI();
        }

        private void UpdateUI()
        {
            //label8.Enabled = rbConnectDirect.Checked;
            //label7.Enabled = rbConnectDirect.Checked;
            //label6.Enabled = rbConnectDirect.Checked;
            //tbADDR.Enabled = rbConnectDirect.Checked;
            //tbSID.Enabled = rbConnectDirect.Checked;
            //udPort.Enabled = rbConnectDirect.Checked;
            
            //label3.Enabled = rbConnectOraClient.Checked;
            //label4.Enabled = rbConnectOraClient.Checked;
            //tbDatabase.Enabled = rbConnectOraClient.Checked;
            //cbHomes.Enabled = rbConnectOraClient.Checked;


        }

        private void rbConnectDirect_CheckedChanged_1(object sender, EventArgs e)
        {
            //if (rbConnectDirect.Checked)
            //    rbConnectOraClient.Checked = false;
            //UpdateUI();
        }

        private void rbConnectOraClient_CheckedChanged(object sender, EventArgs e)
        {
            //if (rbConnectOraClient.Checked)
            //    rbConnectDirect.Checked = false;
            //UpdateUI();
        }


    }
}
