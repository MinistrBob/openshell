﻿using System;
using System.IO;
using System.Text;
using OpenShellClasses.Utils;
#if ODP
using Oracle.DataAccess.Client;
#else
using Devart.Data.Oracle;
#endif
using System.Data;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

namespace OpenShellClasses
{

    // ====================================================================================
    
    //[ConnectionTypeAutoRegister]
    public class DbConnectionType : ConnectionType
    {
        private ctlDbConnectionLogin ctlLogin;

        public DbConnectionType()
        {
            Name = Strings.DbConnectionType_Name;
            Hint = Strings.DbConnectionType_Hint;
            Image = Resources.ResourceImages.database;
        }

        public override Connection CreateConnection(Control loginControl)
        {
            LoadOracleDataAccessAssembly();
            return new DbConnection(this, (loginControl as ctlDbConnectionLogin).ConnectionParams);
        }

        protected virtual void LoadOracleDataAccessAssembly()
        {
        }

        public override System.Windows.Forms.Control GetLoginControl()
        {
            if (ctlLogin == null)
                ctlLogin = new ctlDbConnectionLogin();
            return ctlLogin;
        }

        public override bool LoginControl_Validate(System.Windows.Forms.Control control)
        {
            return ((ctlDbConnectionLogin)control).ValidateData();
        }

        public override string LoginControl_GetData(System.Windows.Forms.Control control)
        {
            return ((ctlDbConnectionLogin)control).ConnectionParams.GetLoginData();
        }

        public override void LoginControl_SetData(System.Windows.Forms.Control control, string data)
        {
            ((ctlDbConnectionLogin)control).SetLoginData(data);
        }
    }

    // ====================================================================================

    public class DbConnection : Connection
    {
        private DbConnectionParams _ConnectionParameters;
        private OracleConnection _Conn;
        private long _DeptId;
        private long _UserId;

        public DbConnection(DbConnectionType Type, DbConnectionParams connectionParameters)
            : base(Type)
        {
            this._ConnectionParameters = connectionParameters;
            _Conn = new OracleConnection();
        }

        public override void Open(IConnectionContext Context)
        {
            if (_Conn.State != System.Data.ConnectionState.Open)
            {
                _Conn.ConnectionString = ConnectionParameters.ConnectionString;

                if (ConnectionParameters.Mode == "SYSDBA")
                    _Conn.ConnectMode = OracleConnectMode.SysDba;

                if (ConnectionParameters.Mode == "SYSOPER")
                    _Conn.ConnectMode = OracleConnectMode.SysOper;

                if (_ConnectionParameters.Direct)
                {
                    _Conn.Direct = true;
                    _Conn.Port = _ConnectionParameters.Port;
                    _Conn.Server = _ConnectionParameters.Server;
                    _Conn.Sid = _ConnectionParameters.SID;
                }

                _Conn.Open();
                AfterConnect();
            }
            else
                SetState(ConnectionState.Open);
        }

        private void AfterConnect()
        {
            try
            {
                _DeptId = Convert.ToInt64(QueryScalar(StringsSchema.Sql_Select_DeptId));
                _UserId = Convert.ToInt64(QueryScalar(StringsSchema.Sql_Select_UserId));
            }
            catch (Exception ex)
            {
                 MessageBox.Show(string.Format("Не удалось получить идентификатор подразделения и/или идентификатор текущего пользователя.\r\nТекст ошибки: {0}\r\nВы можете продолжать работы с базой данных, но часть функцией будет недоступна.\r\nПроверьте конфигурацию БД!",
                    ex.Message), "Ошибка получения параметров", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public override void Close(IConnectionContext Context)
        {
            if (_Conn.State != System.Data.ConnectionState.Closed)
            {
                _Conn.Close();
            }
        }

        public long UserId
        {
            get { return _UserId; }
        }

        public long DeptId
        {
            get { return _DeptId; }
        }

        public OracleConnection Conn
        {
            get { return _Conn; }
        }

        public DbConnectionParams ConnectionParameters
        {
            get { return _ConnectionParameters; }
        }

        #region Вспомогательные методы работы с базой данных
        public DataTable Query(string commandText, params OracleParameter [] parameters)
        {
            CheckConnected();
            OracleDataAdapter adapter = new OracleDataAdapter(commandText, Conn);
            foreach (OracleParameter par in parameters)
                adapter.SelectCommand.Parameters.Add(par);
            DataTable result = new DataTable();
            adapter.Fill(result);
            return result;
        }

        public object QueryScalar(string commandText)
        {
            CheckConnected();
            OracleCommand cmd = new OracleCommand(commandText, Conn);
            return cmd.ExecuteScalar();
        }

        public DataTable QueryTable(string tableName)
        {
            return Query(string.Format(StringsSchema.Sql_SelectAllFromTable, tableName));
        }

        public virtual void ExecuteNonQuery(string CommandText, params OracleParameter[] parameters)
        {
            CheckConnected();
            OracleCommand cmd = new OracleCommand(CommandText, Conn);
            foreach (OracleParameter par in parameters)
                cmd.Parameters.Add(par);
            cmd.ExecuteNonQuery();
        }

        public virtual Int64 SeqNextVal(string SequenceName)
        {
            CheckConnected();
            return Convert.ToInt64(QueryScalar(string.Format("select {0}.NextVal from Dual", SequenceName)));
        }

        private void CheckConnected()
        {
            //if (State != ConnectionState.Open)
              //  throw new ArgumentException(Strings.DbConnection_Error_ConnectionMustBeOpen);
        }

        #endregion


        protected override string GetName()
        {
            return string.Format(Strings.DbConnection_Name, 
                ConnectionParameters.UserName,
                (!ConnectionParameters.Direct) ? ConnectionParameters.Database : ConnectionParameters.SID,
                ConnectionParameters.DbaPrivilege(" AS {0}"));
        }

        protected override string GetConnectionPath()
        {
            if (!ConnectionParameters.Direct)
                return string.Empty;
            else
                return ConnectionParameters.Server;
            /*
            return string.Format("{0}@{1}", 
                ConnectionParameters.UserName.ToUpper(), 
                ConnectionParameters.Database.ToUpper());*/
        }

        public override string GetPropertyString(ConnectionPropertyStringKind Kind)
        {
            switch (Kind)
            {
                case ConnectionPropertyStringKind.User:
                    return ConnectionParameters.UserName.ToUpper();
                case ConnectionPropertyStringKind.Mode:
                    return ConnectionParameters.Mode;
                case ConnectionPropertyStringKind.Database:
                    if (!ConnectionParameters.Direct)
                        return ConnectionParameters.Database;
                    else
                        return ConnectionParameters.SID;
            }
            return base.GetPropertyString(Kind);
        }
    }

    // ====================================================================================

    /// <summary>
    /// Параметры подключения к базе данных
    /// </summary>
    [Serializable]
    public class DbConnectionParams
    {
        private string _UserName;
        private PasswordText _Password;
        private string _Database;
        private bool _SavePassword;
        private string _Mode;
        private string _Home;
        private bool _Direct;
        private int _Port;
        private string _SID;
        private string _Server;

        public DbConnectionParams()
        {
        }

        public DbConnectionParams(
            string UserName, string Password, string Database, 
            bool SavePassword, string Mode, string Home, bool Direct, int Port, string SID, string Server)
        {
            this._UserName = UserName;
            this._Password = new PasswordText();
            this._Password.Text = Password;
            this._Database = Database;
            this._SavePassword = SavePassword;
            this._Mode = Mode;
            this._Home = Home;
            this._Direct = Direct;
            this._Port = Port;
            this._SID = SID;
            this._Server = Server;
        }

        [XmlIgnore]
        public string UserName
        {
            get { return _UserName; }
        }

        [XmlIgnore]
        public PasswordText Password
        {
            get { return _Password; }
        }

        [XmlIgnore]
        public string Mode
        {
            get { return _Mode; }
        }

        [XmlIgnore]
        public string Database
        {
            get { return _Database; }
        }

        [XmlIgnore]
        public string Home
        {
            get
            {
                return _Home;
            }
        }

        [XmlIgnore]
        public bool Direct
        {
            get
            {
                return _Direct;
            }
        }

        [XmlIgnore]
        public int Port
        {
            get
            {
                return _Port;
            }
        }

        [XmlIgnore]
        public string SID
        {
            get
            {
                return _SID;
            }
        }

        [XmlIgnore]
        public string Server
        {
            get
            {
                return _Server;
            }
        }

        [XmlIgnore]
        public string ConnectionString
        {
            get
            {
                return string.Format(StringsSchema.OracleConnectionString, Database, UserName, Password.Text, Home);
            }
        }

        public string DbaPrivilege(string templateString)
        {
            if (_Mode.StartsWith("SYS"))
                return string.Format(templateString, _Mode);
            else
                return string.Empty;
        }

        public bool SavePassword
        {
            get { return _SavePassword; }
        }

        internal string GetLoginData()
        {
            string passwordText = string.Empty;
            if (SavePassword)
                passwordText = Password.EncryptedText;

            // version 1
            //return string.Format("{0}{3}/{1}@{2}", UserName, passwordText, Database,
            //    DbaPrivilege(" AS {0}"));

            // version 2
            using (MemoryStream ms = new MemoryStream())
            using (XmlTextWriter xtw = new XmlTextWriter(ms, Encoding.GetEncoding(1251)))
            {
                xtw.WriteStartDocument();
                xtw.WriteStartElement("LoginData");
                xtw.WriteElementString("Username", UserName);
                xtw.WriteElementString("Password", passwordText);
                xtw.WriteElementString("Database", Database);
                xtw.WriteElementString("Mode", Mode);
                xtw.WriteElementString("Home", Home);
                xtw.WriteElementString("Direct", Convert.ToString(Direct));
                xtw.WriteElementString("Port", Convert.ToString(Port));
                xtw.WriteElementString("SID", SID);
                 xtw.WriteElementString("Server", Server);
                xtw.WriteEndElement();
                xtw.Flush();
                //xtw.Close();
                ms.Seek(0, SeekOrigin.Begin);
                return Encoding.GetEncoding(1251).GetString(ms.ToArray());
            }
        }
    }

    // ====================================================================================

}
