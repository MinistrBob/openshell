﻿// =================================================================================================
//  DbRepositoryConnection.cs
//  Подключение к репозиторию в БД
// =================================================================================================

using System;
using System.Data;
using System.Windows.Forms;
#if ODP
using Oracle.DataAccess.Client;
#else
using Devart.Data.Oracle;
#endif
using OpenShellClasses.Utils;

namespace OpenShellClasses
{
    // =================================================================================================

    /// <summary>
    /// DbRepositoryConnection - подключение к репозиторию в БД
    /// </summary>
    public class DbRepositoryConnection : DbConnection, IRepository
    {        
        #region закрытые поля

        private RepositoryClassTypeList _ClassTypes;
        private RepositoryClassList _Classes;
        private RepositoryUserClassLinkList _ClassLinks;
        private ConnectionTypeList _RegisteredConnectionTypes;
        private IConnectionContext ConnectionContext;

        private DbRepositoryStorage _StorageShell;

        #endregion

        // -------------------------------------------------------------------------------------------

        #region конструктор

        public DbRepositoryConnection(DbConnectionType Type, DbConnectionParams connectionParameters) :
            base(Type, connectionParameters)
        {
            _ClassTypes = new RepositoryClassTypeList();
            _Classes = new RepositoryClassList();
            _ClassTypes = new RepositoryClassTypeList();
            _ClassLinks = new RepositoryUserClassLinkList();

            _RegisteredConnectionTypes = new ConnectionTypeList();
        }

        #endregion

        // -------------------------------------------------------------------------------------------

        #region методы

        public override void Open(IConnectionContext Context)
        {
            base.Open(Context);
        }

        // -------------------------------------------------------------------------------------------

        public override void Close(IConnectionContext Context)
        {
            //IConnectionTypeList list = Context.AppContext.GetService(typeof(IConnectionTypeList)) as IConnectionTypeList;
            //foreach (ConnectionType ct in _ConnectionTypes)
              //  list.RemoveConnectionType(ct);
            
            base.Close(Context);
        }

        // -------------------------------------------------------------------------------------------

        protected override string GetName()
        {
            return string.Format(Strings.DbRepositoryConnection_Name,
                ConnectionParameters.UserName, ConnectionParameters.Database);
        }

        // -------------------------------------------------------------------------------------------

        public RepositoryStorage StorageShell
        {
            get
            {
                if (_StorageShell == null)
                {
                    _StorageShell = new DbRepositoryStorage(this, StringsSchema.Repository_Scope_Shell, 
                        this.RepositoryUserConnection.Id);

                }
                return _StorageShell;
            }
        }

        // -------------------------------------------------------------------------------------------

        #region IRepository Members

        public void OpenRepository(IConnectionContext Context)
        {
            ConnectionContext = Context;

            // заполняется список типов классов
            fillClassTypes();

            // заполняется список доступных классов
            fillClasses();

            LoadUserConfiguration();
        }

        // -------------------------------------------------------------------------------------------

        private void WriteUserConnection(RepositoryUserConnection uc)
        {
            OracleCommand cmd = new OracleCommand(string.Empty, Conn);
#if ODP
            cmd.BindByName = true;
#else
            cmd.PassParametersByName = true;
#endif
            cmd.CommandText = StringsSchema.Sql_UsUserConnection_Select;
            cmd.Parameters.Add("emp_id", 
#if ODP
                OracleDbType.Int64
#else
                OracleDbType.Number
#endif
                ).Value = UserId;
            cmd.Parameters.Add("conn_class_id", 
#if ODP
               OracleDbType.Varchar2
#else
               OracleDbType.VarChar
#endif
).Value = 
                uc.ConnectionTypeClass.Code;
            cmd.Parameters.Add("login_path",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
).Value =
                uc.ConnectionPath;
            long id = -1;
            using (OracleDataReader reader = cmd.ExecuteReader())
            {
                if (reader.Read())
                    id = Convert.ToInt64(reader[0]);
            }

            if (id == -1)
            {
                id = SeqNextVal("US_SEQ");
                cmd.CommandText = StringsSchema.Sql_UsUserConnection_Insert;
                cmd.Parameters.Clear();
                cmd.Parameters.Add("id", 
#if ODP
                OracleDbType.Int64
#else
                OracleDbType.Number
#endif
).Value = id;
                cmd.Parameters.Add("emp_id", 
                    #if ODP
                OracleDbType.Int64
#else
                OracleDbType.Number
#endif
).Value = UserId;
                cmd.Parameters.Add("conn_class_id",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
).Value = uc.ConnectionTypeClass.Code;
                cmd.Parameters.Add("login_data",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
).Value = uc.LoginData;
                cmd.Parameters.Add("login_path",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
).Value = uc.ConnectionPath;
                cmd.Parameters.Add("time_created", OracleDbType.TimeStamp).Value = uc.TimeCreated;
                cmd.Parameters.Add("time_login", OracleDbType.TimeStamp).Value = uc.TimeLogin;
                cmd.Parameters.Add("name",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
).Value = uc.Name;
                cmd.ExecuteNonQuery();
            }
            else
            {
                cmd.CommandText = StringsSchema.Sql_UsUserConnection_Update;
                cmd.Parameters.Clear();
                cmd.Parameters.Add("login_data",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
).Value = uc.LoginData;
                cmd.Parameters.Add("login_path", 
#if ODP
               OracleDbType.Varchar2
#else
               OracleDbType.VarChar
#endif
).Value = uc.ConnectionPath;
                cmd.Parameters.Add("time_login", OracleDbType.TimeStamp).Value = uc.TimeLogin;
                cmd.Parameters.Add("name",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
).Value = uc.Name;
                cmd.Parameters.Add("id",
#if ODP
                OracleDbType.Int64
#else
 OracleDbType.Number
#endif
).Value = id;
                cmd.ExecuteNonQuery();
            }
            uc.Id = id;
        }

        // -------------------------------------------------------------------------------------------

        public void LoadUserConfiguration()
        {
            // заполняются ссылки на классы
            fillClassLinks(_ClassLinks);

            fillConnectionTypeClasses();

            fillModuleClasses();

            ConnectionContext.AppContext.LoadModules();
        }

        // -------------------------------------------------------------------------------------------

        private void fillModuleClasses()
        {
            ConnectionContext.AppContext.ModuleClasses.Clear();
            foreach (RepositoryUserClassLink l in
                _ClassLinks.GetEnabledLinks(_ClassTypes["MODULE"]))
                ConnectionContext.AppContext.ModuleClasses.Add(l.Class.ClassPath);
        }

        // -------------------------------------------------------------------------------------------

        private void fillConnectionTypeClasses()
        {
            UnregisterConnectionTypes();

            ClassPathLoader loader = ConnectionContext.AppContext.GetService(typeof(ClassPathLoader)) as ClassPathLoader;

            foreach (RepositoryUserClassLink link in
                _ClassLinks.GetEnabledLinks(_ClassTypes["CONNECTION_TYPE"]))
            {
                    ConnectionType ct = loader.CreateInstance(link.Class.ClassPath) as ConnectionType;
                    if (ct != null)
                    {
                        ct.ClassId = link.Class.Code;

                        if (ConnectionContext.AppContext.AddConnectionType(ct))
                            _RegisteredConnectionTypes.Add(ct);
                    }
            }
        }

        // -------------------------------------------------------------------------------------------

        private void UnregisterConnectionTypes()
        {
            while (_RegisteredConnectionTypes.Count > 0)
            {
                _RegisteredConnectionTypes.RemoveAt(0);
            }
        }

        // -------------------------------------------------------------------------------------------

        private void fillClassLinks(RepositoryUserClassLinkList list)
        {
            list.Clear();
            DataTable t = Query(StringsSchema.Sql_UsUserClasses_Links,
                new OracleParameter("emp_id", UserId));
            foreach (DataRow row in t.Rows)
            {
                RepositoryClass Class = _Classes.Find(Convert.ToInt32(row["CLASS_ID"]));
                System.Diagnostics.Debug.Assert(Class != null, 
                    "Внутренняя ошибка - не найден класс US_CLASSES в общей коллекции!");

                list.Add(
                    new RepositoryUserClassLink(
                        Class,
                        (row["CONNECTED"].ToString() == "+"),
                        Util.ConvertStrToBool(row["IS_NEW"].ToString())));
            }
        }

        // -------------------------------------------------------------------------------------------

        /// <summary>
        /// заполнение списка типов классов
        /// </summary>
        private void fillClassTypes()
        {
            _ClassTypes.Clear();
            DataTable t = QueryTable(StringsSchema.Db_Table_US_CLASSES_TYPE);
            foreach (DataRow row in t.Rows)
            {
                RepositoryClassType classType = new RepositoryClassType();
                classType.Code = row["CODE"].ToString();
                classType.Name = row["NAME"].ToString();
                classType.SubclassOf = row["SUBCLASS_OF"].ToString();
                _ClassTypes.Add(classType);
            }
        }

        // -------------------------------------------------------------------------------------------

        private void fillClasses()
        {
            _Classes.Clear();
            DataTable t = Query(StringsSchema.Sql_US_CLASSES);
            foreach (DataRow row in t.Rows)
            {
                RepositoryClass cls = new RepositoryClass();
                cls.Code = Convert.ToInt32(row["CODE"].ToString());
                cls.Name = row["NAME"].ToString();
                cls.ClassType = _ClassTypes[row["CLASS_TYPE"].ToString()];
                if (cls.ClassType == null)
                    throw new ArgumentException(string.Format("Не зарегистрирован тип класса ({0})!",
                        row["CLASS_TYPE"].ToString()));
                cls.ClassPath.AsString = row["CLASS_PATH"].ToString();
                cls.Enabled = Util.ConvertStrToBool(row["ENABLED"].ToString());
                cls.DefaultStatus = Util.ConvertDefaultStatusStr(row["DEFAULT_STATUS"].ToString());
                _Classes.Add(cls);
            }
        }

        // -------------------------------------------------------------------------------------------

        public void CloseRepository(IConnectionContext Context)
        {
            
        }

        #endregion

        #endregion

        // -------------------------------------------------------------------------------------------

        #region IRepository Members


        public RepositoryClassList Classes
        {
            get { return _Classes; }
        }

        public RepositoryUserClassLinkList UserClasses
        {
            get 
            {
                RepositoryUserClassLinkList result = new RepositoryUserClassLinkList();
                fillClassLinks(result);
                return result;
            }
        }

        #endregion

        #region IRepository Members


        public void UpdateUserClassLink(RepositoryClass Class, RepositoryUpdateUserClassLinkAction Action)
        {
            OracleCommand cmd = new OracleCommand(string.Empty, Conn);
#if ODP
            cmd.BindByName = true;
#else
            cmd.PassParametersByName = true;
#endif

            cmd.CommandText = StringsSchema.Sql_UsUSerClasses_Select;
            cmd.Parameters.Clear();
#if ODP
            cmd.Parameters.Add("emp_id", OracleDbType.Int32).Value = UserId;
            cmd.Parameters.Add("class_id", OracleDbType.Int32).Value = Class.Code;
#else
            cmd.Parameters.Add("emp_id", OracleDbType.Number).Value = UserId;
            cmd.Parameters.Add("class_id", OracleDbType.Number).Value = Class.Code;
#endif

            bool exists = false;
            Int64 id = 0;
            using (OracleDataReader reader = cmd.ExecuteReader())
            {
                exists = reader.Read();
                if (exists)
                    id = System.Convert.ToInt64(reader["ID"]);
            }

            if (exists)
            {
                if (Action == RepositoryUpdateUserClassLinkAction.Clear)
                {
                    cmd.CommandText = StringsSchema.Sql_UsUserClasses_Delete;
                    cmd.Parameters.Clear();
#if ODP
                    cmd.Parameters.Add("emp_id", OracleDbType.Int32).Value = UserId;
                    cmd.Parameters.Add("class_id", OracleDbType.Int32).Value = Class.Code;
#else
                    cmd.Parameters.Add("emp_id", OracleDbType.Number).Value = UserId;
                    cmd.Parameters.Add("class_id", OracleDbType.Number).Value = Class.Code;
#endif
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    cmd.CommandText = StringsSchema.Sql_UsUserClasses_Update;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("id",
#if ODP
                OracleDbType.Int64
#else
 OracleDbType.Number
#endif
).Value = id;
                    OracleParameter p = cmd.Parameters.Add("status",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
);
                    if (Action == RepositoryUpdateUserClassLinkAction.SetEnabled)
                        p.Value = "+";
                    else
                        p.Value = "-";
                    cmd.ExecuteNonQuery();
                }
            }
            else if (Action != RepositoryUpdateUserClassLinkAction.Clear)
            {
                cmd.CommandText = StringsSchema.Sql_UsUserClasses_Insert;
                cmd.Parameters.Clear();
#if ODP
                cmd.Parameters.Add("emp_id", OracleDbType.Int32).Value = UserId;
                cmd.Parameters.Add("class_id", OracleDbType.Int32).Value = Class.Code;
#else
                cmd.Parameters.Add("emp_id", OracleDbType.Number).Value = UserId;
                cmd.Parameters.Add("class_id", OracleDbType.Number).Value = Class.Code;
#endif
                OracleParameter p = cmd.Parameters.Add("status", 
#if ODP
                    OracleDbType.Varchar2
#else
                    OracleDbType.VarChar
#endif
                    );
                if (Action == RepositoryUpdateUserClassLinkAction.SetEnabled)
                    p.Value = "+";
                else
                    p.Value = "-";
                cmd.ExecuteNonQuery();
            }
        }

        #endregion

        #region IRepository Members

        public RepositoryUserConnectionList GetUserConnectionList()
        {
            RepositoryUserConnectionList result = new RepositoryUserConnectionList();
            OracleCommand cmd = new OracleCommand(StringsSchema.Sql_UsUserConnection_List, Conn);
#if ODP
            cmd.BindByName = true;
#else
            cmd.PassParametersByName = true;
#endif
            cmd.Parameters.Add("emp_id",
#if ODP
                OracleDbType.Int64
#else
 OracleDbType.Number
#endif
).Value = UserId;
            cmd.Parameters.Add("my_conn_class_id",
#if ODP
                OracleDbType.Int64
#else
 OracleDbType.Number
#endif
).Value = ConnectionContext.ConnectionClass.Code;
            using (OracleDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    RepositoryUserConnection uc = new RepositoryUserConnection();
                    uc.ConnectionPath = Convert.ToString(reader["LOGIN_PATH"]);
                    uc.ConnectionTypeClass = Classes.Find(Convert.ToInt32(reader["CONN_CLASS_ID"]));
                    uc.Id = Convert.ToInt64(reader["ID"]);
                    uc.LoginData = Convert.ToString(reader["LOGIN_DATA"]);
                    uc.Name = Convert.ToString(reader["NAME"]);
                    uc.TimeCreated = Convert.ToDateTime(reader["TIME_CREATED"]);
                    uc.TimeLogin = Convert.ToDateTime(reader["TIME_LOGIN"]);
                    result.Add(uc);
                }
            }
            return result;
        }

        #endregion

  
        #region IRepository Members

        public RepositoryUserConnection SaveConnection(IConnectionContext Context)
        {
            RepositoryUserConnection uc = new RepositoryUserConnection(Context);
            WriteUserConnection(uc);
            return uc;
        }

        #endregion

        #region IRepository Members


        public RepositoryStorage GetStorage(RepositoryUserConnection conn, string scope)
        {
            return new DbRepositoryStorage(this, scope, conn.Id);
        }

        #endregion

        #region Члены IRepository


        public void RemoveUserConnection(RepositoryUserConnection repositoryUserConnection)
        {
            //throw new NotImplementedException();
        }

        #endregion
    }

    // =================================================================================================

    [ConnectionTypeRepository]
    [ConnectionTypeAutoRegister]
    public class DbRepositoryConnectionType : DbConnectionType
    {
        public DbRepositoryConnectionType()
            : base()
        {
            Name = Strings.RepositoryDbConnectionType_Name;
            Hint = Strings.RepositoryDbConnectionType_Hint;
            Image = OpenShellClasses.Resources.ResourceImages.Control_DataConnector;
        }

        public override Connection CreateConnection(Control loginControl)
        {
            LoadOracleDataAccessAssembly();
            return new DbRepositoryConnection(this, (loginControl as ctlDbConnectionLogin).ConnectionParams);
        }
    }
    // =================================================================================================

    public class DbRepositoryStorage : RepositoryStorage
    {
        private DbConnection _Connection;
        private string _Scope;

        private OracleCommand cmdRead;
        private OracleCommand cmdInsert;
        private OracleCommand cmdUpdate;
        private long _ConnId;

        public DbRepositoryStorage(DbConnection Connection, string Scope, long ConnId)
        {
            this._Connection = Connection;
            this._Scope = Scope;
            this._ConnId = ConnId;

            InitializeOracleCommands();
        }

        private void InitializeOracleCommands()
        {
            cmdRead = new OracleCommand(StringsSchema.Sql_UsStorage_Read, _Connection.Conn);
#if ODP
            cmdRead.BindByName = true;
#else
            cmdRead.PassParametersByName = true;
#endif
            cmdRead.Parameters.Add("conn_id",
#if ODP
                OracleDbType.Int64
#else
 OracleDbType.Number
#endif
).Value = _ConnId;
            cmdRead.Parameters.Add("scope",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
).Value = Scope;
            cmdRead.Parameters.Add("name",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
);

            cmdInsert = new OracleCommand(StringsSchema.Sql_UsStorage_Insert, _Connection.Conn);
#if ODP
            cmdInsert.BindByName = true;
#else
            cmdInsert.PassParametersByName = true;
#endif
            cmdInsert.Parameters.Add("conn_id",
#if ODP
                OracleDbType.Int64
#else
 OracleDbType.Number
#endif
).Value = _ConnId;
            cmdInsert.Parameters.Add("scope",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
).Value = Scope;
            cmdInsert.Parameters.Add("name",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
);
            cmdInsert.Parameters.Add("value", OracleDbType.Clob);

            cmdUpdate = new OracleCommand(StringsSchema.Sql_UsStorage_Update, _Connection.Conn);
#if ODP
            cmdUpdate.BindByName = true;
#else
            cmdUpdate.PassParametersByName = true;
#endif

            cmdUpdate.Parameters.Add("value", OracleDbType.Clob);
            cmdUpdate.Parameters.Add("conn_id",
#if ODP
                OracleDbType.Int64
#else
 OracleDbType.Number
#endif
).Value = _ConnId;
            cmdUpdate.Parameters.Add("scope",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
).Value = Scope;
            cmdUpdate.Parameters.Add("name",
#if ODP
               OracleDbType.Varchar2
#else
 OracleDbType.VarChar
#endif
);
        }

        public string Scope
        {
            get { return _Scope; }
        }

        #region IRepositoryStorage Members

        public override object ReadValue(string Path)
        {
            cmdRead.Parameters["name"].Value = Path;
            using (OracleDataReader reader = cmdRead.ExecuteReader())
            {
                if (reader.Read())
                {
                    object result = reader[0];
                    if (reader.Read())
                        throw new ArgumentException(Strings.Exception_DbStorage_DupKey);
                    else
                        return result;
                }
                else
                    return null;
            }
        }

        public override bool ValueExists(string Path)
        {
            return (ReadValue(Path) != null);
        }

        public override void WriteValue(string Path, object Value)
        {
            OracleCommand cmd = cmdInsert;
            if (ValueExists(Path))
                cmd = cmdUpdate;
            cmd.Parameters["name"].Value = Path.ToUpper();
            cmd.Parameters["value"].Value = Value;
            cmd.ExecuteNonQuery();
            }

        #endregion
    }

    // =================================================================================================
}
