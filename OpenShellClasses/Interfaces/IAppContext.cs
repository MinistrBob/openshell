﻿// =================================================================================================
//  IAppContext.cs
//  Интерфейс контекста выполнения приложения 
// =================================================================================================

using System;
using System.Windows.Forms;

namespace OpenShellClasses
{
    // ===============================================================================================

    /// <summary>
    /// контекст уровня приложения Универсальная оболочка
    /// </summary>
    public interface IAppContext : IContext
    {
        /// <summary>
        /// универсальный обработчик событий приложения
        /// </summary>
        //event AppEvent AppEvent;

        /// <summary>
        /// текущий репозиторий
        /// </summary>
        IRepository Repository
        {
            get;
        }

        /// <summary>
        /// подключен ли репозиторий
        /// </summary>
        bool RepositoryConnected
        {
            get;
        }

        /// <summary>
        /// обновление универсальной оболочки
        /// </summary>
        void Update();

        bool AddConnectionType(ConnectionType connectionType);

        void RemoveConnectionType(ConnectionType connectionType);

        ClassPathList ModuleClasses
        {
            get;
        }

        void LoadModules();


        Form MainForm
        {
            get;
        }

        event ConnectionEventHandler ConnectionOpened;
        event ConnectionEventHandler ConnectionClosed;

        string LocalUserDirectory
        {
            get;
        }

        void CloseConnection(IConnection Connection);

    }

    // ===============================================================================================

    /// <summary>
    /// интерфейс для управления списком типов соединений
    /// </summary>
    public interface IConnectionTypeList
    {
        /// <summary>
        /// добавить доступный тип соединения
        /// </summary>
        /// <param name="ConnectionType">тип соединения</param>
        void AddConnectionType(ConnectionType type);

        /// <summary>
        /// удалить доступный тип соединения
        /// </summary>
        /// <param name="type"></param>
        void RemoveConnectionType(ConnectionType type);
    }

    // ===============================================================================================

    public enum AppEventBaseType
    {
        RepositoryConnecting        = 0,
        RepositoryConnected         = 1,
        RepositoryDisconnecting     = 2,
        RepositoryDisconnected      = 3,
        ConnectionTypeAdded         = 4,
        ConnectionTypeRemoved       = 5,
        ConnectionOpening           = 6,
        CustomEvent = 99999
    }
        
    // ===============================================================================================

    public class AppEventArgs : EventArgs
    {
        private AppEventBaseType _Type;
        private object _Sender;

        public AppEventArgs(AppEventBaseType Type, object Sender)
        {
            _Type = Type;
            _Sender = Sender;
        }

        public object Sender
        {
            get { return _Sender; }
        }

        public AppEventBaseType Type
        {
            get { return _Type; }
        }
    }

    // ===============================================================================================

    public delegate void AppEvent (IAppContext Host, AppEventArgs e);
}
