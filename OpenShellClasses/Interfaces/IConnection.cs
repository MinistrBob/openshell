﻿using System;
using System.Collections.Generic;
using System.Text;

namespace US.Classes
{
    public interface IConnection
    {
        void Connect(IHost Host);
        void Disconnect(IHost Host);
        string Name { get; }
    }
}
