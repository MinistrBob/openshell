﻿
namespace OpenShellClasses
{
    public interface IConnectionContext : IContext
    {
        IAppContext AppContext
        {
            get;
        }
        
        Connection Connection
        {
            get;
        }

        RepositoryClass ConnectionClass
        {
            get;
        }

        RepositoryStorage GetStorage(string Scope);

        /*ActionsController ActionsNavigator
        {
            get;
        }

        ActionsController ActionsMainMenu
        {
            get;
        }

        ActionsController ActionsMainMenuWorkMenu
        {
            get;
        }

        ActionsController ActionsMainToolbar
        {
            get;
        }*/

        void Update();

        void AddModule(IModule module);

        string SelectedActionId
        {
            get;
            set;
        }
        

        Page CurrentPage
        {
            get;
            set;
        }

        /*Page ConnectionPage
        {
            get;
            set;
        }*/

    }
}
