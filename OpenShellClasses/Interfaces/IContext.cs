﻿using System;

namespace OpenShellClasses
{
    /// <summary>
    /// базовый интерфейс контекстов вызова
    /// </summary>
    public interface IContext : IServiceProvider
    {
    }
}
