﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;

namespace US.Classes
{
    // ===============================================================================================

    /// <summary>
    /// интерфейс программы управляющей оболочки (хоста)
    /// </summary>
    public interface IHost
    {
        event HostEvent HostEvent;

        IRepository CurrentRepository
        {
            get;
            set;
        }

        ConnectionList Connections
        {
            get;
        }

        void Update();
        
    }

    // ===============================================================================================

    public enum HostEventBaseType
    {
        RepositoryConnecting        = 0,
        RepositoryConnected         = 1,
        RepositoryDisconnecting     = 2,
        RepositoryDisconnected      = 3,
        ConnectionTypeAdded         = 4,
        ConnectionTypeRemoved       = 5,
        ConnectionOpening           = 6,
        CustomEvent = 99999
    }
        
    // ===============================================================================================

    public class HostEventArgs : EventArgs
    {
        private HostEventBaseType _Type;
        private object _Sender;

        public HostEventArgs(HostEventBaseType Type, object Sender)
        {
            _Type = Type;
            _Sender = Sender;
        }

        public object Sender
        {
            get { return _Sender; }
        }

        public HostEventBaseType Type
        {
            get { return _Type; }
        }
    }

    // ===============================================================================================

    public delegate void HostEvent (IHost Host, HostEventArgs e);
}
