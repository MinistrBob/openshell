﻿using System;
using System.Collections.Generic;
using System.Text;

namespace US.Classes
{
    // ================================================================================================

    #region Интерфейс IModule: Подключаемый к оболочке модуль

    /// <summary>
    /// Подключаемый к оболочке модуль
    /// </summary>
    public interface IModule
    {
        /// <summary>
        /// основной метод загрузки модуля
        /// </summary>
        /// <param name="Context">контекст управляющей оболочки</param>
        void Load();

        /// <summary>
        /// основной метод выгрузки модуля
        /// </summary>
        /// <param name="Context">контекст управляющей оболочки</param>
        void Unload();
    }

    #endregion

    // ================================================================================================}
}
