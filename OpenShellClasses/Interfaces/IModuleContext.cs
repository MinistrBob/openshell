﻿
namespace OpenShellClasses
{
    public interface IModuleContext : IContext
    {
        RepositoryClass ModuleClass
        {
            get;
        }

        IAppContext AppContext
        {
            get;
        }

        IConnectionContext ConnectionContext
        {
            get;
        }

        IModule Module
        {
            get;
        }

        void AddAction(Action action);

        void Update();
    }

    public enum ActionPlacement
    {
        Navigator,
        MainToolBar,
        MainMenu
    }
}
