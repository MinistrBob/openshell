﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace OpenShellClasses
{
    public class LocalRepositoryConfiguration
    {
        private LocalRepositoryConnection _Repository;
        private LocalRepositoryPrivateConfiguration _Private;
        private LocalRepositorySharedConfiguration _Shared;

        public LocalRepositoryConfiguration(LocalRepositoryConnection Repository)
        {
            this._Repository = Repository;
            _Private = new LocalRepositoryPrivateConfiguration(this);
            _Shared = new LocalRepositorySharedConfiguration(this);
            
        }



        public LocalRepositoryPrivateConfiguration Private
        {
            get { return _Private; }
        }

        public LocalRepositorySharedConfiguration Shared
        {
            get { return _Shared; }
        }

        internal void Load()
        {
            Shared.Load();
            Private.Load(Path.Combine(_Repository.ConnectionContext.AppContext.LocalUserDirectory,
                OpenShellClasses.Utils.StringsSchema.PrivateConfigurationFile));
        }

        internal void Save()
        {
            Private.Save(Path.Combine(_Repository.ConnectionContext.AppContext.LocalUserDirectory,
                OpenShellClasses.Utils.StringsSchema.PrivateConfigurationFile));
        }
    }

    public class LocalRepositoryConfigurationBaseComponent
    {
        private LocalRepositoryConfiguration _Configuration;

        protected LocalRepositoryConfigurationBaseComponent(LocalRepositoryConfiguration Configuration)
        {
            this._Configuration = Configuration;
        }

        public LocalRepositoryConfiguration Configuration
        {
            get { return _Configuration; }
        }
    }

    public class LocalRepositorySharedConfiguration : LocalRepositoryConfigurationBaseComponent
    {
        private LocalRepositorySharedConfigurationData _Data;

        public LocalRepositorySharedConfiguration(LocalRepositoryConfiguration Configuration) : base(Configuration)
        {

        }

        internal RepositoryClassList GetClassList()
        {
            return _Data.ClassList;
        }

        public RepositoryClassList ClassList
        {
            get { return _Data.ClassList; }
        }

        public RepositoryClassTypeList ClassTypeList
        {
            get { return _Data.ClassTypeList; }
        }

        internal void Load()
        {
            _Data = LocalRepositorySharedConfigurationData.Load(
                Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OpenShellClasses.Properties.Settings.Default.SharedConfigurationFileName));
        }
    }

    public class LocalRepositoryPrivateConfiguration : LocalRepositoryConfigurationBaseComponent
    {
        private LocalRepositoryPrivateConfigurationData _Data;

        public LocalRepositoryPrivateConfiguration(LocalRepositoryConfiguration Configuration)
            : base(Configuration)
        {

        }

        internal RepositoryUserClassLinkList GetUserClassList()
        {
            return _Data.UserClassLinks;
        }

        internal void UpdateUserClassLink(RepositoryClass Class, RepositoryUpdateUserClassLinkAction Action)
        {
            if (Action == RepositoryUpdateUserClassLinkAction.Clear)
            {
                RepositoryUserClassLink l;
                while ((l = _Data.UserClassLinks.Find(Class.Code)) != null)
                    _Data.UserClassLinks.Remove(l);
            }
            else
            {
                RepositoryUserClassLink l = _Data.UserClassLinks.Find(Class.Code);
                if (l == null)
                {
                    l = new RepositoryUserClassLink(Class, (Action == RepositoryUpdateUserClassLinkAction.SetEnabled), false);
                    _Data.UserClassLinks.Add(l);
                }
                l.IsConnected = (Action == RepositoryUpdateUserClassLinkAction.SetEnabled);
            }
            Configuration.Save();
        }

        internal RepositoryUserConnectionList GetUserConnectionList()
        {
            return _Data.Connections;
        }

        public LocalRepositoryPrivateConfigurationData Data
        {
            get { return _Data; }
        }

        internal void Load(string fileName)
        {
            _Data = LocalRepositoryPrivateConfigurationData.Load(fileName);
            AfterLoad();
        }

        private void AfterLoad()
        {
            List<RepositoryUserConnection> del_conn_list = new List<RepositoryUserConnection>();
            foreach (RepositoryUserConnection conn in _Data.Connections)
            {
                conn.ConnectionTypeClass = Configuration.Shared.ClassList.Find(conn.ConnectionTypeClassCode);
                if (conn.ConnectionTypeClass == null)
                    del_conn_list.Add(conn);
            }

            foreach (RepositoryUserConnection del_conn in del_conn_list)
                _Data.Connections.Remove(del_conn);

            List<RepositoryUserClassLink> del_link_list = new List<RepositoryUserClassLink>();
            foreach (RepositoryUserClassLink link in _Data.UserClassLinks)
            {
                link.Class = Configuration.Shared.ClassList.Find(link.ClassId);
                if (link.Class == null)
                    del_link_list.Add(link);
            }
            foreach (RepositoryUserClassLink del_link in del_link_list)
                _Data.UserClassLinks.Remove(del_link);
        }

        internal void Save(string fileName)
        {
            _Data.Save(fileName);
        }
    }

    [Serializable]
    [XmlType(TypeName="SharedConfiguration")]
    public class LocalRepositorySharedConfigurationData
    {
        private RepositoryClassTypeList _ClassTypeList;
        private RepositoryClassList _ClassList;

        public static LocalRepositorySharedConfigurationData Load(string FileName)
        {
            if (File.Exists(FileName))
            {
                try
                {
                    using (StreamReader sr  = new StreamReader(FileName, Const.DefaultEncoding))
                    {
                        XmlSerializer xs = new XmlSerializer(typeof(LocalRepositorySharedConfigurationData));
                        LocalRepositorySharedConfigurationData result = xs.Deserialize(sr) as LocalRepositorySharedConfigurationData;
                        if (result == null)
                            throw new ArgumentException("Shared config deserialization returns null!");
                        result.AfterLoad();
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(
                        string.Format(OpenShellClasses.Utils.Strings.Error_SharedConfigFileLoadError,
                        FileName, ex.Message), ex);
                }
            }
            else
                throw new ArgumentException(string.Format(OpenShellClasses.Utils.Strings.Error_SharedConfigFileNotFound, FileName));
        }

        private void AfterLoad()
        {
            if (ClassTypeList.Count == 0)
                throw new ArgumentException("Ошибка загрузки общей конфигурации: не заданы типы классов!");
            _CheckClassType("CONNECTION_TYPE");
            _CheckClassType("MODULE");

            foreach (RepositoryClass cls in ClassList)
            {
                _CheckClassType(cls.ClassTypeCode);
                cls.ClassType = _ClassTypeList[cls.ClassTypeCode];
            }
        }

        private void _CheckClassType(string classTypeName)
        {
            if (!ClassTypeList.Contains(classTypeName))
            {
                throw new ArgumentException(
                    string.Format("Не найдено объявление необходимого типа класса ({0})!", classTypeName));
            }
        }

        public RepositoryClassList ClassList
        {
            get { return _ClassList; }
            set { _ClassList = value; }
        }

        public RepositoryClassTypeList ClassTypeList
        {
            get { return _ClassTypeList; }
            set { _ClassTypeList = value; }
        }
    }

    [Serializable]
    public class LocalRepositoryPrivateConfigurationData
    {
        private long _LastConnectionId;
        private RepositoryUserClassLinkList _UserClassLinks;
        private RepositoryUserConnectionList _Connections;

        public LocalRepositoryPrivateConfigurationData()
        {
            _Connections = new RepositoryUserConnectionList();
            _UserClassLinks = new RepositoryUserClassLinkList();
        }

        public static LocalRepositoryPrivateConfigurationData Load(string FileName)
        {
            if (File.Exists(FileName))
            {
                try
                {
                    using (StreamReader sr = new StreamReader(FileName, Const.DefaultEncoding))
                    {
                        XmlSerializer xs = new XmlSerializer(typeof(LocalRepositoryPrivateConfigurationData));
                        LocalRepositoryPrivateConfigurationData result = xs.Deserialize(sr) as LocalRepositoryPrivateConfigurationData;
                        if (result == null)
                            throw new ArgumentException("Private config deserialization returns null!");
                        return result;
                    }
                }
                catch //(Exception ex)
                {
                    //throw new InvalidOperationException(
                    //    string.Format(OpenShellClasses.Utils.Strings.Error_SharedConfigFileLoadError,
                    //    FileName, ex.Message), ex);
                }
            }
            return new LocalRepositoryPrivateConfigurationData();
        }

        public RepositoryUserClassLinkList UserClassLinks
        {
            get { return _UserClassLinks; }
            set { _UserClassLinks = value; }
        }

        public RepositoryUserConnectionList Connections
        {
            get { return _Connections; }
            set { _Connections = value; }
        }

        public long LastConnectionId
        {
            get { return _LastConnectionId; }
            set { _LastConnectionId = value; }
        }

        internal void Save(string fileName)
        {
            using (StreamWriter sw = new StreamWriter(fileName, false, Const.DefaultEncoding))
            {
                XmlSerializer xs = new XmlSerializer(GetType());
                xs.Serialize(sw, this);
            }

        }
    }
}
