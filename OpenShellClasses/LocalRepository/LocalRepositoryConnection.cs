﻿using System;
using OpenShellClasses.Utils;

namespace OpenShellClasses
{
    public class LocalRepositoryConnection : Connection, IRepository
    {
        private LocalRepositoryConfiguration _Configuration;
        private LocalRepositoryStorage _StorageShell;
        private ConnectionTypeList _RegisteredConnectionTypes;
        internal IConnectionContext ConnectionContext;
        private RepositoryUserClassLinkList _ClassLinks;

        public LocalRepositoryConnection() : base(new LocalRepositoryConnectionType())
        {
            _Configuration = new LocalRepositoryConfiguration(this);
            _RegisteredConnectionTypes = new ConnectionTypeList();
            _ClassLinks = new RepositoryUserClassLinkList();
        }

        public LocalRepositoryConfiguration Configuration
        {
            get { return _Configuration; }
        }

        public override void Open(IConnectionContext Context)
        {
        }

        public override void Close(IConnectionContext Context)
        {
        }

        protected override string GetName()
        {
            return "(локальный репозиторий)";
        }

        protected override string GetConnectionPath()
        {
            return "LOCAL_REPOSITORY";
        }

        private void fillConnectionTypeClasses()
        {
            ClassPathLoader loader = ConnectionContext.AppContext.GetService(typeof(ClassPathLoader)) as ClassPathLoader;
            ConnectionTypeList new_list = new ConnectionTypeList();

            foreach (RepositoryUserClassLink link in _ClassLinks.
                GetEnabledLinks(Configuration.Shared.ClassTypeList["CONNECTION_TYPE"]))
            {
                Type connTypeType = loader.LoadType(link.Class.ClassPath);
                ConnectionType ct = null;

                // ищем зарегистрированный тип подключения
                ct = _RegisteredConnectionTypes.Find(
                    delegate(ConnectionType connType)
                    {
                        return (connType.GetType() == connTypeType);
                    });

                if (ct == null)
                    ct = loader.CreateInstance(link.Class.ClassPath) as ConnectionType;

                if (ct != null)
                {
                    ct.ClassId = link.Class.Code;
                    new_list.Add(ct);
                }
            }

            ConnectionTypeList unregister_list = new ConnectionTypeList(_RegisteredConnectionTypes.ToArray());

            foreach (ConnectionType ct in new_list)
            {
                if (!_RegisteredConnectionTypes.Contains(ct))
                {
                    if (ConnectionContext.AppContext.AddConnectionType(ct))
                        _RegisteredConnectionTypes.Add(ct);
                }
                else
                    unregister_list.Remove(ct);
            }

            foreach (ConnectionType ct in unregister_list)
            {
                ConnectionContext.AppContext.RemoveConnectionType(ct);
                _RegisteredConnectionTypes.Remove(ct);
            }                
        }

        // -------------------------------------------------------------------------------------------

        private void fillModuleClasses()
        {
            ConnectionContext.AppContext.ModuleClasses.Clear();
            foreach (RepositoryUserClassLink l in
                _ClassLinks.GetEnabledLinks(Configuration.Shared.ClassTypeList["MODULE"]))
                ConnectionContext.AppContext.ModuleClasses.Add(l.Class.ClassPath);
        }

        // -------------------------------------------------------------------------------------------


        #region IRepository Members

        public void OpenRepository(IConnectionContext Context)
        {
            ConnectionContext = Context;
            LoadUserConfiguration();
        }

        private void fillClassLinks()
        {
            _ClassLinks.Clear();
            _ClassLinks.AddRange(Configuration.Private.GetUserClassList().ToArray());
            foreach (RepositoryClass cls in Configuration.Shared.ClassList)
                if (_ClassLinks.Find(cls.Code) == null)
                {
                    RepositoryUserClassLink link = new RepositoryUserClassLink(cls,
                        (cls.DefaultStatus != RepositoryClassDefaultStatus.Disabled),
                        false);
                    _ClassLinks.Add(link);
                }
            _ClassLinks.SortById();
        }

        public void CloseRepository(IConnectionContext Context)
        {
            Configuration.Save();
        }

        public RepositoryStorage StorageShell
        {
            get 
            {
                if (_StorageShell == null)
                    _StorageShell = new LocalRepositoryStorage(this, null,
                        StringsSchema.Repository_Scope_Shell);
                return _StorageShell;
            }
        }

        public RepositoryClassList Classes
        {
            get 
            {
                return Configuration.Shared.GetClassList();
            }
        }

        public RepositoryUserClassLinkList UserClasses
        {
            get 
            {
                return Configuration.Private.GetUserClassList();
            }
        }

        public void UpdateUserClassLink(RepositoryClass Class, RepositoryUpdateUserClassLinkAction Action)
        {
            Configuration.Private.UpdateUserClassLink(Class, Action);
        }

        public RepositoryUserConnectionList GetUserConnectionList()
        {
            return Configuration.Private.GetUserConnectionList();
        }

        public void LoadUserConfiguration()
        {
            Configuration.Load();
            fillClassLinks();
            fillConnectionTypeClasses();
            fillModuleClasses();
            ConnectionContext.AppContext.LoadModules();            
        }

        public RepositoryUserConnection SaveConnection(IConnectionContext connection)
        {
            RepositoryUserConnectionList user_connections = GetUserConnectionList();
            RepositoryUserConnection result = new RepositoryUserConnection(connection);
            bool exists = false;

            foreach (RepositoryUserConnection uc in user_connections)
                if (string.Compare(uc.LoginData, result.LoginData, true) == 0 && uc.ConnectionTypeClassCode == result.ConnectionTypeClassCode)
                {
                    uc.UpdateProps(result);
                    result = uc;
                    exists = true;
                    break;
                }

            if (!exists)
            {
                result.Id = Configuration.Private.Data.LastConnectionId + 1;
                user_connections.Add(result);
                Configuration.Private.Data.LastConnectionId = result.Id;
            }

            user_connections.SortByTimeLoginDesc();

            Configuration.Save();
            return result;
   
        }

        public RepositoryStorage GetStorage(RepositoryUserConnection conn, string scope)
        {
            return new LocalRepositoryStorage(this, conn, scope);
        }

        public void RemoveUserConnection(RepositoryUserConnection repositoryUserConnection)
        {
            Configuration.Private.GetUserConnectionList().Remove(repositoryUserConnection);
            Configuration.Save();
        }

        #endregion
    }

    [ConnectionTypeRepository]
    public class LocalRepositoryConnectionType : ConnectionType
    {
        public override Connection CreateConnection(System.Windows.Forms.Control loginControl)
        {
            return new LocalRepositoryConnection();
        }

        public override System.Windows.Forms.Control GetLoginControl()
        {
            return null;
        }

        public override bool LoginControl_Validate(System.Windows.Forms.Control control)
        {
            return true;
        }

        public override string LoginControl_GetData(System.Windows.Forms.Control control)
        {
            return null;
        }

        public override void LoginControl_SetData(System.Windows.Forms.Control control, string data)
        {
        }
    }
}
