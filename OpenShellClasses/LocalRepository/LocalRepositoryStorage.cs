﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using OpenShellClasses.Utils;

namespace OpenShellClasses
{
    public class LocalRepositoryStorage : RepositoryStorage
    {
        private LocalRepositoryConnection _Repository;
        private RepositoryUserConnection _Connection;
        private string _Scope;
        private string _StorageDirectory;
        private string _StorageFileName;

        public LocalRepositoryStorage(LocalRepositoryConnection Repository, RepositoryUserConnection Connection, string Scope)
        {
            _Repository = Repository;
            _Scope = Scope;
            _Connection = Connection;
            _StorageDirectory= 
                Path.Combine(Repository.ConnectionContext.AppContext.LocalUserDirectory,
                    StringsSchema.StorageDirectory);
            if (!Directory.Exists(_StorageDirectory))
                Directory.CreateDirectory(_StorageDirectory);

            string fileName;

            if (Connection == null)
            {
                fileName = string.Format("UserProfile.xml", Scope);
            }
            else
            {
                fileName = string.Format("{0:000000}-UserModProp-{1}.xml", Connection.Id, Scope);
            }

            _StorageFileName = Path.Combine(_StorageDirectory, fileName);
        }

        public LocalRepositoryConnection Repository
        {
            get { return _Repository; }
        }

        public string Scope
        {
            get { return _Scope; }
        }

        public override object ReadValue(string Path)
        {
            LocalRepositoryStorageData data = new LocalRepositoryStorageData(_StorageFileName);
            if (data.ContainsKey(Path))
                return data.GetValue(Path);
            else
                return null;
        }

        public override bool ValueExists(string Path)
        {
            LocalRepositoryStorageData data = new LocalRepositoryStorageData(_StorageFileName);
            return (data.ContainsKey(Path));
        }

        public override void WriteValue(string Path, object Value)
        {
            LocalRepositoryStorageData data = new LocalRepositoryStorageData(_StorageFileName);
            data.SetValue(Path, Value);
            data.Save(_StorageFileName);
        }
    }
    
    [Serializable]
    public class LocalRepositoryStorageDataItem
    {
        [XmlAttribute]
        public string Path;
        public object Data;
    }

    public class LocalRepositoryStorageDataItemList : List<LocalRepositoryStorageDataItem>
    {
    }

    [Serializable]
    public class LocalRepositoryStorageData 
    {
        private LocalRepositoryStorageDataItemList _List;
        private Dictionary<string, LocalRepositoryStorageDataItem> _Dic;

        public LocalRepositoryStorageData()
        {
            _List = new LocalRepositoryStorageDataItemList();
            FillDic();
        }

        public LocalRepositoryStorageData(string FileName) : this()
        {
            LoadFromFile(FileName);
        }

        private void FillDic()
        {
            _Dic = new Dictionary<string, LocalRepositoryStorageDataItem>();
            foreach (LocalRepositoryStorageDataItem item in _List)
                _Dic.Add(item.Path, item);
        }

        public virtual void LoadFromFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    using (StreamReader sr = new StreamReader(fileName, Const.DefaultEncoding))
                    {
                        XmlSerializer xs = new XmlSerializer(typeof(LocalRepositoryStorageDataItemList));
                        _List = xs.Deserialize(sr) as LocalRepositoryStorageDataItemList;
                        FillDic();
                    }
                }
                catch
                {
                }
            }
        }

        internal void Save(string FileName)
        {
            using (StreamWriter sw = new StreamWriter(FileName, false, Const.DefaultEncoding))
            {
                XmlSerializer xs = new XmlSerializer(typeof(LocalRepositoryStorageDataItemList));
                xs.Serialize(sw, _List);
            }
        }

        internal bool ContainsKey(string Path)
        {
            return _Dic.ContainsKey(Path);
        }

        internal void SetValue(string Path, object Value)
        {
            if (!ContainsKey(Path))
            {
                LocalRepositoryStorageDataItem item = new LocalRepositoryStorageDataItem();
                item.Path = Path;
                item.Data = Value;
                _List.Add(item);
                _Dic.Add(Path, item);
            }

            _Dic[Path].Data = Value;
        }

        internal object GetValue(string Path)
        {
            return (_Dic[Path] != null) ? _Dic[Path].Data : null;
        }
    }
}
