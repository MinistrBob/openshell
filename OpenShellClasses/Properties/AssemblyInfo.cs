﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("OpenShellClasses")]
[assembly: AssemblyDescription("OpenShellClasses")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ООО Октопус")]
[assembly: AssemblyProduct("OpenShellClasses")]
[assembly: AssemblyCopyright("Copyright © ООО Октопус")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("1DF78DBB-7DD4-4D64-9996-444F037C5A88")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.1.1")]
[assembly: AssemblyFileVersion("1.1.1.1")]
