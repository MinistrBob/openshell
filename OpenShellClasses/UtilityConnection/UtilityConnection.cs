﻿
namespace OpenShellClasses
{
    public class UtilityConnection : Connection
    {
        public UtilityConnection(UtilityConnectionType Connection) : base(Connection)
        {

        }

        public override void Open(IConnectionContext Context)
        {
        }

        public override void Close(IConnectionContext Context)
        {
        }

        protected override string GetName()
        {
            return "Утилиты";
        }

        protected override string GetConnectionPath()
        {
            return string.Empty;
        }
    }
}
