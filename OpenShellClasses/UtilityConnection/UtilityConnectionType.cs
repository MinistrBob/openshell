﻿using OpenShellClasses.Utils;

namespace OpenShellClasses
{
    public class UtilityConnectionType : ConnectionType
    {
        public UtilityConnectionType()
        {
            this.Name = Strings.UtilityConnectionType_Name;
            this.Hint = Strings.UtilityConnectionType_Hint;
            this.Image = Resources.ResourceImages.db_sql_procedure16;
        }

        public override Connection CreateConnection(System.Windows.Forms.Control loginControl)
        {
            return new UtilityConnection(this);
        }

        public override System.Windows.Forms.Control GetLoginControl()
        {
            return null;
        }

        public override bool LoginControl_Validate(System.Windows.Forms.Control control)
        {
            return true;
        }

        public override string LoginControl_GetData(System.Windows.Forms.Control control)
        {
            return string.Empty;
        }

        public override void LoginControl_SetData(System.Windows.Forms.Control control, string data)
        {
        }
    }
}
