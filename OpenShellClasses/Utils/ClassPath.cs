﻿// =================================================================================================
//  ClassPath.cs
//  Классы для загрузки типов, находящихся в других сборках
// =================================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using OpenShellClasses.Utils;

namespace OpenShellClasses
{
    // =================================================================================================

    /// <summary>
    /// указатель на класс в виде строки
    /// </summary>
    public class ClassPath
    {
        // ------------------------------------------------------------------------------------------------------------

        #region закрытые поля

        private string _AssemblyName = string.Empty;
        private string _TypeName = string.Empty;
        private RepositoryClass _ClassRef;

        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region конструктор

        public ClassPath()
        {
        }

        public ClassPath(string ClassPathString) : this()
        {
            AsString = ClassPathString;
        }

        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region свойства

        [XmlIgnore]
        public string AssemblyName
        {
            get { return _AssemblyName; }
        }

        // ------------------------------------------------------------------------------------------------------------

        [XmlIgnore]
        public string TypeName
        {
            get { return _TypeName; }
        }

        // ------------------------------------------------------------------------------------------------------------

        [XmlAttribute(AttributeName="AsString")]
        public string AsString
        {
            get
            {
                if (!string.IsNullOrEmpty(TypeName))
                {
                    StringBuilder sb = new StringBuilder();
                    if (!string.IsNullOrEmpty(_AssemblyName))
                    {
                        sb.Append(AssemblyName);
                        sb.Append(", ");
                    }
                    sb.Append(TypeName);
                    return sb.ToString();
                }
                else
                    return string.Empty;                        
            }
            set
            {
                _AssemblyName = string.Empty;
                _TypeName = string.Empty;
                if (!string.IsNullOrEmpty(value))
                {
                    if (value.Contains(Const.DefaultStringDelimiter.ToString()))
                    {
                        int ix = value.IndexOf(Const.DefaultStringDelimiter);
                        _AssemblyName = value.Remove(ix).Trim();
                        _TypeName = value.Substring(ix + 1).Trim();
                    }
                    else
                        _TypeName = value;
                }
            }
        }

        public RepositoryClass ClassRef
        {
            get { return _ClassRef; }
            set { _ClassRef = value; }
        }

        #endregion

        // ------------------------------------------------------------------------------------------------------------

        #region методы

        public override string ToString()
        {
            return AsString;
        }

        #endregion

        // ------------------------------------------------------------------------------------------------------------
    }

    // =================================================================================================

    #region ClassPathLoader - загрузчик типов, описанных в виде ClassPath

    /// <summary>
    /// ClassPathLoader - загрузчик типов, описанных в виде ClassPath 
    /// с поддержкой поиска по указанным каталогам
    /// </summary>
    public class ClassPathLoader
    {
        private List<string> searchPaths;
        private AppDomain _Domain;

        public ClassPathLoader(string searchPathsString)
        {
            searchPaths = new List<string>(searchPathsString.Split(Const.DefaultStringDelimiter));
            _Domain = AppDomain.CurrentDomain;
            //_Domain.AssemblyResolve += new ResolveEventHandler(_Domain_AssemblyResolve);
        }

        Assembly _Domain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (args.Name.IndexOf(",") >= 0)
            {
                ClassPath cp = new ClassPath(args.Name.Remove(args.Name.IndexOf(",")) + ",");
                Assembly asm = LoadAssembly(cp);
                return asm;
            }
            else
                return null;
        }

        public AppDomain Domain
        {
            get { return _Domain; }
            set { _Domain = value; }
        }

        /// <summary>
        /// набор подкаталогов, в которых можно искать модули
        /// </summary>
        public List<string> SearchPaths
        {
            get { return searchPaths; }
        }
        
        #region методы

        /// <summary>
        /// создает экземпляр указанного типа и подгружает при этом сборку, есди это необходимо
        /// </summary>
        public object CreateInstance(ClassPath cpath)
        {
            Type type = LoadType(cpath);
            try
            {
                if (type != null)
                {
                    try
                    {
                        object result = Activator.CreateInstance(type);
                        return result;
                    }
                    catch (Exception ex)
                    {
                        throw new ClassPathLoaderException(cpath, "Ошибка при создании экземпляра типа", ex);
                    }
                }
                else
                    throw new ClassPathLoaderException(cpath, "Ошибка загрузки типа - не найден тип класса", null);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(string.Format("Произошла ошибка при создании экземпляра указанного класса.\r\n{0}",
                    ex.Message), "Ошибка загрузки типа", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return null;
            }
        }

        // ------------------------------------------------------------------------------------------------------------

        public Type LoadType(ClassPath cpath)
        {
            AppDomain domain = _Domain;
            if (string.IsNullOrEmpty(cpath.AssemblyName))
                domain = AppDomain.CurrentDomain;

            Type result = null;
            Assembly assembly = LoadAssembly(cpath);
            List<string> defaultNamespaces = new List<string>(StringsSchema.DefaultModuleNamespaces.Split(','));
            if (assembly != null)
            {
                // если указана сборка, то ищем только в ней
                result = probeTypeName(assembly, cpath.TypeName);

                // пробуем загрузить имя сборки + имя типа
                if (result == null)
                    result = probeTypeName(assembly,
                        assembly.FullName.Remove(assembly.FullName.IndexOf(',')) + "." + cpath.TypeName);

                if (result == null)
                    foreach (string defaultNamespace in defaultNamespaces)
                    {
                        string s = defaultNamespace;
                        if (!s.EndsWith("."))
                            s += ".";
                        result = probeTypeName(assembly, s + cpath.TypeName);
                        if (result != null)
                            break;
                    }
            }
            else
            {
                foreach (Assembly asm in domain.GetAssemblies())
                    if (asm.FullName.Contains("US"))
                        foreach (Type t in asm.GetTypes())
                        {
                            if (string.Compare(t.Name, cpath.TypeName, true) == 0)
                                return t;
                        }
            }
            return result;
        }

        // ------------------------------------------------------------------------------------------------------------

        private Type probeTypeName(Assembly assembly, string typeName)
        {
            return assembly.GetType(typeName, false);
        }

        // ------------------------------------------------------------------------------------------------------------

        private Assembly LoadAssembly(ClassPath cpath)
        {
            if (!string.IsNullOrEmpty(cpath.AssemblyName))
            {
                // этап 1 - пытаемся интерпретировать имя сборки как имя файла DLLки
                // ищем как просто файл, так и в указанных каталогах поиска

                List<string> searchFiles = new List<string>();

                // включаем в список поиска файлов исходную строку ссылки на сборку и файл, 
                // лежащий в корневом каталоге приложения
                searchFiles.Add(cpath.AssemblyName);
                searchFiles.Add(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, cpath.AssemblyName));

                foreach (string path in searchPaths)
                {
                    searchFiles.Add(Path.Combine(path, cpath.AssemblyName));
                    searchFiles.Add(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, path), cpath.AssemblyName));
                }

                foreach (string fileName in searchFiles)
                {
                    Assembly result = tryLoadAssemblyFile(fileName);
                    if (result == null)
                        result = tryLoadAssemblyFile(fileName + ".dll");
                    if (result != null)
                        return result;
                }

                // этап 2 - пытаемся загрузить сборку по полному имени
                try
                {
                    return Assembly.Load(new AssemblyName(cpath.AssemblyName));
                }
                catch 
                {
                }
            }
            return null;
        }

        private static Dictionary<string, Assembly> loaded_files = new Dictionary<string, Assembly>();

        private Assembly tryLoadAssemblyFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                try
                {
                    if (!searchPaths.Contains(Path.GetDirectoryName(fileName)))
                        searchPaths.Add(Path.GetDirectoryName(fileName));
                    string fn = Path.GetFullPath(fileName);
                    if (!loaded_files.ContainsKey(fn))
                    {                        
                        var result = Assembly.LoadFrom(fn);
                        loaded_files.Add(fn, result);
                        return result;
                    }
                    else
                    {
                        return loaded_files[fn];
                    }
                }
                catch (Exception ex)
                {
                    // в ситуации когда нашли файл а загрузить не можем - вызываем exception
                    throw new InvalidOperationException(string.Format(Strings.ClassPath_Assembly_Load_Error,
                        fileName, ex.Message), ex);
                }
            }
            return null;
        }

        #endregion

        // ------------------------------------------------------------------------------------------------------------

    }

    #endregion

    // =================================================================================================

    public class ClassPathList : List<ClassPath>
    {
    }

    // =================================================================================================

    public class ClassPathLoaderException : Exception
    {
        public ClassPathLoaderException(ClassPath classPath, string Message, Exception InnerException)
            : base(string.Format(
@"Ошибка загрузки заданного класса
  Тип класса: {0}
  Тип ошибки: {1}
  Текст ошибки: {2}", classPath.ToString(), Message, 
            (InnerException != null) ? InnerException.Message : string.Empty), InnerException)
        {

        }
    }
}
