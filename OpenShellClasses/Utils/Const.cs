﻿// =================================================================================================
//  Const.cs
//  Класс для определения общих констант
// =================================================================================================

using System.Text;

namespace OpenShellClasses
{
    /// <summary>
    /// Общие для универсальной оболочки и модулей константы
    /// </summary>
    public static class Const
    {
        //public const string DefaultNamespace = @"http://www.i-teco.ru/egrp";
        public static Encoding DefaultEncoding = Encoding.GetEncoding(1251);

        /// <summary>
        /// разделитель элементов в строках по умолчанию
        /// </summary>
        public static char DefaultStringDelimiter = ',';

    }
}
