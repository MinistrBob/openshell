﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;

namespace OpenShellClasses
{
    public class OdpNetConfiguration
    {
        private string _AppDirectory;
        private OdpNetConfigFileList _ConfigFiles;
        private OracleMajorVersion _OracleDataAccessVersionMajor = OracleMajorVersion.Unknown;
        private bool _OracleDataAccessExists;

        public OdpNetConfiguration()
        {
            _ConfigFiles = new OdpNetConfigFileList();
        }

        public string AppDirectory
        {
            get { return _AppDirectory; }
        }

        internal void Fill()
        {
            TryLoadDefaultAssembly();
            SearchAppDirectory();

            if (AppDirectory == null)
                _AppDirectory = AppDomain.CurrentDomain.BaseDirectory;

            _AppDirectory = Path.GetFullPath(AppDirectory);

            _ConfigFiles.Clear();
            _SearchForConfigFiles(AppDirectory);

            _OracleDataAccessExists = File.Exists(Path.Combine(AppDirectory, "Oracle.DataAccess.dll"));
            if (_OracleDataAccessExists)
                _OracleDataAccessVersionMajor = OdpNetConfigFile.GetMajorVersion(FileVersion.GetVersion(
                    Path.Combine(AppDirectory, "Oracle.DataAccess.dll")));
        }

        public OracleMajorVersion OracleDataAccessVersionMajor
        {
            get { return _OracleDataAccessVersionMajor; }
        }

        public bool OracleDataAccessExists
        {
            get { return _OracleDataAccessExists; }
        }

        private void SearchAppDirectory()
        {
            List<string> dirs = new List<string>();
            dirs.Add(AppDomain.CurrentDomain.BaseDirectory);
            dirs.Add(Directory.GetCurrentDirectory());
            dirs.Add(@"..");
            dirs.Add(@"..\..");
            dirs.Add(@"..\..\..");

            foreach (string dir in dirs)
                if (Directory.GetFiles(dir, "*.config").Length > 0)
                {
                    _AppDirectory = dir;
                    break;
                }

            if (string.IsNullOrEmpty(_AppDirectory))
            {
                foreach (string dir in dirs)
                    if (Directory.GetFiles(dir, "Oracle.DataAccess.dll").Length > 0)
                    {
                        _AppDirectory = dir;
                        break;
                    }
            }
        }

        private bool IsAppDirectory(string dir)
        {
            if (Directory.GetFiles(dir, "*.config").Length > 0)
                return true;
            return false;
        }

        public OdpNetConfigFileList ConfigFiles
        {
            get { return _ConfigFiles; }
        }

        private void TryLoadDefaultAssembly()
        {
            try
            {
            }
            catch 
            {
            }
        }

        private void _SearchForConfigFiles(string directory)
        {
            string [] files = Directory.GetFiles(directory, "*.config");
            foreach (string fn in files)
                if (!fn.Contains(".vshost.") && (string.Compare(Path.GetFileName(fn), "security.config", true) != 0) && !_ConfigFiles.Contains(fn))
                {
                    OdpNetConfigFile file = new OdpNetConfigFile(fn);
                    _ConfigFiles.Add(file);
                }
        }

        public OracleMajorVersion ConfiguredVersionMajor
        {
            get
            {
                OracleMajorVersion result = OracleMajorVersion.Unknown;
                foreach (OdpNetConfigFile cf in ConfigFiles)
                    if (cf.NewVersionMajor != OracleMajorVersion.Unknown)
                    {
                        if (result == OracleMajorVersion.Unknown)
                            result = cf.NewVersionMajor;
                        else if (result != cf.NewVersionMajor)
                        {
                            result = OracleMajorVersion.Unknown;
                            break;
                        }
                    }
                return result;
            }
        }

        /// <summary>
        /// рекомендуемая версия
        /// </summary>
        /// <param name="oracleConfig"></param>
        /// <param name="bundle"></param>
        /// <returns></returns>
        //public OdpNetBundleItem GetRecommendedBundleItem(OracleConfiguration oracleConfig, OdpNetBundle bundle)
        //{
        //    OracleMajorVersion homeVersion = oracleConfig.GetRecommendedVersionMajor();

        //    foreach (OdpNetBundleItem item in bundle.Items)
        //    {
        //        if (item.VersionMajor == homeVersion && item.CheckPlatform(item))
        //        {
        //            return item;
        //        }
        //    }
           
        //    return null;
        //}
    }

    public class OdpNetConfigFile
    {
        private string _FileName;
        private bool _IsRedirectionSet;
        private string _OldVersion;
        private string _NewVersion;
        private bool _IgnorePublisherPolicy;
        private OracleMajorVersion _NewVersionMajor;

        public OdpNetConfigFile(string FileName)
        {
            _FileName = FileName;

            AssemblyName an = new AssemblyName("Oracle.DataAccess");

            LoadInfo();
        }

        private void LoadInfo()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(FileName);

            _IsRedirectionSet = false;

            if ((doc["configuration"] != null) &&
                (doc["configuration"]["runtime"] != null) &&
                (doc["configuration"]["runtime"]["assemblyBinding"] != null))
            {
                foreach (XmlNode n in doc["configuration"]["runtime"]["assemblyBinding"].ChildNodes)
                    if (string.Compare(n.Name, "dependentAssembly", true) == 0 &&
                        n["assemblyIdentity"] != null &&
                        n["assemblyIdentity"].Attributes["name"].InnerText == "Oracle.DataAccess"
)
                    {
                        _IsRedirectionSet = n["bindingRedirect"] != null;
                        if (_IsRedirectionSet)
                        {
                            _OldVersion = n["bindingRedirect"].Attributes["oldVersion"].InnerText;
                            _NewVersion = n["bindingRedirect"].Attributes["newVersion"].InnerText;
                        }
                        _IgnorePublisherPolicy = false;
                        if (n["publisherPolicy"] != null)
                        {
                            string v = n["publisherPolicy"].Attributes["apply"].InnerText;
                            if ((string.Compare(v, "no", true) == 0) ||
                                (string.Compare(v, "false", true) == 0))
                            {
                                _IgnorePublisherPolicy = true;
                                _NewVersionMajor = GetMajorVersion(_NewVersion);
                            }
                        }
                    }
            }
        }

        public bool IsRedirectionSet
        {
            get { return _IsRedirectionSet; }
            set { _IsRedirectionSet = value; }
        }

        public string OldVersion
        {
            get { return _OldVersion; }
            set { _OldVersion = value; }
        }

        public string NewVersion
        {
            get { return _NewVersion; }
            set { _NewVersion = value; }
        }

        public bool IgnorePublisherPolicy
        {
            get { return _IgnorePublisherPolicy; }
            set { _IgnorePublisherPolicy = value; }
        }

        public OracleMajorVersion NewVersionMajor
        {
            get { return _NewVersionMajor; }
        }

        public static OracleMajorVersion GetMajorVersion(string newVersion)
        {
            string[] vv = newVersion.Split('.');
            OracleMajorVersion result = OracleMajorVersion.Unknown;

            int first_num = 0;
            int second_num = 0;
            int v_no = 0;

            if (vv.Length > 0)
                int.TryParse(vv[0], out first_num);

            if (vv.Length > 1)
                int.TryParse(vv[1], out second_num);

            if (first_num >= 1 && first_num <= 3)
            {
                // формат записи - .NET версия.сборка + 100
                v_no = (second_num / 10);
            }
            else
                v_no = first_num;

            if (v_no == 8)
                result = OracleMajorVersion.V8;
            else if (v_no == 9)
                result = OracleMajorVersion.V9;
            else if (v_no == 10)
                result = OracleMajorVersion.V10;
            else if (v_no == 11)
                result = OracleMajorVersion.V11;

            return result;
        }

        public string Name
        {
            get { return Path.GetFileName(FileName); }
        }

        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }

        public string Directory
        {
            get { return System.IO.Path.GetDirectoryName(FileName); }
        }

        //internal void UpdateBindingRedirect(OdpNetBundleItem item)
        //{
        //    if (File.Exists(FileName))
        //    {
        //        XmlDocument doc = new XmlDocument();
        //        doc.Load(FileName);

        //        XmlNode n = doc;
        //        if (n["configuration"] == null)
        //            n.AppendChild(doc.CreateElement("configuration"));

        //        if (n["configuration"]["runtime"] == null)
        //            n["configuration"].AppendChild(doc.CreateElement("runtime"));

        //        if (n["configuration"]["runtime"]["assemblyBinding"] == null)
        //        {
        //            XmlAttribute a = doc.CreateAttribute("xmlns");
        //            a.Value = "urn:schemas-microsoft-com:asm.v1";
        //            n["configuration"]["runtime"].AppendChild(doc.CreateElement("assemblyBinding")).Attributes.Append(a);
        //        }

        //        n = n["configuration"]["runtime"]["assemblyBinding"];

        //        XmlNode dep_node = null;
        //        foreach (XmlNode nn in n.ChildNodes)
        //            if (nn.Name == "dependentAssembly" && nn["assemblyIdentity"] != null)
        //            {
        //                dep_node = nn;
        //                break;
        //            }

        //        if (dep_node == null)
        //        {
        //            dep_node = n.AppendChild(doc.CreateElement("dependentAssembly"));
        //            dep_node.AppendChild(doc.CreateElement("assemblyIdentity"));
        //            dep_node.AppendChild(doc.CreateElement("bindingRedirect"));
        //            dep_node.AppendChild(doc.CreateElement("publisherPolicy"));                    
        //        }

        //        if (dep_node["assemblyIdentity"].Attributes["name"] == null)
        //            dep_node["assemblyIdentity"].Attributes.Append(doc.CreateAttribute("name"));
        //        dep_node["assemblyIdentity"].Attributes["name"].Value = "Oracle.DataAccess";

        //        if (dep_node["assemblyIdentity"].Attributes["publicKeyToken"] == null)
        //            dep_node["assemblyIdentity"].Attributes.Append(doc.CreateAttribute("publicKeyToken"));
        //        dep_node["assemblyIdentity"].Attributes["publicKeyToken"].Value = "89b483f429c47342";

        //        if (dep_node["bindingRedirect"].Attributes["oldVersion"] == null)
        //            dep_node["bindingRedirect"].Attributes.Append(doc.CreateAttribute("oldVersion"));
        //        dep_node["bindingRedirect"].Attributes["oldVersion"].Value = "0.0.0.0-99.999.999.999";

        //        if (dep_node["bindingRedirect"].Attributes["newVersion"] == null)
        //            dep_node["bindingRedirect"].Attributes.Append(doc.CreateAttribute("newVersion"));
        //        dep_node["bindingRedirect"].Attributes["newVersion"].Value = item.Version;

        //        if (dep_node["publisherPolicy"].Attributes["apply"] == null)
        //            dep_node["publisherPolicy"].Attributes.Append(doc.CreateAttribute("apply"));
        //        dep_node["publisherPolicy"].Attributes["apply"].Value = "no";

        //        doc.Save(FileName);
        //    }


        //}
    }

    public class OdpNetConfigFileList : List <OdpNetConfigFile>
    {
        public OdpNetConfigFile FindByPath(string path)
        {
            path = Path.GetFullPath(path);
            return Find(delegate(OdpNetConfigFile fl)
            {
                return (string.Compare(fl.FileName, path, true) == 0);
            });
        }

        internal bool Contains(string fn)
      
        {
            return (FindByPath(fn) != null);
        }
    }
}
