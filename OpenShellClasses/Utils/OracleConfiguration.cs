﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Win32;
using System.Diagnostics;

namespace OpenShellClasses
{
    public enum OracleMajorVersion
    {
        Unknown = 0,
        V8 = 8,
        V9 = 9,
        V10 = 10,
        V11 = 11
    }

    public class OracleConfiguration
    {
        private OracleHomeInfoList _Homes;

        public OracleConfiguration()
        {
            _Homes = new OracleHomeInfoList();
        }

        public virtual void Fill()
        {
                _FillOracleHomeInfoList();
        }

        private void _FillOracleHomeInfoList()
        {
            Homes.Clear();
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"Software\Oracle");
            if (rk != null)
            {
                string[] sub_keys = rk.GetSubKeyNames();
                foreach (string sub_key in sub_keys)
                {
                    RegistryKey rk_home = rk.OpenSubKey(sub_key);
                    if (rk_home.GetValue("ORACLE_HOME") != null)
                    {
                        OracleHomeInfo home = new OracleHomeInfo();
                        home.HomeName = Convert.ToString(rk_home.GetValue("ORACLE_HOME_NAME"));
                        home.HomePath = Convert.ToString(rk_home.GetValue("ORACLE_HOME"));
                        home.GroupName = Convert.ToString(rk_home.GetValue("ORACLE_GROUP_NAME"));
                        home.NlsLang = Convert.ToString(rk_home.GetValue("NLS_LANG"));

                        foreach (string val_name in rk_home.GetValueNames())
                            home.RegistryValues.Add(val_name.ToUpper(), rk_home.GetValue(val_name));

                        home.CheckOraHome();

                        Homes.Add(home);
                    }
                }
            }
        }

        public OracleHomeInfoList Homes
        {
            get { return _Homes; }
        }

        public OracleMajorVersion GetRecommendedVersionMajor()
        {
            OracleMajorVersion result = OracleMajorVersion.Unknown;
            foreach (OracleHomeInfo home in Homes)
                if (home.IsValid && home.VersionMajor > result)
                {
                    result = home.VersionMajor;
                }

            if (result == OracleMajorVersion.V11)
            {
                foreach (OracleHomeInfo home in Homes)
                    if (home.IsValid && home.VersionMajor == OracleMajorVersion.V10)
                    {
                        result = home.VersionMajor;
                        break;
                    }
            }

            return result;
        }
    }

    public class OracleHomeInfo
    {
        public string NlsLang;
        public string HomePath;
        public string HomeName;
        public bool IsValid;
        public string IsNotValid_Text;
        public string AdminPath;
        public string TnsNamesOraFileName;
        public string TnsNamesOraContents;
        public string SqlNetOraFileName;
        public string GroupName;
        public string Version;

        public string OciDllPath;
        public string BinPath;

        public OracleMajorVersion VersionMajor;

        public Dictionary<string, object> _RegistryValues;

        private TnsNamesItemList _Names;

        public OracleHomeInfo()
        {
            _RegistryValues = new Dictionary<string, object>();
            _Names = new TnsNamesItemList();
        }

        public Dictionary<string, object> RegistryValues
        {
            get { return _RegistryValues; }
        }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(HomeName))
                return HomeName;
            else
                return base.ToString();
        }

        public TnsNamesItemList Names
        {
            get { return _Names; }
        }

        /// <summary>
        /// проверка информации о заданном Oracle Home
        /// </summary>
        public virtual void CheckOraHome()
        {
            try
            {
                CheckParams();

                IsValid = true;
                IsNotValid_Text = string.Empty;

                Version = FileVersion.GetVersion(OciDllPath);

                ProbeHomeVersion();


                if (!Directory.Exists(HomePath))
                {
                    IsValid = false;
                    IsNotValid_Text = string.Format("Не найден каталог Oracle Home ({0})!", HomePath);
                    return;
                }

                if (!File.Exists(OciDllPath))
                {
                    IsValid = false;
                    IsNotValid_Text = string.Format("Не найден файл ({0})!", OciDllPath);
                    return;
                }

                if (!Directory.Exists(AdminPath))
                {
                    IsValid = false;
                    IsNotValid_Text = string.Format("Не найден каталог администратора для Oracle Home ({0})!", AdminPath);
                    return;
                }

                ReadTnsNames();


            }
            catch (Exception ex)
            {
                IsNotValid_Text = string.Format("Непредвиденная ошибка при проверке Oracle Home:\r\n{0}\r\nСтек трассировки: {1}",
                    ex.Message, ex.StackTrace);
                IsValid = false;
            }
        }

        private void ReadTnsNames()
        {            
            if (File.Exists(TnsNamesOraFileName))
            {
                TnsNamesOraContents = File.ReadAllText(TnsNamesOraFileName, Encoding.GetEncoding(1251));

                Regex regex_tns = new Regex(
                    @"(?<name>\S+)\s*=\s*\(DESCRIPTION\s*=\s*\(ADDRESS", 
                    RegexOptions.Multiline | RegexOptions.Compiled | RegexOptions.IgnoreCase);

                MatchCollection mc = regex_tns.Matches(TnsNamesOraContents);

                for (int i = 0; i < mc.Count; i++)
                {
                    int next_index = TnsNamesOraContents.Length;
                    if (i < mc.Count - 1)
                        next_index = mc[i + 1].Index;

                    string s = this.TnsNamesOraContents.Substring(mc[i].Index, (next_index - mc[i].Index));

                    TnsNamesItem item = new TnsNamesItem(s);
                    Names.Add(item);
                }
            }
        }

        private void ProbeHomeVersion()
        {
            if (!string.IsNullOrEmpty(Version) && Version.Contains("."))
            {
                string s = Version.Remove(Version.IndexOf('.'));
                int i;
                if (int.TryParse(s, out i))
                {
                    if (i == 10)
                        VersionMajor = OracleMajorVersion.V10;
                    else if (i == 11)
                        VersionMajor = OracleMajorVersion.V11;
                    else if (i == 9)
                        VersionMajor = OracleMajorVersion.V9;
                    else if (i == 8)
                        VersionMajor = OracleMajorVersion.V8;
                }
            }
            if (VersionMajor == OracleMajorVersion.Unknown)
            {
                if (File.Exists(Path.Combine(BinPath, "ORA803.DLL")) ||
                    File.Exists(Path.Combine(BinPath, "ORA804.DLL")) ||
                    File.Exists(Path.Combine(BinPath, "ORA805.DLL")))
                    VersionMajor = OracleMajorVersion.V8;
            }

        }

        private void CheckParams()
        {
            BinPath = Path.Combine(HomePath, "bin");
            if (!Directory.Exists(BinPath))
                BinPath = HomePath;

            ProbeHomeVersion();

            AdminPath = Path.Combine(HomePath, "network\\admin");
            if (VersionMajor == OracleMajorVersion.V8)
                AdminPath = Path.Combine(HomePath, "net80\\admin");

            if (!Directory.Exists(AdminPath) && _RegistryValues.ContainsKey("TNS_ADMIN"))
                AdminPath = _RegistryValues["TNS_ADMIN"].ToString();

            OciDllPath = Path.Combine(BinPath, "oci.dll");
            if (!File.Exists(OciDllPath))
                OciDllPath = Path.Combine(HomePath, "oci.dll");

            TnsNamesOraFileName = Path.Combine(AdminPath, "tnsnames.ora");
            SqlNetOraFileName = Path.Combine(AdminPath, "sqlnet.ora");


        }
    }

    public class OracleHomeInfoList : List<OracleHomeInfo>
    {
    }

    public class OdpNetAppConfigFileInfo
    {
        public OdpNetAppConfigFileInfo(string fileName)
        {

        }

        public string FileName
        {
            get;
            private set;
        }
    }

    //public class Win32Imports
    //{
    //    [DllImport("version.dll")]
    //    public static extern bool GetFileVersionInfo(string sFileName,
    //          int handle, int size, byte[] infoBuffer);
    //    [DllImport("version.dll")]
    //    public static extern int GetFileVersionInfoSize(string sFileName,
    //          out int handle);
        
    //    // The third parameter - "out string pValue" - is automatically
    //    // marshaled from ANSI to Unicode:
    //    [DllImport("version.dll")]
    //    unsafe public static extern bool VerQueryValue(byte[] pBlock,
    //          string pSubBlock, out string pValue, out uint len);
    //    // This VerQueryValue overload is marked with 'unsafe' because 
    //    // it uses a short*:
    //    [DllImport("version.dll")]
    //    unsafe public static extern bool VerQueryValue(byte[] pBlock,
    //          string pSubBlock, out short* pValue, out uint len);
    //}

    //public class FileVersion
    //{
    //    unsafe public static string GetVersion(string fileName)
    //    {
    //        try
    //        {
    //            int handle = 0;
    //            // Figure out how much version info there is:
    //            int size =
    //                  Win32Imports.GetFileVersionInfoSize(fileName,
    //                  out handle);

    //            if (size == 0) return string.Empty;

    //            byte[] buffer = new byte[size];

    //            if (!Win32Imports.GetFileVersionInfo(fileName, handle, size, buffer))
    //            {
    //                return "Failed to query file version information.";
    //            }

    //            short* subBlock = null;
    //            uint len = 0;
    //            // Get the locale info from the version info:
    //            if (!Win32Imports.VerQueryValue(buffer, @"\VarFileInfo\Translation", out subBlock, out len))
    //            {
    //                return "Failed to query file version information.";
    //            }

    //            string spv = @"\StringFileInfo\" + subBlock[0].ToString("X4") + subBlock[1].ToString("X4") + @"\FileVersion";

    //            byte* pVersion = null;
    //            // Get the ProductVersion value for this program:
    //            string versionInfo;

    //            if (!Win32Imports.VerQueryValue(buffer, spv, out versionInfo, out len))
    //            {
    //                return "Failed to query version information.";
    //            }

    //            return versionInfo;
    //        }
    //        catch (Exception e)
    //        {
    //            return "Caught unexpected exception " + e.Message;
    //        }
    //    }
    //}

    public class FileVersion
    {
        public static string GetVersion(string fileName)
        {
            try
            {
                string versionInfo;

                FileVersionInfo myFileVersionInfo = FileVersionInfo.GetVersionInfo(fileName);
                versionInfo = myFileVersionInfo.FileMajorPart.ToString();

                return versionInfo;
            }
            catch (Exception e)
            {
                return string.Format("Не могу получить версию файла {0} ошибка {1}", fileName, e.Message);
            }
        }
    }

    public class TnsNamesItem
    {
        private string _Name;
        private string _Host;
        private string _Protocol;
        private string _Port;
        private string _ServiceName;
        private string _Text;

        private static Regex regex_desc = new Regex(
                @"(?<name>\S+)\s*=\s*\(DESCRIPTION",
                RegexOptions.Multiline | RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private static Regex regex_prot = new Regex(
                    @"PROTOCOL\s*=\s*(?<name>[^\)]+)\)",
                    RegexOptions.Multiline | RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private static Regex regex_port = new Regex(
                    @"PORT\s*=\s*(?<name>[^\)]+)\)",
                    RegexOptions.Multiline | RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private static Regex regex_host = new Regex(
                    @"HOST\s*=\s*(?<name>[^\)]+)\)",
                    RegexOptions.Multiline | RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private static Regex regex_serviceName = new Regex(
                    @"SERVICE_NAME\s*=\s*(?<name>[^\)]+)\)",
                    RegexOptions.Multiline | RegexOptions.Compiled | RegexOptions.IgnoreCase);



        public TnsNamesItem(string data)
        {
            Match m = regex_desc.Match(data);
            if (m.Success)
                _Name = m.Groups["name"].Value.Trim();

            m = regex_prot.Match(data);
            if (m.Success)
                _Protocol = m.Groups["name"].Value.Trim();

            m = regex_port.Match(data);
            if (m.Success)
                _Port = m.Groups["name"].Value.Trim();

            m = regex_host.Match(data);
            if (m.Success)
                _Host = m.Groups["name"].Value.Trim();

            m = regex_serviceName.Match(data);
            if (m.Success)
                _ServiceName = m.Groups["name"].Value.Trim();

            _Text = data;
        }

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string Host
        {
            get { return _Host; }
            set { _Host = value; }
        }

        public string Port
        {
            get { return _Port; }
            set { _Port = value; }
        }

        public string Protocol
        {
            get { return _Protocol; }
            set { _Protocol = value; }
        }

        public override string ToString()
        {
            return Name;
        }

        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        public string ServiceName
        {
            get { return _ServiceName; }
            set { _ServiceName = value; }
        }
    }

    public class TnsNamesItemList : List<TnsNamesItem>
    {
    }
 

}
