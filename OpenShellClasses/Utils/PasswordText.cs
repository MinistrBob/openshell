﻿// =================================================================================================
//  PasswordText.cs
//  Классы для хранения паролей в зашифрованном виде
// =================================================================================================

using System;
using System.ComponentModel;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;

namespace OpenShellClasses
{
    [Serializable]
    [TypeConverter(typeof(PasswordTextTypeConverter))]
    public class PasswordText
    {
        private string encryptedText = string.Empty;
        private int len;

        private byte[] key = Encoding.Default.GetBytes("ка029jds");
        private byte[] iv = Encoding.Default.GetBytes( "jleiw521");

        public PasswordText()
        {
        }

        [XmlIgnore]
        public string Text
        {
            get { return Decrypt(); }
            set { Encrypt(value); }
        }

        [XmlAttribute(AttributeName="text")]
        public string EncryptedText
        {
            get { return encryptedText; }
            set 
            { 
                encryptedText = value;
                len = Decrypt().Length;
            }
        }

        [XmlAttribute(AttributeName = "length")]
        public int Length
        {
            get { return len; }
            set { len = value; }
        }

        private void Encrypt(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                DES csp;
                csp = DES.Create();
                ICryptoTransform tr = csp.CreateEncryptor(key, iv);
                byte[] buf = Encoding.Default.GetBytes(value);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, tr, CryptoStreamMode.Write);
                StreamWriter sw = new StreamWriter(cs);
                sw.Write(value);
                sw.Flush();
                cs.FlushFinalBlock();
                encryptedText = Convert.ToBase64String(ms.ToArray());
            }
            else
                encryptedText = string.Empty;
            len = value.Length;
        }

        private string Decrypt()
        {
            if (!string.IsNullOrEmpty(encryptedText))
            {
                DES csp = DES.Create();
                ICryptoTransform tr = csp.CreateDecryptor(key, iv);
                MemoryStream ms = new MemoryStream(Convert.FromBase64String(encryptedText));
                CryptoStream cs = new CryptoStream(ms, tr, CryptoStreamMode.Read);
                StreamReader sr = new StreamReader(cs);
                return sr.ReadToEnd();
            }
            else
                return string.Empty;
        }

        public override string ToString()
        {
            return new string('*', len);
        }
    }

    public class PasswordTextTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            else
                return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string)
            {
                string s = value.ToString();
                    PasswordText pwdText = context.PropertyDescriptor.GetValue(context.Instance) as PasswordText;
                if (!s.Contains("*"))
                {
                    pwdText.Text = s;
                }
                return pwdText;
            }
                return base.ConvertFrom(context, culture, value);
        }
    }
}
