﻿// =================================================================================================
//  Property.cs
//  Общие классы для определения свойств типа Имя-Значение
// =================================================================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OpenShellClasses
{

    // =======================================================================================

    public enum PropertyDataType
    {
        String,
        Int32,
        Int64,
        DateTime,
        Double,
        Object,
        List,
        Bool
    }

    // =======================================================================================

    [XmlType(TypeName = "Property")]
    public class Property
    {
        private string _Name = string.Empty;
        private string _Caption = string.Empty;
        private string _Description = string.Empty;
        private string _Category = string.Empty;
        private PropertyDataType _DataType = PropertyDataType.Object;
        private object _Value = null;
        private PropertyListItemCollection _ListItems;
        private bool _Visible = true;

        public Property()
        {
            //_ListItems = new PropertyListItemCollection();
        }

        public Property(string Name, PropertyDataType DataType) : this()
        {
            this.Name = Name;
            this.DataType = DataType;
            this.Visible = false;
        }

        public Property(string Name, object Value) : this(Name, Name, Value)
        {
        }

        public Property(string Name, string Caption, object Value) : 
            this(Name, Caption, (Value != null) ? GetNativeTypeAsDataType(Value.GetType()): PropertyDataType.Object, Value)
        {
        }

        public Property(string Name, string Caption, PropertyDataType DataType, object Value)
            : this()
        {
            this.Name = Name;
            this.Caption = Caption;
            this.Value = Value;
            this.DataType = DataType;
            this.Visible = false;
        }

        [XmlAttribute]
        [DefaultValue("")]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        [XmlAttribute]
        [DefaultValue("")]
        public string Caption
        {
            get { return _Caption; }
            set { _Caption = value; }
        }

        [XmlAttribute]
        [DefaultValue(true)]
        public bool Visible
        {
            get { return _Visible; }
            set { _Visible = value; }
        }

        public object Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        [XmlIgnore]
        public string AsString
        {
            get { return (Value != null) ? Value.ToString() : null; }
            set { Value = value; }
        }

        [XmlAttribute]
        [DefaultValue("")]
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        [XmlAttribute]
        [DefaultValue("")]
        public string Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        [XmlAttribute]
        public PropertyDataType DataType
        {
            get { return _DataType; }
            set { _DataType = value; }
        }

        public PropertyListItemCollection ListItems
        {
            get { return _ListItems; }
            set { _ListItems = value; }
        }

        public static Type GetDataTypeAsNativeType(PropertyDataType DataType)
        {
            switch (DataType)
            {
                case PropertyDataType.String:
                    return typeof(string);
                case PropertyDataType.Int32:
                    return typeof(Int32);
                case PropertyDataType.Int64:
                    return typeof(Int64);
                case PropertyDataType.DateTime:
                    return typeof(DateTime);
                case PropertyDataType.Double:
                    return typeof(Double);
                case PropertyDataType.Bool:
                    return typeof(bool);
                default:
                    return typeof(object);
            }
        }

        public static PropertyDataType GetNativeTypeAsDataType(Type NativeType)
        {
            if (NativeType == typeof(string))
                return PropertyDataType.String;
            else if (NativeType == typeof(Int32))
                return PropertyDataType.Int32;
            else if (NativeType == typeof(Int32))
                return PropertyDataType.Int64;
            else if (NativeType == typeof(DateTime))
                return PropertyDataType.DateTime;
            else if (NativeType == typeof(Double))
                return PropertyDataType.Double;
            else if (NativeType == typeof(bool))
                return PropertyDataType.Bool;
            else
                return PropertyDataType.Object;
        }

        [XmlIgnore]
        public Type DataTypeAsNativeType
        {
            get { return GetDataTypeAsNativeType(DataType); }
        }
    }

    // =======================================================================================

    [XmlType(TypeName = "Properties")]
    public class PropertyList : List<Property>
    {
        private XmlWriterSettings _XmlWriterSettings;
        private XmlReaderSettings _XmlReaderSettings;

        public int IndexOf(string Name)
        {
            for (int i = 0; i < Count; i++)
                if (string.Compare(this[i].Name, Name, true) == 0)
                    return i;
            return -1;
        }

        public new Property Add(Property item)
        {
            base.Add(item);
            return item;
        }

        public Property this[string Name]
        {
            get
            {
                int index = IndexOf(Name);
                if (index >= 0)
                    return this[index];
                else
                {
                    Property newProp = new Property(Name, PropertyDataType.String);
                    Add(newProp);
                    return newProp;
                }
            }
            set
            {
                int index = IndexOf(Name);
                if (index >= 0)
                    this[index].Value = value;
                else
                    Add(new Property(Name, value));
            }
        }

        public object Get(string Name, object DefaultValue)
        {
            if (IndexOf(Name) == -1)
                Add(new Property(Name, Name, Property.GetNativeTypeAsDataType(DefaultValue.GetType()), DefaultValue));
            return this[Name].Value;
        }

        #region SERIALIZATION

        public void Serialize(XmlWriter writer, bool IncludeSchema)
        {
            GetSerializer(IncludeSchema).Serialize(writer, this);
        }

        public string Serialize(bool IncludeSchema)
        {
            StringBuilder sb = new StringBuilder();
            MemoryStream ms = new MemoryStream();
            //using (StreamWriter sw = new StreamWriter(ms, Encoding.GetEncoding(1251)))
            ///using (XmlWriter xtw = XmlTextWriter.Create(sw, XmlWriterSettings))
            {
                Serialize(new XmlTextWriter(ms, Encoding.GetEncoding(1251)), IncludeSchema);
                ms.Seek(0, SeekOrigin.Begin);
                return Encoding.GetEncoding(1251).GetString(ms.ToArray());
            }
        }

        public void Serialize(string fileName, bool IncludeSchema)
        {
            using (XmlWriter xtw = XmlTextWriter.Create(fileName, XmlWriterSettings))
            {
                Serialize(xtw, IncludeSchema);
            }
        }

        private XmlSerializer GetSerializer(bool includeSchema)
        {
            XmlAttributeOverrides xo = new XmlAttributeOverrides();
            XmlAttributes ignoreAttributes = new XmlAttributes();
            ignoreAttributes.XmlIgnore = true;

            if (!includeSchema)
            {
                xo.Add(typeof(Property), "DataType", ignoreAttributes);
                xo.Add(typeof(Property), "Category", ignoreAttributes);
                xo.Add(typeof(Property), "Caption", ignoreAttributes);
                xo.Add(typeof(Property), "Description", ignoreAttributes);
                xo.Add(typeof(Property), "ListItems", ignoreAttributes);
                xo.Add(typeof(Property), "Visible", ignoreAttributes);
            }

            return new XmlSerializer(typeof(PropertyList), xo);
        }

        private XmlWriterSettings XmlWriterSettings
        {
            get
            {
                if (_XmlWriterSettings == null)
                {
                    _XmlWriterSettings = new XmlWriterSettings();
                    _XmlWriterSettings.Encoding = System.Text.Encoding.GetEncoding(1251);
                    _XmlWriterSettings.Indent = true;
                }
                return _XmlWriterSettings;
            }
        }

        private XmlReaderSettings XmlReaderSettings
        {
            get
            {
                if (_XmlReaderSettings == null)
                {
                    _XmlReaderSettings = new XmlReaderSettings();
                }
                return _XmlReaderSettings;
            }
        }


        public static PropertyList DeserializeSchema(string data)
        {
            if (!string.IsNullOrEmpty(data))
            {
                XmlSerializer xs = new XmlSerializer(typeof(PropertyList));
                using (MemoryStream ms = new MemoryStream(Encoding.GetEncoding(1251).GetBytes(data)))
                {
                    ms.Seek(0, SeekOrigin.Begin);
                    using (StreamReader sr = new StreamReader(ms, Encoding.GetEncoding(1251)))
                        return (PropertyList)xs.Deserialize(sr);
                }
            }
            else
                return new PropertyList();
        }

        public void Deserialize(XmlTextReader reader)
        {
        }

        #endregion

        /// <summary>
        /// установка значений параметров из указанного блока параметров
        /// </summary>
        /// <param name="pl"></param>
        public void SetValues(PropertyList pl)
        {
            foreach (Property p in pl)
            {
                if (this[p.Name] != null)
                    this[p.Name].Value = p.Value;
            }
        }

        public void Deserialize(string FileName)
        {
            using (XmlReader xr = XmlReader.Create(FileName, XmlReaderSettings))
                Deserialize(xr);
        }

        private void Deserialize(XmlReader xr)
        {
            XmlSerializer xs = GetSerializer(false);
            PropertyList values = xs.Deserialize(xr) as PropertyList;
            SetValues(values);
        }

        internal void DeserializeString(string data)
        {
            if (!string.IsNullOrEmpty(data))
                using (XmlReader xtr = XmlReader.Create(new StringReader(data)))
                    Deserialize(xtr);
        }

        public PropertyList Clone()
        {
            return DeserializeSchema(Serialize(true));
        }
    }

    // =======================================================================================

    public class PropertyListTypeDescriptor : ICustomTypeDescriptor
    {
        private PropertyList _PropList;
        private AttributeCollection _AttributeCollection;
        private EventDescriptorCollection _EventDescriptorCollection;
        private PropertyDescriptorCollection _PropertyDescriptorCollection;

        public PropertyListTypeDescriptor(PropertyList PropList)
        {
            this._PropList = PropList;
            BuildPropertyDescriptorCollection();
        }

        private void BuildPropertyDescriptorCollection()
        {
            List<PropertyDescriptor> pda = new List<PropertyDescriptor>();
            foreach (Property prop in _PropList)
                if (prop.Visible)
                    pda.Add(new PropertyPropertyDescriptor(prop));
            _PropertyDescriptorCollection = new PropertyDescriptorCollection(pda.ToArray());
        }

        #region ICustomTypeDescriptor Members

        public AttributeCollection GetAttributes()
        {
            if (_AttributeCollection == null)
                _AttributeCollection = new AttributeCollection();
            return _AttributeCollection;
        }

        public string GetClassName()
        {
            return _PropList.GetType().Name;
        }

        public string GetComponentName()
        {
            return _PropList.GetType().Name;
        }

        public TypeConverter GetConverter()
        {
            return null;
        }

        public EventDescriptor GetDefaultEvent()
        {
            return null;
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return null;
        }

        public object GetEditor(Type editorBaseType)
        {
            return null;
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return GetEvents();
        }

        public EventDescriptorCollection GetEvents()
        {
            if (_EventDescriptorCollection == null)
                _EventDescriptorCollection = new EventDescriptorCollection(new System.ComponentModel.EventDescriptor[] { });
            return _EventDescriptorCollection;
        }

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            return GetProperties();
        }

        public PropertyDescriptorCollection GetProperties()
        {
            return _PropertyDescriptorCollection;
        }

        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }

        #endregion
    }

    public class PropertyPropertyDescriptor : PropertyDescriptor
    {
        private Property _Property;

        public PropertyPropertyDescriptor(Property Property) : base(Property.Caption, GetAttributes(Property))
        {
            this._Property = Property;
        }

        private static Attribute[] GetAttributes(Property Property)
        {
            List<Attribute> result = new List<Attribute>();
            if (!string.IsNullOrEmpty(Property.Description))
                result.Add(new DescriptionAttribute(Property.Description));
            if (!string.IsNullOrEmpty(Property.Category))
                result.Add(new CategoryAttribute(Property.Category));
            if (Property.ListItems != null && Property.ListItems.Count > 0)
                result.Add(new TypeConverterAttribute(typeof(PropertyListItemTypeConverter)));
            return result.ToArray();
        }

        public Property Property
        {
            get { return _Property; }
        }

        public override bool CanResetValue(object component)
        {
            return false;
        }

        public override Type ComponentType
        {
            get { return typeof(PropertyList); }
        }

        public override object GetValue(object component)
        {
            return _Property.Value;
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        public override Type PropertyType
        {
            get 
            {
                return _Property.DataTypeAsNativeType;
            }
        }

        public override void ResetValue(object component)
        {            
        }

        public override void SetValue(object component, object value)
        {
            if (_Property.ListItems.Count > 0)
                foreach (PropertyListItem li in _Property.ListItems)
                {
                    if (string.Compare(li.Name, value.ToString(), true) == 0)
                    {
                        _Property.Value = li.Value;
                        return;
                    }
                }
            _Property.Value = value;
        }

        public override bool ShouldSerializeValue(object component)
        {
            return false;
        }
    }

    // =======================================================================================

    [XmlType(TypeName="ListItem")]
    public class PropertyListItem 
    {
        private string _Name;
        private string _Value;

        public PropertyListItem()
        {
        }

        public PropertyListItem(string Name, string Value)
        {
            this._Name = Name;
            this._Value = Value;
        }

        public PropertyListItem(string Name, object Value) : this(Name, Value.ToString())
        {
        }

        [XmlAttribute]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        [XmlAttribute]
        public string Value
        {
            get { return _Value; }
            set 
            { 
                _Value = value;
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }

    // =======================================================================================

    [XmlType(TypeName="ListItems")]
    public class PropertyListItemCollection : List<PropertyListItem>
    {
        public void Add(string Name, object Value)
        {
            Add(new PropertyListItem(Name, Value));
        }
    }

    // =======================================================================================

    public class PropertyListItemTypeConverter : TypeConverter
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            return true;
        }

        public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            PropertyPropertyDescriptor dsc = (PropertyPropertyDescriptor)context.PropertyDescriptor;
            List<string> names = new List<string>();
            foreach (PropertyListItem pli in dsc.Property.ListItems)
                names.Add(pli.Name);
            return new StandardValuesCollection(names);
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            else
                return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string && context != null)
            {
                PropertyPropertyDescriptor dsc = (PropertyPropertyDescriptor)context.PropertyDescriptor;
                foreach (PropertyListItem pli in dsc.Property.ListItems)
                    if (string.Compare(pli.Name, value.ToString(), true) == 0)
                        return pli.Value;
                return value;
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
                return true;
            else
                return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (value is string && context != null)
            {
                PropertyPropertyDescriptor dsc = (PropertyPropertyDescriptor)context.PropertyDescriptor;
                foreach (PropertyListItem pli in dsc.Property.ListItems)
                {
                    if (string.Compare(pli.Value, value.ToString(), true) == 0)
                        return pli.Name;
                }
                //return value;
            }

            object o = base.ConvertTo(context, culture, value, destinationType);
            return o;
        }
    }

    // =======================================================================================

}
