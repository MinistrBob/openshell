﻿using System;
using System.Collections.Generic;
using Microsoft.Win32;

namespace OpenShellClasses
{
    public static class Util
    {
        public static bool ConvertStrToBool(string value)
        {
            return (value == "Д");
        }

        public static string ConvertBoolToStr(bool value)
        {
            if (value)
                return "Д";
            else
                return "Н";
        }

        public static RepositoryClassDefaultStatus ConvertDefaultStatusStr(string s)
        {
            if (s == "+")
                return RepositoryClassDefaultStatus.Enabled;
            else if (s == "*")
                return RepositoryClassDefaultStatus.Always;
            else
                return RepositoryClassDefaultStatus.Disabled;
        }

        public static string ConvertDefaultStatus(RepositoryClassDefaultStatus status)
        {
            switch (status)
            {
                case RepositoryClassDefaultStatus.Enabled:
                    return "+";
                case RepositoryClassDefaultStatus.Disabled:
                    return "-";
                case RepositoryClassDefaultStatus.Always:
                    return "*";
                default:
                    return "-";
            }
        }

        /// <summary>
        /// возвращает список доступных идентификаторов БД в системе
        /// </summary>
        public static string[] GetOracleDbNames()
        {
            List<string> result = new List<string>();
            try
            {
                OdpNetConfiguration odp_cfg = new OdpNetConfiguration();
                OracleConfiguration ora_cfg = new OracleConfiguration();
                ora_cfg.Fill();
                odp_cfg.Fill();

                foreach (OracleHomeInfo Home in ora_cfg.Homes)
                {
                    if (Home.VersionMajor == odp_cfg.ConfiguredVersionMajor ||
                        odp_cfg.ConfiguredVersionMajor == OracleMajorVersion.Unknown)
                    {
                        Home.CheckOraHome();
                        foreach (TnsNamesItem item in Home.Names)
                            if (!result.Contains(item.Name) && item.Protocol != "IPC")
                                result.Add(item.Name);
                        Home.CheckOraHome();
                    }
                }
            }
            catch (Exception)
            {
                return result.ToArray();
            }
            /*

            int config_ver = GetConfiguredVersion();

            RegistryKey rk = Registry.LocalMachine.OpenSubKey("Software\\ORACLE");
            List<string> oraclePaths = new List<string>();
            List<string> checkDirs = new List<string>();
            if (rk != null)
            {
                string oracle_data_path_value_name = "ORACLE_HOME";
                if (RegistryKeyContainsValue(rk, oracle_data_path_value_name))
                    checkDirs.Add(rk.GetValue(oracle_data_path_value_name).ToString());
                string[] subkeys = rk.GetSubKeyNames();
                foreach (string s_subkey in subkeys)
                {
                    RegistryKey rks = rk.OpenSubKey(s_subkey, false);
                    if (RegistryKeyContainsValue(rks, oracle_data_path_value_name))
                        checkDirs.Add(rks.GetValue(oracle_data_path_value_name).ToString());
                    rks.Close();
                }
                rk.Close();

                foreach (string dir in checkDirs)
                    if (Directory.Exists(dir) && File.Exists(Path.Combine(dir, @"network\ADMIN\tnsnames.ora")))
                        using (StreamReader sr = new StreamReader(Path.Combine(dir, @"network\ADMIN\tnsnames.ora"), Encoding.Default))
                        {
                            StringBuilder sb = new StringBuilder();
                            bool gather = true;
                            int sq_count = 0;
                            string s = null;
                            while ((s = sr.ReadLine()) != null)
                            {
                                s = s.Trim();
                                if (s.ToUpper().Replace(" ", "").Contains("PROTOCOL=IPC") &&
                                    (result.Count > 0))
                                    result.RemoveAt(result.Count - 1);

                                if (!string.IsNullOrEmpty(s) && !s.StartsWith("#"))
                                {
                                    foreach (char c in s)
                                    {
                                        if (!char.IsWhiteSpace(c) && gather && (c != '=') && (c != '(') && (c != ')'))
                                            sb.Append(c);
                                        else if (c == '(')
                                        {
                                            sq_count++;
                                            if (gather)
                                            {
                                                result.Add(sb.ToString());
                                                sb.Remove(0, sb.Length);
                                            }
                                            gather = false;
                                        }
                                        else if (c == ')')
                                        {
                                            sq_count--;
                                            if (sq_count < 0)
                                                sq_count = 0;
                                            if (!gather && sq_count == 0)
                                                gather = true;
                                        }
                                    }
                                }
                            }
                        }
            }*/
            return result.ToArray();    
        }

        private static int GetConfiguredVersion()
        {
            OdpNetConfiguration config = new OdpNetConfiguration();
            config.Fill();
            return 0;
        }

        private static bool RegistryKeyContainsValue(RegistryKey rk, string value_path)
        {
            string[] ss = rk.GetValueNames();
            foreach (string s in ss)
                if (string.Compare(s, value_path, true) == 0)
                    return true;
            return false;
        }
    }
}
